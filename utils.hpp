#ifndef UTILS_HPP
#define UTILS_HPP

#include <dlib/image_processing.h>

#include <fstream>
#include <iostream>
#include <stdio.h>
#include <string>
#include <time.h>

#include <opencv2/bgsegm.hpp>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/objdetect/objdetect.hpp>

#include "UserFacialInfor.hpp"

#include <boost/filesystem.hpp>
#define LEVEL 256
#define INTENSITY_MAX 255
#define INTENSITY_MIN 0

// test
using namespace boost::filesystem;
using namespace dlib;
using namespace std;
using namespace cv;

extern int d2;
extern int d3;

void Save_Images_to_TrainingImagePath(Mat image, cv::Rect det, string filepath,
                                      string filename);
void Drawing_Detection(Mat &image, Rect rect, Scalar color);
void Drawing_ProgressBar(Mat &image, Point pt, int width, int length,
                         int maxlength);
void Drawing_Landmark(
    Mat &image, const full_object_detection
                    &det); // const std::vector<full_object_detection>& dets);
void Remove_BackgroundNoise_oneface(Mat &image,
                                    const full_object_detection &shape,
                                    cv::Rect det);
void Remove_BackgroundNoise(Mat &image, std::vector<matrix<rgb_pixel>> &faces,
                            const std::vector<full_object_detection> &shapes,
                            std::vector<cv::Rect> dets);

void Init_BGSubtraction(Ptr<BackgroundSubtractor> pBgSub);
bool IndentifyMoving(Mat image, Ptr<BackgroundSubtractor> pBgSub);
void ChangeBrightnessFocus(Mat &image, Rect rectperson, int &current_state,
                           string name);
Mat shadowRemove(Mat &background, Mat forceground, Mat fgMask);
dlib::matrix<dlib::rgb_pixel> Convert_Mat2Dlib(Mat image);
// Mat Convert_Dlib2Mat(dlib::matrix<dlib::rgb_pixel> dimg);
void Get_Average_Brightness(const Mat img, double &brightness);

FILE *logfile(void);
void Save_Log_Activity(string res);
User_Infor Read_Info_GivenName(string filepath, string &str);
void Auto_Save_Face_Images(matrix<rgb_pixel> face, bool save_all_or_unknown);
dlib::matrix<dlib::rgb_pixel> Convert_Mat2Dlib(Mat image);

void Flush_Opencv_Buffer(VideoCapture &camera);

Mat Histogram_Equalization(Mat inputImage);
Mat Histogram_Matching(Mat inputFile, Mat targetFile);

bool copyDir(boost::filesystem::path const &source,
             boost::filesystem::path const &destination);
void Clustering_Then_Removing_Minority(string directory);

void Detect_FaceonImage_toSavetoGallery(string imagename, string pathtonameid);

void Measure_Distance_Between_Images(string imagename1, string imagename2);
void Pose_Angle_Estimation(); // using 3dCalibrate for attraction detection
void MovingScene_Sensitive();
void CallBackFunc(int event, int x, int y, int flags, void *userdata);

void initParameters();
void textCentered(cv::InputOutputArray img, const std::string &text, int x1,
                  int _width, int y, int fontFace, double fontScale,
                  cv::Scalar color, int thickness = 1,
                  int lineType = cv::LINE_8, bool bottomLeftOrigin = false,
                  bool background = false);
#endif // UTILS_HPP
