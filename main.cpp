#include "mainwindow.h"
#include <QApplication>
#include <memory>

#include "include/qpointcloud.h"
#include "include/qpointcloudgeometry.h"
#include "include/qpointcloudreader.h"
#include "include/qpointfield.h"
#include <QQmlApplicationEngine>
#include <QQuickView>
//#include <SocketIoUtils.hpp>
#include <qqml.h>

int main(int argc, char *argv[]) {

  qRegisterMetaType<QVector<string>>("QVector<string>");
  qRegisterMetaType<QVector<matrix<float, 0, 1>>>("QVector<matrix<float,0,1>>");
  qRegisterMetaType<QVector<QImage>>("QVector<QImage>");
  qRegisterMetaType<QVector<matrix<rgb_pixel>>>("QVector<matrix<rgb_pixel>>");
  qRegisterMetaType<int>("int");

  qmlRegisterType<QPointCloudReader>("pcl", 1, 0, "PointcloudReader");
  qmlRegisterType<QPointcloud>("pcl", 1, 0, "Pointcloud");
  qmlRegisterType<QPointcloudGeometry>("pcl", 1, 0, "PointcloudGeometry");
  qmlRegisterUncreatableType<QPointfield>(
      "pcl", 1, 0, "Pointfield",
      "Can not yet be created in qml, use PointcloudReader.");
  //  SocketIoUtils socket;
  //  cout << "main 1" << endl;
  //  socket.SetUrl("ws://api.hrl.vp9.vn/");
  //  cout << "main 2" << endl;
  //  socket.Connect2Server();
  //  cout << "main 3" << endl;
  QApplication a(argc, argv);
  MainWindow *w = new MainWindow();

  w->setAttribute(Qt::WA_DeleteOnClose, true);
  w->show();

  //    QQmlApplicationEngine engine;
  //    engine.load(QUrl(QStringLiteral("qml/main.qml")));
  //  socket.Disconnect();
  return a.exec();
}
