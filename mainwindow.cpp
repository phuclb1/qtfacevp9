#include "mainwindow.h"

#include "View.h"
#include <cstdlib>
#include <functional>
#include <iostream>

#include "GraphicsItem.h"
#include <Q3DScatter>
#include <QFileDialog>
#include <QGraphicsItem>
#include <QHBoxLayout>
#include <QLabel>
#include <QMenuBar>
#include <QMessageBox>
#include <QPushButton>
#include <QSlider>
#include <QSplitter>
#include <QStatusBar>
#include <QTime>
#include <QToolBar>
#include <QVBoxLayout>
#include <mutex>

#include <QtDataVisualization>
using namespace std;
#define kURL "http://api.hrl.vp9.vn/"
#define BIND_EVENT(IO, EV, FN) IO->on(EV, FN)
MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent) {
  bool debug = false;
  //  //  h.connect("ws://api.hrl.vp9.vn/");
  //  _io->connect("http://api.hrl.vp9.vn/socket.io/"
  //               "?EIO=4&transport=websocket");
  //  std::string a = "asv";

  //  using std::placeholders::_1;
  //  using std::placeholders::_2;
  //  using std::placeholders::_3;
  //  using std::placeholders::_4;
  //  socket::ptr sock = _io->socket();

  //  BIND_EVENT(sock, "arlerthuman",
  //             std::bind(&MainWindow::OnNewProfile, this, _1, _2, _3, _4));
  //  _io->set_socket_open_listener(
  //      std::bind(&MainWindow::OnConnected, this, std::placeholders::_1));
  //  _io->set_close_listener(std::bind(&MainWindow::OnClosed, this, _1));
  //  _io->set_fail_listener(std::bind(&MainWindow::OnFailed, this));

  face_id_3d_set.clear();
  face_id_3d_set.insert("NamND");
  face_id_3d_set.insert("Nga");
  face_id_3d_set.insert("Bon");
  face_id_3d_set.insert("DungVQ");
  face_id_3d_set.insert("ThanhNN");
  face_id_3d_set.insert("TucNV");
  face_id_3d_set.insert("HaLeManh");
  face_id_3d_set.insert("PhucLee");
  face_id_3d_set.insert("Tuyen");
  face_id_3d_set.insert("CuongVL");

  if (debug) {
    std::cout << "MainWindow::MainWindow >> 0" << std::endl;
  }

  data = std::make_shared<Gallery>();
  if (debug) {
    std::cout << "MainWindow::MainWindow >> 0.1 " << std::endl;
  }
  data->readFromFolder("Gallery_demo/");
  if (debug) {
    std::cout << "MainWindow::MainWindow >> 0.2 " << std::endl;
  }
  data->writeToBinaryFile("FaceVP9.bin");
  if (debug) {
    std::cout << "MainWindow::MainWindow >> 0.3 " << std::endl;
  }
  //  data->readFromBinaryFile("FaceVP9.bin");
  double theta = 0.1;
  double perplexity = 20;
  int no_dims = 2;
  int max_iter = 1000;
  if (debug) {
    std::cout << "MainWindow::MainWindow >> 0.4 " << std::endl;
  }
  data->tSNE(theta, perplexity, no_dims, max_iter);
  //  data->writeToTextFile("FaceVP9.txt");
  if (debug) {
    std::cout << "MainWindow::MainWindow >> 0.5 " << std::endl;
  }
  std::string currentFaceId = "";
  for (int i = 0; i < data->getN(); ++i) {

    if (data->getFaceId(i).compare(currentFaceId) != 0) {

      std::shared_ptr<Gallery> newData = std::make_shared<Gallery>();
      newData->cloneDataByFaceId(data->getFaceId(i), data);
      if (newData->getN() > 10) {

        double theta = 0.1;
        double perplexity = 3;
        int no_dims = 3;
        int max_iter = 1000;
        newData->tSNE(theta, perplexity, no_dims, max_iter);
        this->list3dData.insert(
            std::pair<std::string, std::shared_ptr<Gallery>>(data->getFaceId(i),
                                                             newData));
      }
      currentFaceId = data->getFaceId(i);
    } else {
    }
  }

  populateScene();

  if (debug) {
    std::cout << "MainWindow::MainWindow >> 1" << std::endl;
  }

  myPlayer = new Player();
  if (debug) {
    std::cout << "MainWindow::MainWindow >> 1.socket" << std::endl;
  }
  //  usleep(5000);
  //  mySocket = new Socket(0);

  if (debug) {
    std::cout << "MainWindow::MainWindow >> 1,1,socket" << std::endl;
  }
  QObject::connect(myPlayer, SIGNAL(processedImage(QImage)), this,
                   SLOT(updatePlayerUI(QImage)));
  this->resize(1198, 702);
  // Phuclb

  //  list_widget = new QListWidget(this);
  //  item_1 = new QListWidgetItem(list_widget);
  //  QIcon icon_1;

  //  item_1->setIcon(icon_1);
  //  item_2 = new QListWidgetItem(list_widget);
  //  item_3 = new QListWidgetItem(list_widget);

  centralWidget = new QWidget(this);
  centralWidget->setObjectName(QStringLiteral("centralWidget"));
  if (debug) {
    std::cout << "MainWindow::MainWindow >> 1.1" << std::endl;
  }

  vboxlayout = new QVBoxLayout();
  vsplitter = new QSplitter(this);
  vsplitter->setOrientation(Qt::Vertical);

  // toggle switch
  hboxlayout_3 = new QHBoxLayout();
  widget_3 = new QWidget(this);
  switch_3 = new Switch;

  // widget_3->setWindowFlags(Qt::FramelessWindowHint);
  // switch_3->shortcut(QKeySequence(Qt::Key_Tab));
  hboxlayout_3->addWidget(switch_3);
  widget_3->setLayout(hboxlayout_3);
  hboxlayout_3->addStretch();

  widget_3->setMaximumHeight(40);
  widget_3->setMinimumHeight(40);
  // widget_3->setMaximumHeight(50);

  vsplitter->addWidget(widget_3);
  QObject::connect(switch_3, SIGNAL(keyPressed()), this,
                   SLOT(on_switch_clicked()));
  QObject::connect(switch_3, SIGNAL(clicked()), this,
                   SLOT(on_switch_clicked()));
  hboxlayout = new QHBoxLayout();

  video_label = new QLabel(this);
  video_label->setObjectName(QStringLiteral("label"));
  video_label->setGeometry(QRect(10, 10, 1041, 571));
  video_label->setStyleSheet(QStringLiteral("Background-color: #000;"));
  hboxlayout->addWidget(video_label);

  //	graph_label = new QLabel(centralWidget);
  //	graph_label->setStyleSheet("QLabel { background-color : red; color :
  // blue; }");
  //	hboxlayout->addWidget(graph_label);

  //	vboxlayout->addLayout(hboxlayout);
  widget_0 = new QWidget(this);
  widget_0->setLayout(hboxlayout);
  widget_0->setMinimumHeight(600);
  vsplitter->addWidget(widget_0);

  if (debug) {
    std::cout << "MainWindow::MainWindow >> 1.2" << std::endl;
  }

  hboxlayout_1 = new QHBoxLayout();

  pushButton = new QPushButton(this);
  pushButton->setObjectName(QStringLiteral("pushButton"));
  pushButton->setGeometry(QRect(1080, 220, 99, 27));
  hboxlayout_1->addWidget(pushButton);

  pushButton_2 = new QPushButton(this);
  pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
  pushButton_2->setGeometry(QRect(1080, 280, 99, 27));
  hboxlayout_1->addWidget(pushButton_2);

  hboxlayout_1->addStretch();

  push_button_show_graph = new QPushButton(this);
  push_button_show_graph->setText("Graph");
  push_button_show_graph->setGeometry(QRect(1080, 280, 99, 27));
  hboxlayout_1->addWidget(push_button_show_graph);

  QObject::connect(push_button_show_graph, SIGNAL(clicked()), this,
                   SLOT(onGraphButtonClicked()));

  //	vboxlayout->addLayout(hboxlayout_1);
  widget_1 = new QWidget(this);
  widget_1->setLayout(hboxlayout_1);
  vsplitter->addWidget(widget_1);

  hboxlayout_2 = new QHBoxLayout();

  label_2 = new QLabel(this);
  label_2->setObjectName(QStringLiteral("label_2"));
  hboxlayout_2->addWidget(label_2);

  horizontalSlider = new QSlider(this);
  horizontalSlider->setObjectName(QStringLiteral("horizontalSlider"));
  horizontalSlider->setOrientation(Qt::Horizontal);
  hboxlayout_2->addWidget(horizontalSlider);

  if (debug) {
    std::cout << "MainWindow::MainWindow >> 1.3" << std::endl;
  }

  label_3 = new QLabel(this);
  label_3->setObjectName(QStringLiteral("label_3"));
  label_3->setGeometry(QRect(930, 610, 67, 17));
  hboxlayout_2->addWidget(label_3);

  //	vboxlayout->addLayout(hboxlayout_2);

  if (debug) {
    std::cout << "MainWindow::MainWindow >> 1.3.1" << std::endl;
  }

  widget_2 = new QWidget(this);
  widget_2->setLayout(hboxlayout_2);
  vsplitter->addWidget(widget_2);
  vsplitter->setMinimumWidth(1100);

  // set height for video & "Stop" button in vsplitter
  vsplitter->setStretchFactor(0, 10);
  vsplitter->setStretchFactor(1, 120);
  vsplitter->setStretchFactor(2, 10);
  vsplitter->setStretchFactor(3, 10);

  if (debug) {
    std::cout << "MainWindow::MainWindow >> 1.3.2" << std::endl;
  }

  vboxlayout->addWidget(vsplitter);
  centralWidget->setLayout(vboxlayout);

  splitter = new QSplitter(this);
  //  splitter->addWidget(list_widget);
  //  list_widget->hide();
  splitter->addWidget(centralWidget);

  if (debug) {
    std::cout << "MainWindow::MainWindow >> 1.3.3" << std::endl;
  }
  vsplitter2 = new QSplitter(this);
  vsplitter2->setOrientation(Qt::Vertical);

  // toggle switch
  hboxlayout_graph = new QHBoxLayout();
  widget_graph = new QWidget(this);
  switch_graph = new Switch;

  // widget_3->setWindowFlags(Qt::FramelessWindowHint);
  // switch_3->shortcut(QKeySequence(Qt::Key_Tab));
  hboxlayout_graph->addWidget(switch_graph);
  widget_graph->setLayout(hboxlayout_graph);
  hboxlayout_graph->addStretch();

  widget_graph->setMaximumHeight(40);
  widget_graph->setMinimumHeight(40);
  // widget_3->setMaximumHeight(50);

  vsplitter2->addWidget(widget_graph);
  QObject::connect(switch_graph, SIGNAL(keyPressed()), this,
                   SLOT(on_switch_graph_clicked()));
  QObject::connect(switch_graph, SIGNAL(clicked()), this,
                   SLOT(on_switch_graph_clicked()));

  if (debug) {
    std::cout << "MainWindow::MainWindow >> 1.3.4" << std::endl;
  }

  //  vboxlayout2 = new QVBoxLayout();

  //==================
  // 2D & 3D Graph
  //==================

  view = new View("Graph", this);
  view->view()->setScene(scene);
  vsplitter2->addWidget(view);
  //  newView = new View("Detail Graph", this);
  //  vsplitter2->addWidget(newView);
  dGraph = new QtDataVisualization::Q3DScatter();

  container3DGraph = QWidget::createWindowContainer(dGraph);

  vsplitter2->addWidget(container3DGraph);

  this->container3DGraph->setVisible(false);
  this->splitter->addWidget(vsplitter2);

  //  this->newView->setVisible(false);

  if (debug) {
    std::cout << "MainWindow::MainWindow >> 1.3.5" << std::endl;
  }

  //  view_3d = new QQuickView();
  //  view_3d->setSource(QUrl("qml/main.qml"));
  //  view_3d->show();
  //  widget_3d = QWidget::createWindowContainer(view_3d, this);

  //==================
  // 3D face
  //==================

  current_3d_face_id = "";
  widget_3d = 0;
  //  widget_3d = new QQuickWidget(this);
  //  widget_3d->setSource(QUrl(QStringLiteral("qml/main.qml")));

  layout_3d = new QVBoxLayout();
  //  layout_3d->addWidget(widget_3d);
  //  widget_3d->setGeometry(1, 1, 100, 100);
  //    layout_3d->setSizeConstraint(QLayout::SetMaximumSize);

  group_box_3d = new QGroupBox(tr("3D"), this);
  group_box_3d->setLayout(layout_3d);

  vsplitter2->addWidget(group_box_3d);
  vsplitter2->setStretchFactor(0, 10);
  vsplitter2->setStretchFactor(1, 100);
  vsplitter2->setStretchFactor(2, 100);
  vsplitter2->setStretchFactor(3, 100);
  this->setCentralWidget(splitter);

  if (debug) {
    std::cout << "MainWindow::MainWindow >> 1.4" << std::endl;
  }

  //  menuBar = new QMenuBar(this);
  //  menuBar->setObjectName(QStringLiteral("menuBar"));
  //  menuBar->setGeometry(QRect(0, 0, 1198, 25));
  //  this->setMenuBar(menuBar);

  //  mainToolBar = new QToolBar(this);
  //  mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
  //  this->addToolBar(Qt::TopToolBarArea, mainToolBar);

  statusBar = new QStatusBar(this);
  statusBar->setObjectName(QStringLiteral("statusBar"));
  this->setStatusBar(statusBar);
  this->setWindowTitle("FaceVP9");

  video_label->setText(QString());

  if (debug) {
    std::cout << "MainWindow::MainWindow >> 1.5" << std::endl;
  }

  pushButton->setText("Change");
  QObject::connect(pushButton, SIGNAL(clicked()), this,
                   SLOT(on_pushButton_clicked()));

  if (debug) {
    std::cout << "MainWindow::MainWindow >> 2" << std::endl;
  }

  //    std::string file_name = "/home/vp9/Downloads/Telegram
  //    Desktop/2018_03_16_17_13_39.mp4";
  //    std::string file_name = "/home/vp9/Desktop/2018_01_16_222.mp4";
  //    std::string file_name = "/home/vp9/GitLab/QtOpenCV/camgocrong3.mp4";
  //    std::string file_name = "rtsp://10.12.11.152:554/stream1";
  //    std::string file_name = "rtsp://10.12.11.222";
  std::string file_name = "rtsp://192.168.10.18:554/av0_0";
  //    std::string file_name = "phuclb.mp4";
  //    std::string file_name = "rtsp://192.168.2.100:554/av0_0"; // VTV
  if (!myPlayer->loadVideo(file_name)) {
    QMessageBox msgBox;
    msgBox.setText("The selected video could not be opened!");
    msgBox.exec();
  } else {
    pushButton_2->setEnabled(true);
    horizontalSlider->setEnabled(true);
    horizontalSlider->setMaximum(myPlayer->getNumberOfFrames());
    label_3->setText(getFormattedTime((int)myPlayer->getNumberOfFrames() /
                                      (int)myPlayer->getFrameRate()));
  }

  QObject::connect(pushButton_2, SIGNAL(clicked()), this,
                   SLOT(on_pushButton_2_clicked()));

  pushButton_2->setText("Play");
  label_2->setText("00:00");
  label_3->setText("00:00");

  this->modifier = new ScatterDataModifier(this->dGraph);
  this->dGraph->axisX()->setTitle("X");
  this->dGraph->axisY()->setTitle("Y");
  this->dGraph->axisZ()->setTitle("Z");

  QObject::connect(
      myPlayer, SIGNAL(new_persons_appear(QVector<matrix<float, 0, 1>>,
                                          QVector<matrix<rgb_pixel>>)),
      this, SLOT(on_catch_new_person_appear(QVector<matrix<float, 0, 1>>,
                                            QVector<matrix<rgb_pixel>>)));

  if (debug) {
    std::cout << "MainWindow::MainWindow >> 2.1" << std::endl;
  }

  //  sio::client h;
  //  connection_listener l(h);

  //  h.set_open_listener(std::bind(&connection_listener::on_connected, &l));
  //  h.set_close_listener(
  //      std::bind(&connection_listener::on_close, &l, std::placeholders::_1));
  //  h.set_fail_listener(std::bind(&connection_listener::on_fail, &l));
  //  h.connect("http://api.hrl.vp9.vn/");
  //  _lock.lock();
  //  if (!connect_finish) {
  //    _cond.wait(_lock);
  //  }
  //  _lock.unlock();
  //  current_socket = h.socket();

  if (debug) {
    std::cout << "MainWindow::MainWindow >> 2.2" << std::endl;
  }

  //  BIND_EVENT(sock, "arlerthuman",
  //             std::bind(&Player::onReceiveNewProfile, myPlayer, _1, _2, _3,
  //             _4));

  //	pushButton_2->setEnabled(false);
  if (debug) {
    std::cout << "MainWindow::MainWindow >> 2.3" << std::endl;
  }
  horizontalSlider->setEnabled(false);

  pushButton->setVisible(false);
  widget_2->setVisible(false);

  if (debug) {
    std::cout << "MainWindow::MainWindow 3" << std::endl;
  }
  //#endif
  //  _io->socket()->emit("cleartrack", a);
}

MainWindow::~MainWindow() {
  _io->socket()->off_all();
  _io->socket()->off_error();
  if (myPlayer != 0) {
    delete myPlayer;
    myPlayer = 0;
  }
  if (hboxlayout != 0) {
    delete hboxlayout;
    hboxlayout = 0;
  }
  if (hboxlayout_1 != 0) {
    delete hboxlayout_1;
    hboxlayout_1 = 0;
  }
  if (hboxlayout_2 != 0) {
    delete hboxlayout_2;
    hboxlayout_2 = 0;
  }
  if (vboxlayout != 0) {
    delete vboxlayout;
    vboxlayout = 0;
  }
  if (layout_3d != 0) {
    delete layout_3d;
    layout_3d = 0;
  }
  //	if (centralWidget != 0) {
  //		delete centralWidget;
  //		centralWidget = 0;
  //	}
  //	if (pushButton != 0) {
  //		delete pushButton;
  //		pushButton = 0;
  //	}
  //	if (pushButton_2 != 0) {
  //		delete pushButton_2;
  //		pushButton_2 = 0;
  //	}
  //	if (horizontalSlider != 0) {
  //		delete horizontalSlider;
  //		horizontalSlider = 0;
  //	}
  //	if (label_2 != 0) {
  //		delete label_2;
  //		label_2 = 0;
  //	}
  //	if (label_3 != 0) {
  //		delete label_3;
  //		label_3 = 0;
  //	}
  //	if (menuBar != 0) {
  //		delete menuBar;
  //		menuBar = 0;
  //	}
  //	if (mainToolBar != 0) {
  //		delete mainToolBar;
  //		mainToolBar = 0;
  //	}
  //	if (statusBar != 0) {
  //		delete statusBar;
  //		statusBar = 0;
  //	}
  //  if (view_3d) {
  //      delete view_3d;
  //      view_3d = 0;
  //  }
}

void MainWindow::updatePlayerUI(QImage img) {
  bool debug = false;
  if (debug) {
    std::cout << "MainWindow::updatePlayerUI >> 0" << std::endl;
  }
  if (!img.isNull()) {
    if (debug) {
      std::cout << "MainWindow::updatePlayerUI >> 0.0" << std::endl;
    }
    video_label->setAlignment(Qt::AlignCenter);
    if (debug) {
      std::cout << "MainWindow::updatePlayerUI >> 0.1" << std::endl;
    }
    video_label->setPixmap(QPixmap::fromImage(img).scaled(
        video_label->size(), Qt::KeepAspectRatio, Qt::SmoothTransformation));
    if (debug) {
      std::cout << "MainWindow::updatePlayerUI >> 0.2" << std::endl;
    }
  }
  if (debug) {
    std::cout << "MainWindow::updatePlayerUI >> 1" << std::endl;
  }
  horizontalSlider->setValue(myPlayer->getCurrentFrame());
  if (debug) {
    std::cout << "MainWindow::updatePlayerUI >> 2" << std::endl;
  }
  label_2->setText(getFormattedTime((int)myPlayer->getCurrentFrame() /
                                    (int)myPlayer->getFrameRate()));
  if (debug) {
    std::cout << "MainWindow::updatePlayerUI >> 3" << std::endl;
  }
}

void MainWindow::on3DFaceChange(const QString &face_id) {
  bool debug = true;

  if ((!face_id_3d_set.contains(face_id)) || (face_id == current_3d_face_id)) {
    return;
  }

  QUrl url(QStringLiteral("qml/") + face_id + QStringLiteral(".qml"));
  if (!url.isValid()) {
    return;
  }

  current_3d_face_id = face_id;

  if (widget_3d) {
    layout_3d->removeWidget(widget_3d);
    delete widget_3d;
    widget_3d = 0;
  }
  widget_3d = new QQuickWidget(this);
  widget_3d->setSource(QUrl(QStringLiteral("qml/") + current_3d_face_id +
                            QStringLiteral(".qml")));
  //  widget_3d->resize(vsplitter2->width(), 400); //@TODO -> set height for
  //  widget_3d
  widget_3d->update();
  layout_3d->addWidget(widget_3d);
}

void MainWindow::on_pushButton_clicked() {
  bool debug = true;
  //  QString filename = QFileDialog::getOpenFileName(
  //      this, tr("Open Video"), ".", tr("Video Files (*.avi *.mpg *.mp4)"));
  //  QFileInfo name = filename;

  //  if (!filename.isEmpty()) {
  //    if (!myPlayer->loadVideo(filename.toLatin1().data())) {
  //      QMessageBox msgBox;
  //      msgBox.setText("The selected video could not be opened!");
  //      msgBox.exec();
  //    } else {
  //      //            this->setWindowTitle(name.fileName());
  //      pushButton_2->setEnabled(true);
  //      horizontalSlider->setEnabled(true);
  //      horizontalSlider->setMaximum(myPlayer->getNumberOfFrames());
  //      label_3->setText(getFormattedTime((int)myPlayer->getNumberOfFrames() /
  //                                        (int)myPlayer->getFrameRate()));
  //      //
  //      video_label->setFixedWidth(myPlayer->getFrameWidth());
  //      //
  //      video_label->setFixedHeight(myPlayer->getFrameHeight());
  //    }
  //  }
  if (debug) {
    std::cout << "MainWindow::on_pushButton_clicked >> current_3d_face_id = "
              << current_3d_face_id.toStdString() << std::endl;
  }
  if (current_3d_face_id == "") {
    if (debug) {
      std::cout
          << "MainWindow::on_pushButton_clicked >> 3d face id change >> main"
          << std::endl;
    }
    on3DFaceChange("main");
  } else if (current_3d_face_id == "main") {
    if (debug) {
      std::cout
          << "MainWindow::on_pushButton_clicked >> 3d face id change >> Muoi"
          << std::endl;
    }
    on3DFaceChange("Muoi");
  } else if (current_3d_face_id == "Muoi") {
    if (debug) {
      std::cout
          << "MainWindow::on_pushButton_clicked >> 3d face id change >> xxx"
          << std::endl;
    }
    on3DFaceChange("xxx");
  } else {
    if (debug) {
      std::cout
          << "MainWindow::on_pushButton_clicked >> 3d face id change >> ..."
          << std::endl;
    }
    on3DFaceChange("");
  }
}

void MainWindow::on_pushButton_2_clicked() {
  if (myPlayer->isStopped()) {
    myPlayer->Play();
    pushButton_2->setText(tr("Stop"));
  } else {
    myPlayer->Stop();
    pushButton_2->setText(tr("Play"));
  }
}

void MainWindow::on_switch_clicked() {
  if (this->switch_3->isSwitchOn()) {
    myPlayer->setMode(MODE::TRAINING);
  } else {
    myPlayer->setMode(MODE::RECOGNITION);
  }
}

void MainWindow::on_switch_graph_clicked() {
  if (this->switch_graph->isSwitchOn()) {
    this->view->setVisible(false);
    this->container3DGraph->setVisible(true);
  } else {
    this->view->setVisible(true);
    this->container3DGraph->setVisible(false);
  }
}

void MainWindow::on_catch_new_person_appear(
    QVector<matrix<float, 0, 1>> vec, QVector<matrix<rgb_pixel>> img_vec) {
  bool debug = false;
  if (debug) {
    cout << "MainWindow::on_catch_new_person_apear >> 0" << endl;
  }

  for (int i = 0; i < vec.size(); ++i) {
    //      if (vec.at(i).data )
    std::vector<int> index_vec;
    double sum_distance = 99;
    this->findCloserFace(index_vec, sum_distance, vec.at(i));
    if (sum_distance > 0.9)
      continue;
    std::vector<Point2D> point_vec;
    std::vector<double> distance_vec;
    std::string tmpId = this->data->getFaceId(index_vec[0]);
    if (debug) {
      cout << "MainWindow::on_catch_new_person_apear 1.3  " << i << endl;
    }
    if (this->current_item_vec.size() > 0) {
      for (int k = 0; k < this->current_item_vec.size(); ++k) {
        this->scene->removeItem(this->current_item_vec.at(k));
        this->current_item_vec[k] = NULL;
      }

      this->current_item_vec.erase(current_item_vec.begin(),
                                   current_item_vec.end());
    }
    if (debug) {
      cout << "MainWindow::on_catch_new_person_apear 1.4  " << i << endl;
      cout << "Size:" << index_vec.size() << endl;
    }
    for (int j = 0; j < index_vec.size(); ++j) {
      Point2D tmp_point(this->data->get_2d_X(index_vec[j]),
                        this->data->get_2d_Y(index_vec[j]));

      point_vec.push_back(tmp_point);
      distance_vec.push_back(
          length(this->data->get128Vec(index_vec[j]) - vec[i]));
    }
    //        int x = (int)100 * (this->data->get_2d_X(index_vec[0]) +
    //                            this->data->get_2d_X(index_vec[1]) +
    //                            this->data->get_2d_X(index_vec[2])) /
    //                3;
    //        int y = (int)100 * (this->data->get_2d_Y(index_vec[0]) +
    //                            this->data->get_2d_Y(index_vec[1]) +
    //                            this->data->get_2d_Y(index_vec[2])) /
    //            3;
    // cout << "XXX: " << x << "  -  " << y << endl;
    Point2D tmp_point = findPosOnGraph(point_vec, distance_vec);
    int x = (int)100 * tmp_point.x;
    int y = (int)100 * tmp_point.y;
    cv::Mat cv_image = toMat(img_vec[i]);
    cv::resize(cv_image, cv_image, cv::Size(150, 150));
    cv::cvtColor(cv_image, cv_image, CV_BGR2RGB);

    QImage img = QImage((const unsigned char *)(cv_image.data), cv_image.cols,
                        cv_image.rows, cv_image.step, QImage::Format_RGB888);

    QGraphicsItem *new_item =
        new GraphicsItem(QColor(150, 150, 150, 100), x, y, img);
    new_item->setPos(QPointF(x, y));

    this->scene->addItem(new_item);
    this->current_item_vec.push_back(new_item);
    if (debug) {
      cout << "on_catch_new_person_apear 3" << endl;
    }
    Point2D centerOfCluster = this->findPosOfCluster(tmpId);
    //    this->populateScene(tmpId);
    //    cout << "Center : " << centerOfCluster.x << "__" <<
    //    centerOfCluster.y
    //         << endl;
    if (debug) {
      cout << "on_catch_new_person_apear 3.1" << endl;
    }
    this->view->view()->centerOn((qreal)centerOfCluster.x,
                                 (qreal)centerOfCluster.y);
    if (debug) {
      cout << "on_catch_new_person_apear 3.2" << endl;
    }
    if (tmpId.compare(current_face_id) != 0)
      this->on3DFaceChange(QString::fromStdString(tmpId));
    current_face_id = tmpId;
    this->populateScene(tmpId, vec.at(i));
    //    QTransform a = this->view->view()->transform();
    //    if (a.m11() != 0.4 && a.m22() != 0.4)
    //      this->view->view()->scale(0.4, 0.4);
  }
  if (debug) {
    cout << "on_catch_new_person_apear 4" << endl;
  }
  //  myPlayer->getCurrentFaceImg().erase(myPlayer->getCurrentFaceImg().begin(),
  //                                      myPlayer->getCurrentFaceImg().end());
}

void MainWindow::findCloserFace(std::vector<int> &index, double &distance_sum,
                                matrix<float, 0, 1> vec128) {
  //    cout << "start findCloserFace" << endl;
  double min_1 = 9999;
  double min_2 = 9999;
  double min_3 = 9999;
  int index_1 = -1;
  int index_2 = -1;
  int index_3 = -1;

  for (int i = 0; i < data->getN(); ++i) {
    double d = dlib::length(data->get128Vec(i) - vec128);
    //    cout << i << "---" << d << endl;
    if (min_1 > d) {
      min_3 = min_2;
      index_3 = index_2;
      min_2 = min_1;
      index_2 = index_1;
      min_1 = d;
      index_1 = i;
    } else if (min_2 > d) {
      min_3 = min_2;
      index_3 = index_2;
      min_2 = d;
      index_2 = i;
    } else if (min_3 > d) {
      min_3 = d;
      index_3 = i;
    }
  }
  index.push_back(index_1);
  //  cout << "FaceID 1 :" << index_1;
  index.push_back(index_2);
  //  cout << "FaceID 2 :" << index_2;
  index.push_back(index_3);
  //  cout << "FaceID 3 :" << index_3;
  distance_sum = min_1 + min_2 + min_3;
}

QString MainWindow::getFormattedTime(int timeInSeconds) {
  int seconds = (int)(timeInSeconds) % 60;
  int minutes = (int)((timeInSeconds / 60) % 60);
  int hours = (int)((timeInSeconds / (60 * 60)) % 24);
  QTime t(hours, minutes, seconds);
  if (hours == 0)
    return t.toString("mm:ss");
  else
    return t.toString("h:mm:ss");
}
void MainWindow::on_horizontalSlider_sliderPressed() { myPlayer->Stop(); }
void MainWindow::on_horizontalSlider_sliderReleased() { myPlayer->Play(); }
void MainWindow::on_horizontalSlider_sliderMoved(int position) {
  myPlayer->setCurrentFrame(position);
  label_2->setText(getFormattedTime(position / (int)myPlayer->getFrameRate()));
}

void MainWindow::populateScene() {
  bool debug = false;
  scene = new QGraphicsScene;

  QImage image("qt4logo.png");

  if (debug) {
    std::cout << "populateScene 1" << std::endl;
  }

  // Populate scene
  //	int xx = 0;
  //	int nitems = 0;
  //	for (int i = -11000; i < 11000; i += 110) {
  //		++xx;
  //		int yy = 0;
  //		for (int j = -7000; j < 7000; j += 70) {
  //			++yy;
  //			qreal x = (i + 11000) / 22000.0;
  //			qreal y = (j + 7000) / 14000.0;

  //			QColor color(image.pixel(int(image.width() * x),
  // int(image.height() * y)));
  //			QGraphicsItem *item = new GraphicsItem(color, xx, yy);
  //			item->setPos(QPointF(i, j));
  //			scene->addItem(item);

  //			++nitems;
  //		}
  //	}

  QList<QColor> colors;
  for (int i = 0; i < 1000; ++i) {
    int _r = std::rand() % 255;
    int _g = std::rand() % 255;
    int _b = std::rand() % 255;
    colors << QColor(_r, _g, _b, 100);
  }
  int color_count = 0;
  std::map<QString, QColor> color_map;
  int zzz = 100;

  int max_x = (int)zzz * data->get_2d_X(0);
  int min_x = (int)zzz * data->get_2d_X(0);
  int max_y = (int)zzz * data->get_2d_Y(0);
  int min_y = (int)zzz * data->get_2d_Y(0);

  for (int i = 0; i < data->getN(); ++i) {
    int x = (int)zzz * data->get_2d_X(i);
    int y = (int)zzz * data->get_2d_Y(i);
    if (max_x < x) {
      max_x = x;
    }
    if (min_x > x) {
      min_x = x;
    }
    if (max_y < y) {
      max_y = y;
    }
    if (min_y < y) {
      min_y = y;
    }
    QString face_id = QString::fromStdString(data->getFaceId(i));
    QString pose_id = QString::fromStdString(data->getPoseId(i));
    QString path = QString::fromStdString(data->getPath(i));
    QColor color;
    if (color_map.find(face_id) == color_map.end()) {
      color = colors[color_count];
      color_map[face_id] = color;
      color_count++;
    } else {
      color = color_map[face_id];
    }

    QGraphicsItem *item = new GraphicsItem(color, x, y, pose_id, path);
    item->setPos(QPointF(x, y));
    scene->addItem(item);
  }

  //	scene->setSceneRect(-2000, -2000, 4000, 4000);

  if (debug) {
    std::cout << "min_x = " << min_x << std::endl;
    std::cout << "max_x = " << max_x << std::endl;
    std::cout << "min_y = " << min_y << std::endl;
    std::cout << "max_y = " << max_y << std::endl;
    std::cout << "populateScene 2" << std::endl;
  }
}

double calculateDistaneBtwTwoPoint(Point2D point0, Point2D point1) {
  double x = abs(point0.x - point1.x);
  double y = abs(point0.y - point1.y);
  return sqrt((x * x) + (y * y));
}
int findMax(std::vector<double> vec) {
  return std::max_element(vec.begin(), vec.end()) - vec.begin();
}

void updatePosition(Point2D &point, Point2D target_point) {
  point.x += (target_point.x - point.x) * 0.01;
  point.y += (target_point.y - point.y) * 0.01;
}
bool checkDistane(std::vector<double> distance128_vec,
                  std::vector<double> distance2_vec,
                  std::vector<double> &divide_vec) {

  //  std::transform(distance2_vec.begin(), distance2_vec.end(),
  //                 distance128_vec.begin(), ratio_vec.begin(),
  //                 std::divides<double>());
  for (int i = 0; i < 3; ++i) {
    divide_vec.push_back(distance2_vec[i] / distance128_vec[i]);
    // cout << "ratio ::" << divide_vec[i] << endl;
  }
  if (abs(divide_vec[0] - divide_vec[1]) <= 0.1 &&
      abs(divide_vec[2] - divide_vec[1]) <= 0.1)
    return true;
  return false;
}
Point2D findPosOnGraph(std::vector<Point2D> point_vec,
                       std::vector<double> distance_vec) {
  Point2D temp_point((point_vec[0].x + point_vec[1].x + point_vec[2].x) / 3,
                     (point_vec[0].y + point_vec[1].y + point_vec[2].y) / 3);
  int cnt = 0;
  while (cnt < 1000) {
    cnt++;
    std::vector<double> distance2_vec;
    distance2_vec.push_back(
        calculateDistaneBtwTwoPoint(temp_point, point_vec[0]));
    distance2_vec.push_back(
        calculateDistaneBtwTwoPoint(temp_point, point_vec[1]));
    distance2_vec.push_back(
        calculateDistaneBtwTwoPoint(temp_point, point_vec[2]));
    std::vector<double> divide_vec;
    if (checkDistane(distance_vec, distance2_vec, divide_vec))
      return temp_point;
    else {
      updatePosition(temp_point, point_vec[findMax(divide_vec)]);
    }
  }
  return temp_point;
}

Point2D MainWindow::findPosOfCluster(std::string faceId) {
  //  cout << "ID:" << faceId << endl;
  int count = 0;
  double x = 0, y = 0;
  for (int i = 0; i < data->getN1(); ++i) {
    if (data->getFaceId(i).compare(faceId) == 0) {
      //      cout << 100 * data->get_2d_X(i) << "-" << 100 * data->get_2d_Y(i)
      //      << endl;
      x += 100 * data->get_2d_X(i);
      y += 100 * data->get_2d_Y(i);
      count++;
    }
  }

  Point2D res(x / count, y / count);
  //  cout << "end findPosOfCluster" << endl;
  return res;
}

void MainWindow::populateScene(std::string _faceId,
                               dlib::matrix<float, 0, 1> vec) {
  bool debug = false;
  if (debug) {
    std::cout << "MainWindow::populateScene >> 0 >> size = "
              << list3dData.size() << endl;
  }
  if (list3dData.find(_faceId) == list3dData.end()) {
    if (debug) {
      cout << _faceId << " --- This face ID not exist " << endl;
    }
    return;
  }
  QScatterDataArray *dataArray = new QScatterDataArray;

  if (debug) {
    std::cout << "MainWindow::populateScene >> 1" << std::endl;
  }
  latest_face_id = _faceId;
  dataArray->resize(list3dData[_faceId]->getN());
  QScatterDataItem *ptrToDataArray = &dataArray->first();
  int zzz = 100;
  for (int i = 0; i < list3dData[_faceId]->getN(); ++i) {

    int x = (int)zzz * list3dData[_faceId]->get_2d_X(i);
    int y = (int)zzz * list3dData[_faceId]->get_2d_Y(i);
    int z = (int)zzz * list3dData[_faceId]->get_2d_Z(i);

    //    this->modifier->series->setMesh(QAbstract3DSeries::MeshPyramid);
    this->dGraph->seriesList().at(0)->setMesh(QAbstract3DSeries::MeshPoint);
    //    this->modifier->series->setBaseColor(QColor(0, 0, 255, 100));

    //    this->modifier->series->setMesh(QAbstract3DSeries::MeshCube);
    ptrToDataArray->setPosition(QVector3D(x, y, z));

    ptrToDataArray++;
  }
  if (debug) {
    std::cout << "MainWindow::populateScene >> 3" << std::endl;
  }
  this->dGraph->seriesList().at(0)->dataProxy()->resetArray(dataArray);
  // new series

  if (debug) {
    std::cout << "MainWindow::populateScene >> 3.1" << std::endl;
  }
  std::vector<int> posVec;

  list3dData[_faceId]->getPosOfVec(vec, posVec);

  if (debug) {
    std::cout << "MainWindow::populateScene >> 3.2" << std::endl;
  }
  if (dGraph->seriesList().size() > 1) {
    this->dGraph->removeSeries(current_series);
  }
  if (debug) {
    std::cout << "MainWindow::populateScene >> 3.3" << std::endl;
  }
  this->current_series = NULL;

  if (debug) {
    std::cout << "MainWindow::populateScene >> 4" << std::endl;
  }
  QScatterDataProxy *proxy = new QScatterDataProxy;
  this->current_series = new QScatter3DSeries(proxy);
  current_series->setItemLabelFormat(
      QStringLiteral("@xTitle: @xLabel @yTitle: @yLabel @zTitle: @zLabel"));
  //  QAbstract3DSeries::MeshUserDefined
  current_series->setMesh(QAbstract3DSeries::MeshCube);
  if (debug) {
    std::cout << "MainWindow::populateScene >> 5" << std::endl;
  }
  this->dGraph->addSeries(current_series);
  QScatterDataArray *dataArrayX = new QScatterDataArray;
  dataArrayX->resize(1);
  QScatterDataItem *ptrToDataArrayX = &dataArrayX->first();
  ptrToDataArrayX->setPosition(QVector3D(posVec[0], posVec[1], posVec[2]));
  this->dGraph->seriesList().at(1)->dataProxy()->resetArray(dataArrayX);
  current_series->setBaseColor(QColor(0, 0, 255, 100));
  //  cout << "PosVec Size: " << posVec.size() << endl;
  //  for (int a : posVec)
  //    cout << a << endl;
  if (debug) {
    std::cout << "MainWindow::populateScene >> done!" << std::endl;
  }
}

void MainWindow::onFaceAdded(const QString &face_id, const QImage &face_image) {
  bool debug = true;
  if (debug) {
    std::cout << "MainWindow::onFaceAdded >> " << face_id.toStdString()
              << std::endl;
  }
}

void MainWindow::onGraphButtonClicked() {
  vsplitter2->setVisible(!vsplitter2->isVisible());
}

void MainWindow::OnNewProfile(std::string const &name, message::ptr const &data,
                              bool hasAck, message::list &ack_resp) {
  bool debug = true;
  if (debug) {
    std::cout << "MainWindow::OnNewProfile >> " << std::endl;
  }
  if (data->get_flag() == message::flag_string) {
    cout << data->get_string() << endl;
  }
}

void MainWindow::OnConnected(std::string const &nsp) {
  cout << "connected" << endl;
}

void MainWindow::OnClosed(client::close_reason const &reason) {
  cout << "closed" << endl;
}

void MainWindow::OnFailed() { cout << "failed" << endl; }
