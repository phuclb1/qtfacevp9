#include "SocketIoUtils.hpp"
#include <QDebug>
#include <QFile>
#include <QString>

using namespace sio;
using namespace std;
std::mutex _lock;

std::condition_variable_any _cond;

bool connect_finish = false;

void connection_listener::on_connected() {
  _lock.lock();
  _cond.notify_all();
  connect_finish = true;
  _lock.unlock();
}
void connection_listener::on_close(client::close_reason const &reason) {
  std::cout << "sio closed " << std::endl;
  exit(0);
}

void connection_listener::on_fail() {
  std::cout << "sio failed " << std::endl;
  exit(0);
}

SocketIoUtils::SocketIoUtils() {

  SendDataDelegate = [=](string mFaceID, std::string data, std::string mCamId,
                         std::string vec128) {
    SendData(mFaceID, data, mCamId, vec128);
  };
  url = faceID = "";
}

void SocketIoUtils::SendData(string mFaceID, std::string data,
                             std::string mCamId, std::string mStatus) {
  cout << "Start SendData" << endl;
  if (!data.empty()) {
    base64Data = data;
    faceID = mFaceID;
    camID = mCamId;
    name = mStatus;
    std::string jsonParseData = WrapData();

    this->h.socket()->emit("found", jsonParseData);
  }
  cout << "End SendData" << endl;
}
void SocketIoUtils::BindEvent() {
  current_socket->on(
      "alerthuman", sio::socket::event_listener_aux([&](
                        string const &name, message::ptr const &data,
                        bool isAck, message::list &ack_resp) {
        _lock.lock();
        cout << "heard" << endl;
        if (data->get_flag() == message::flag_string) {
          cout << data->get_string() << endl;
          Json::Value root;
          Json::Reader reader;
          bool parsingSuccessful =
              reader.parse(data->get_string().c_str(), root); // parse process
          if (!parsingSuccessful) {
            std::cout << "Failed to parse"
                      << reader.getFormattedErrorMessages();
          }
          this->alertUrl = root.get("photo", "").asCString();
          this->alertId = root.get("id", "").asCString();
          this->alertName = root.get("name", "").asCString();
        }
        _cond.notify_all();
        _lock.unlock();
        current_socket->off("alerthuman");
      }));
}
string SocketIoUtils::WrapData() {
  Json::Value jdata;
  jdata["FaceId"] = faceID;
  jdata["CamId"] = camID;
  jdata["Time"] = currentDateTime();
  ;
  jdata["Img"] = base64Data;
  jdata["Status"] = name;
  jdata["Cpu"] = cpuUsage;
  jdata["Ram"] = ramUsage;
  jdata["Disk"] = diskUsage;
  // Output to see the result
  // cout<<"creating nested Json::Value Example pretty print: "
  //     <<endl<<jdata.toStyledString()
  //     <<endl;
  return jdata.toStyledString();
}

void SocketIoUtils::Connect2Server() {

  if (url.empty()) {
    cout << "Url is empty, can't connect to server";
    throw 1;
    return;
  } else {
    connection_listener l(h);
    h.set_open_listener(std::bind(&connection_listener::on_connected, &l));
    h.set_close_listener(
        std::bind(&connection_listener::on_close, &l, std::placeholders::_1));
    h.set_fail_listener(std::bind(&connection_listener::on_fail, &l));
    h.connect(this->url);
    current_socket = h.socket();
    std::string msg = "";
    //    current_socket->emit("cleartrack", msg);
  }
}
void SocketIoUtils::Disconnect() {
  cout << "Closing..." << endl;

  h.sync_close();
  h.clear_con_listeners();
}

string SocketIoUtils::GetCpuUsage() {
  if (sysinfo(&memInfo) != -1) {
    long long virtualMemUsed = memInfo.totalram - memInfo.freeram;
    cout << "Cpu usage: " << virtualMemUsed << endl;
  }
  return "";
}

string SocketIoUtils::GetRamUsage() {
  system("cat /proc/meminfo >/tmp/VP9Ram.txt");
  // qt framework
  QFile file("/tmp/VP9Ram.txt");
  int total = 0, available = 0;
  if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
    return "Error";
  }
  while (!file.atEnd()) {
    QString line = file.readLine();
    if (line.contains("MemTotal")) {
      line = line.simplified().split(" ").at(1);
      total = line.toInt();
    } else if (line.contains("MemAvailable")) {
      available = line.simplified().split(" ").at(1).toInt();
    }
  }
  file.close();
  int pecent = (float)available / total * 100;
  QString result = QString::number(pecent) + "%";
  qDebug() << "result: " << result;
  return result.toStdString();
}

string SocketIoUtils::GetDiskUsage() {
  cout << "wtf " << endl;
  system("df -h / > /tmp/VP9Disk.txt");
  // qt framework
  QFile file("/tmp/VP9Disk.txt");
  if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
    cout << "error" << endl;
    return "Error";
  }
  string result = "error";
  while (!file.atEnd()) {
    QString line = file.readLine();
    qDebug() << "disk: " << line;
    if (line.contains("Use%")) {
      line = file.readLine();
      line = line.simplified().split(" ").at(4);
      qDebug() << "line " << line;
      result = line.toStdString();
      break;
    }
  }
  file.close();
  return result;
}

const string SocketIoUtils::currentDateTime() {
  time_t now = time(0);
  struct tm tstruct;
  char buf[80];
  tstruct = *localtime(&now);
  // Visit http://en.cppreference.com/w/cpp/chrono/c/strftime
  // for more information about date/time format
  strftime(buf, sizeof(buf), "%Y-%m-%d.%X", &tstruct);

  return buf;
}

void SocketIoUtils::Listener() {
  while (true) {
    this->BindEvent();
  }
}
