#include "config.h"
#include <dlib/clustering.h>
#include <dlib/image_loader/load_image.h>
#include <dlib/image_processing/frontal_face_detector.h>
#include <dlib/image_saver/save_jpeg.h>
#include <dlib/opencv.h>

#include <fstream>
#include <iostream>
#include <streambuf>
#include <string>

#include <stdio.h>
#include <time.h>

#include <opencv2/bgsegm.hpp>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/objdetect/objdetect.hpp>

#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <boost/foreach.hpp>

#include "Models.hpp"
#include "UserFacialInfor.hpp"
#include "utils.hpp"

using namespace boost::filesystem;
using namespace dlib;
using namespace std;
using namespace cv;

////////////////////////////////////IMPLEMENTATION//////////////////////////////////////////////
int d2;
int d3;

void Save_Images_to_TrainingImagePath(Mat image, cv::Rect det, string filepath,
                                      string filename) {
  // filepath is folder path ti faceid
  // filename is auto-created file name without extension
  cv::Rect rec(det.x - det.height / 2, det.y - det.height / 2,
               det.width + det.height, det.height + det.height);
  if (rec.x < 0 || rec.y < 0 || rec.x + rec.width > image.cols ||
      rec.y + rec.height > image.rows)
    return;

  cv::Mat face = image(rec);
  if (!boost::filesystem::exists(filepath))
    boost::filesystem::create_directory(filepath);

  string str = filepath + "/" + filename + ".jpg";

  cv::imwrite(str, face);
}

void Drawing_ProgressBar(Mat &image, Point pt, int width, int length,
                         int maxlength) {
  const Scalar color = Scalar(255, 255, 0);
  const Scalar color_pro = Scalar(0, 255, 0);
  int height = (int)width / 15;
  cv::Rect rec(pt.x, pt.y, width, height);
  cv::rectangle(image, rec, color, 2);
  cv::Point vertices[4];
  vertices[0].x = pt.x;
  vertices[0].y = pt.y;
  vertices[1].x = pt.x + (int)width * length / maxlength;
  vertices[1].y = pt.y;
  vertices[2].x = pt.x + (int)width * length / maxlength;
  vertices[2].y = pt.y + height;
  vertices[3].x = pt.x;
  vertices[3].y = pt.y + height;

  // Now we can fill the rotated rectangle with our specified color
  cv::fillConvexPoly(image, vertices, 4, color_pro);
}

void Drawing_Landmark(
    Mat &image, const full_object_detection
                    &det) // const std::vector<full_object_detection>& dets)
{
  int thickness = 1;
  const Scalar color = Scalar(0, 255, 0);
  // for (unsigned long i = 0; i < dets.size(); ++i)
  //{
  DLIB_CASSERT(det.num_parts() == 68,
               "\t Invalid inputs were given to this function. "
                   << "\n\t det.num_parts():  " << det.num_parts());

  const full_object_detection &d = det;
  // Around Chin. Ear to Ear
  for (unsigned long i = 1; i <= 16; ++i)
    line(image, Point(d.part(i)(0), d.part(i)(1)),
         Point(d.part(i - 1)(0), d.part(i - 1)(1)), color, thickness);

  //        cv::putText(image, "1", Point(d.part(1)(0),
  //        d.part(1)(1)),cv::FONT_HERSHEY_TRIPLEX, 1.3,
  //        cv::Scalar(255,255,255), 2, 8);
  //        cv::putText(image, "16", Point(d.part(16)(0),
  //        d.part(16)(1)),cv::FONT_HERSHEY_TRIPLEX, 1.3,
  //        cv::Scalar(255,255,255), 2, 8);

  // Line on top of nose
  for (unsigned long i = 28; i <= 30; ++i)
    line(image, Point(d.part(i)(0), d.part(i)(1)),
         Point(d.part(i - 1)(0), d.part(i - 1)(1)), color, thickness);

  // left eyebrow
  for (unsigned long i = 18; i <= 21; ++i)
    line(image, Point(d.part(i)(0), d.part(i)(1)),
         Point(d.part(i - 1)(0), d.part(i - 1)(1)), color, thickness);
  // Right eyebrow
  for (unsigned long i = 23; i <= 26; ++i)
    line(image, Point(d.part(i)(0), d.part(i)(1)),
         Point(d.part(i - 1)(0), d.part(i - 1)(1)), color, thickness);
  // Bottom part of the nose
  for (unsigned long i = 31; i <= 35; ++i)
    line(image, Point(d.part(i)(0), d.part(i)(1)),
         Point(d.part(i - 1)(0), d.part(i - 1)(1)), color, thickness);
  // Line from the nose to the bottom part above
  line(image, Point(d.part(30)(0), d.part(30)(1)),
       Point(d.part(35)(0), d.part(35)(1)), color, thickness);

  // Left eye
  for (unsigned long i = 37; i <= 41; ++i)
    line(image, Point(d.part(i)(0), d.part(i)(1)),
         Point(d.part(i - 1)(0), d.part(i - 1)(1)), color, thickness);
  line(image, Point(d.part(36)(0), d.part(36)(1)),
       Point(d.part(41)(0), d.part(41)(1)), color, thickness);

  // Right eye
  for (unsigned long i = 43; i <= 47; ++i)
    line(image, Point(d.part(i)(0), d.part(i)(1)),
         Point(d.part(i - 1)(0), d.part(i - 1)(1)), color, thickness);
  line(image, Point(d.part(42)(0), d.part(42)(1)),
       Point(d.part(47)(0), d.part(47)(1)), color, thickness);

  // Lips outer part
  for (unsigned long i = 49; i <= 59; ++i)
    line(image, Point(d.part(i)(0), d.part(i)(1)),
         Point(d.part(i - 1)(0), d.part(i - 1)(1)), color, thickness);
  line(image, Point(d.part(48)(0), d.part(48)(1)),
       Point(d.part(59)(0), d.part(59)(1)), color, thickness);

  // Lips inside part
  for (unsigned long i = 61; i <= 67; ++i)
    line(image, Point(d.part(i)(0), d.part(i)(1)),
         Point(d.part(i - 1)(0), d.part(i - 1)(1)), color, thickness);
  line(image, Point(d.part(60)(0), d.part(60)(1)),
       Point(d.part(67)(0), d.part(67)(1)), color, thickness);

  // }
}

void Remove_BackgroundNoise_oneface(Mat &image,
                                    const full_object_detection &shape,
                                    cv::Rect det) {
  // note mask may be single channel, even if img is multichannel
  //    cv::Mat mask = cv::Mat::zeros(img.rows, img.cols, CV_8UC1);

  if (det.x <= 0 || det.y <= 0 || det.x + det.width >= image.cols ||
      det.y + det.height >= image.rows || det.x >= image.cols ||
      det.y >= image.rows)
    return;

  DLIB_CASSERT(shape.num_parts() == 68,
               "\t Invalid inputs were given to this function. "
                   << "\n\t shape.num_parts():  " << shape.num_parts());

  cv::Point corners[1][19];
  float scale_rate = 1; // 150/det.width;

  // Around Chin. Ear to Ear
  for (unsigned long j = 0; j <= 16; ++j) {
    corners[0][j] = Point(scale_rate * (shape.part(j)(0) - det.x),
                          scale_rate * (shape.part(j)(1) - det.y));
  }

  // int width = d.part(16)(0) - d.part(0)(0);
  int height = std::max(shape.part(8)(1) - shape.part(0)(1),
                        shape.part(8)(1) - shape.part(16)(1));
  corners[0][17] =
      Point(shape.part(16)(0) - det.x, shape.part(16)(1) - (int)height - det.y);
  corners[0][18] =
      Point(shape.part(0)(0) - det.x, shape.part(0)(1) - (int)height - det.y);

  const Point *corner_list[1] = {corners[0]};

  // fill mask with nonzero values, e.g. as Tim suggests
  // cv::fillPoly(...)
  int num_points = 19;
  int num_polygons = 1;
  int line_type = 8;

  cv::Rect det_bigger(det.x - (int)det.width / 2, det.y - (int)det.height / 2,
                      2 * det.width, 2 * det.height);
  if (det_bigger.x <= 0 || det_bigger.y <= 0 ||
      det_bigger.x + det_bigger.width >= image.cols ||
      det_bigger.y + det_bigger.height >= image.rows ||
      det_bigger.x >= image.cols || det_bigger.y >= image.rows)
    return;

  Mat temp = image(det);

  cv::Mat result(temp.size(), temp.type(), cv::Scalar(255, 255, 255));

  cv::Mat mask(temp.size(), temp.type(), cv::Scalar(0, 0, 0));
  cv::Mat mask_bigger(det_bigger.size(), temp.type(), cv::Scalar(0, 0, 0));

  cv::fillPoly(mask, corner_list, &num_points, num_polygons,
               cv::Scalar(255, 255, 255), line_type);

  // Can use bitwise here
  cv::bitwise_and(temp, mask, result);

  cv::Rect det_small(det.width / 2, det.height / 2, det.width, det.height);
  result.copyTo(mask_bigger(det_small));

  mask_bigger.copyTo(image(det_bigger));
}

void Remove_BackgroundNoise(Mat &image, std::vector<matrix<rgb_pixel>> &faces,
                            const std::vector<full_object_detection> &shapes,
                            std::vector<cv::Rect> dets) {
  // note mask may be single channel, even if img is multichannel
  //    cv::Mat mask = cv::Mat::zeros(img.rows, img.cols, CV_8UC1);

  // int thickness = 1;
  // const Scalar color = Scalar(0,255,0);

  // cout<<faces.size()<<" "<<shapes.size()<<" "<<dets.size()<<endl;

  for (unsigned long i = 0; i < faces.size(); ++i) {
    if (shapes[i].num_parts() != 68)
      continue;
    if (dets[i].x <= 0 || dets[i].y <= 0 ||
        dets[i].x + dets[i].width >= image.cols ||
        dets[i].y + dets[i].height >= image.rows || dets[i].x >= image.cols ||
        dets[i].y >= image.rows)
      continue;

    DLIB_CASSERT(shapes[i].num_parts() == 68,
                 "\t Invalid inputs were given to this function. "
                     << "\n\t shapes[" << i
                     << "].num_parts():  " << shapes[i].num_parts());

    const full_object_detection &d = shapes[i];
    cv::Point corners[1][19];
    float scale_rate = 1; // 150/dets[i].width;

    // Around Chin. Ear to Ear
    for (unsigned long j = 0; j <= 16; ++j) {
      // line(img, Point(d.part(i)(0), d.part(i)(1)), Point(d.part(i-1)(0),
      // d.part(i-1)(1)), Scalar(255,255,255), 1);

      corners[0][j] = Point(scale_rate * (d.part(j)(0) - dets[i].x),
                            scale_rate * (d.part(j)(1) - dets[i].y));
    }

    // int width = d.part(16)(0) - d.part(0)(0);
    int height = d.part(8)(1) - d.part(0)(1);
    corners[0][17] = Point(d.part(16)(0) - dets[i].x,
                           d.part(16)(1) - (int)height * 2 / 3 - dets[i].y);
    corners[0][18] = Point(d.part(0)(0) - dets[i].x,
                           d.part(0)(1) - (int)height * 2 / 3 - dets[i].y);

    //        corners[0][16] = Point(d.part(16)(0)+(int) width/8 -dets[i].x,
    //        d.part(16)(1) -dets[i].y);
    //        corners[0][17] = Point(d.part(16)(0)+(int) width/8 -dets[i].x,
    //        d.part(16)(1)-(int) height*2/3 -dets[i].y);
    //        corners[0][18] = Point(d.part(1)(0)-(int) width/8 -dets[i].x,
    //        d.part(16)(1)-(int) height*2/3 -dets[i].y);
    //        corners[0][19] = Point(d.part(1)(0)-(int) width/8 -dets[i].x,
    //        d.part(1)(1) -dets[i].y);

    const Point *corner_list[1] = {corners[0]};

    // fill mask with nonzero values, e.g. as Tim suggests
    // cv::fillPoly(...)
    int num_points = 19;
    int num_polygons = 1;
    int line_type = 8;

    // imshow("1", mask);
    // cout<<dets[i].x<<" "<<dets[i].y<<" "<<dets[i].width<<"
    // "<<dets[i].height<<endl;

    Mat temp = image(dets[i]); // Convert_Dlib2Mat(faces[i]);
    // imshow("2", temp);
    cv::Mat result(temp.size(), temp.type(), cv::Scalar(255, 255, 255));

    cv::Mat mask(temp.size(), temp.type(), cv::Scalar(0, 0, 0));
    cv::fillPoly(mask, corner_list, &num_points, num_polygons,
                 cv::Scalar(255, 255, 255), line_type);

    // Can use bitwise here
    cv::bitwise_and(temp, mask, result);

    //        result.copyTo(image(dets[i]));
    // imshow("3", result);
    // temp.copyTo(result, mask);
    resize(result, result, Size(150, 150));
    faces[i] = Convert_Mat2Dlib(result);
  }
}

void Get_Average_Brightness(const Mat img, double &brightness) {
  Mat temp, color[3], lum;
  temp = img;

  split(temp, color);

  color[0] = color[0] * 0.299;
  color[1] = color[1] * 0.587;
  color[2] = color[2] * 0.114;

  lum = color[0] + color[1] + color[2];

  Scalar summ = sum(lum);

  brightness = summ[0] / ((pow(2, 8) - 1) * img.rows * img.cols) *
               2; //-- percentage conversion factor
}

Mat shadowRemove(Mat &background, Mat forceground, Mat fgMask) {
  //    float alpha = 0.1, beta = 0.9;
  //    float Ts = 0.25, Th = 0.25;
  float alpha = 0.3, beta = 0.7;
  float Ts = 0.9, Th = 0.9;

  Mat forceground_hsv;
  cvtColor(forceground, forceground_hsv, CV_BGR2HSV);
  for (int i = 0, n = forceground.rows; i < n; i++) {
    for (int j = 0, k = forceground.cols; j < k; j++) {
      //            cout << "OK " << i << j << endl;
      Vec3b background_px = background.at<Vec3b>(i, j);
      Vec3b forceground_px = forceground.at<Vec3b>(i, j);

      float FvBv = forceground_px[2] / 1.0 / background_px[2];
      float FsBs = (forceground_px[1] - background_px[1]) / 255.0;
      float FhBh = abs(forceground_px[0] - background_px[0]) / 255.0;
      //            cout << FvBv << " " << FsBs << " " << FhBh << endl;
      if (alpha <= FvBv && FvBv <= beta && FsBs <= Ts && FhBh <= Th) {
        forceground.at<Vec3b>(i, j) = 0;
        fgMask.at<uchar>(i, j) = 0;
        //                cout << "yeah" << i << " " << j << endl;
      } else {
        //                forceground
      }
      //            cout << "OK " << i << j << endl;
    }
  }
  return fgMask;
}

void Drawing_Detection(Mat &image, Rect rect, Scalar color) {
  int x1 = rect.x;
  int y1 = rect.y;
  int x2 = rect.x + rect.width;
  int y2 = rect.y + rect.height;
  int cornerlength = 20;
  // Scalar color = Scalar(0,255,0);
  int thickness = 2;
  line(image, Point(x1, y1), Point(x1 + cornerlength, y1), color, thickness);
  line(image, Point(x1, y1), Point(x1, y1 + cornerlength), color, thickness);

  line(image, Point(x2, y2), Point(x2, y2 - cornerlength), color, thickness);
  line(image, Point(x2, y2), Point(x2 - cornerlength, y2), color, thickness);

  line(image, Point(x1, y2), Point(x1 + cornerlength, y2), color, thickness);
  line(image, Point(x1, y2), Point(x1, y2 - cornerlength), color, thickness);

  line(image, Point(x2, y1), Point(x2 - cornerlength, y1), color, thickness);
  line(image, Point(x2, y1), Point(x2, y1 + cornerlength), color, thickness);
}

// Make image (mode 1) or bounding region (mode 2)
// brighter 1-step (brighter 1) or dimmer 1-step (brighter 0)
// dimmer 4-step (brighter -1)

// current_state = 0: No person presented
// current_state = 1: person presenting
// current_state = 2: person indentified, welcome screen
// current_state = 3: person indentified, goodbye screen

void ChangeBrightnessFocus(Mat &image, Rect rectperson, int &current_state,
                           string name) {
  // This function is called last
  Mat new_image = Mat::zeros(image.size(), image.type());
  int brighter_step = 5;
  /// Initialize values
  //    std::cout<<" Basic Linear Transforms "<<std::endl;
  //    std::cout<<"-------------------------"<<std::endl;
  //    std::cout<<"* Enter the alpha value [1.0-3.0]: ";std::cin>>alpha;
  //    std::cout<<"* Enter the beta value [0-100]: "; std::cin>>beta;

  /// Do the operation new_image(i,j) = alpha*image(i,j) + beta
  if (current_state == 0) {
    image.convertTo(new_image, -1, 1, -150);
    new_image.copyTo(image);

    cv::rectangle(image, rectperson, cv::Scalar(0, 255, 0), 2);
    imshow("Check in-out", image);
    waitKey(1);
  } else if (current_state ==
             1) { // If identify moving object -> screen go bright
    // do nothing, means that to make image bright again
    imshow("Check in-out", image);
    waitKey(1);
  } else if (current_state == 2 || current_state == 3) {
    Mat roi = image(rectperson);

    for (int i = 0; i < brighter_step; i++) {
      for (int y = 0; y < image.rows; y++) {
        for (int x = 0; x < image.cols; x++) {
          if (!rectperson.contains(Point(x, y))) {
            for (int c = 0; c < 3; c++) {
              new_image.at<Vec3b>(y, x)[c] =
                  saturate_cast<uchar>(image.at<Vec3b>(y, x)[c] -
                                       (int)(i + 1) * 255 / brighter_step);
            }
          }
        }
      }
    }
    new_image.copyTo(image);

    roi.copyTo(image(rectperson));

    if (name.empty())
      name = "Guest";
    string text1 = "Welcome: " + name;
    string text2 = "Have a nice day! Mike";
    Point pt1(rectperson.x, rectperson.y + rectperson.height + 10);
    cv::putText(image, text1, pt1, cv::FONT_HERSHEY_TRIPLEX, 1.55,
                cv::Scalar(255, 255, 255), 2, 8);
    Point pt2(rectperson.x, rectperson.y + rectperson.height + 60);
    cv::putText(image, text2, pt2, cv::FONT_HERSHEY_TRIPLEX, 0.55,
                cv::Scalar(255, 255, 255), 2, 8);

    cv::rectangle(image, rectperson, cv::Scalar(0, 255, 0), 2);
    imshow("Check in-out", image);
    waitKey(3000); // 3s

    current_state = 0;
  }
}

void Init_BGSubtraction(Ptr<BackgroundSubtractor> pBgSub) {

  string type = "MOG2";

  if (type == "MOG2") {
    Ptr<BackgroundSubtractorMOG2> pBgSubMOG2 = createBackgroundSubtractorMOG2();

    pBgSubMOG2->setDetectShadows(true);
    pBgSubMOG2->setNMixtures(3);
    pBgSubMOG2->setShadowThreshold(0.8);
    pBgSubMOG2->setShadowValue(0);
    pBgSub = pBgSubMOG2;
  }
  //    else if (type == "MOG")
  //    {
  //        pBgSub = createBackgroundSubtractorMOG();
  //    }
  //#endif
}

bool IndentifyMoving(Mat image, Ptr<BackgroundSubtractor> pBgSub) {
  Mat frame = image.clone();
  Mat1b fgMask;

  double ratioPyramid = 10;
  cv::resize(frame, frame, Size((int)frame.cols / ratioPyramid,
                                (int)frame.rows / ratioPyramid));
  Mat kernel = getStructuringElement(MORPH_RECT, Size(2, 2));

  Mat gray;
  cvtColor(frame, gray, COLOR_BGR2GRAY);

  pBgSub->apply(gray, fgMask);

  // Clean foreground from noise
  morphologyEx(fgMask, fgMask, MORPH_OPEN, kernel);

  // Find contours
  std::vector<std::vector<Point>> contours;
  findContours(fgMask.clone(), contours, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE);

  if (!contours.empty()) {
    // Get largest contour

    for (int i = 0; i < (int)contours.size(); ++i) {
      double area = contourArea(contours[i]);

      // std::cout << area <<std::endl;
      if (area > 30)
        return true;
    }
  }

  return false;
}

FILE *logfile(void) {
  static char name[20];
  time_t now = time(0);
  strftime(name, sizeof(name), "%Y%m%d.txt", localtime(&now));
  return fopen(name, "ab");
}

void Save_Log_Activity(string res) {
  FILE *file = logfile();
  time_t now = time(0);
  static char name[20];
  strftime(name, sizeof(name), "%H%M%S", localtime(&now));
  fprintf(file, "%s %s\n", name, res);
  // do logging
  fclose(file);
}
void Write_Info_GivenName(string filepath) {
  std::ofstream outputStream;
  outputStream.open(filepath);
  //    outputStream << "Name: \n";
  //    outputStream << "Gender: \n";
  //    outputStream << "Age: \n";
  //    outputStream << "Height: \n";
  //    outputStream << "Other: \n";

  outputStream.close();

  //    FILE *file= fopen(filepath, "w");
  //    fprintf(file, "%s\n", info_res);
  //    // do logging
  //    fclose(file);
}
User_Infor Read_Info_GivenName(string filepath, string &str) {

  User_Infor userinfor;
  std::ifstream t(filepath);

  //    t.seekg(0, std::ios::end);
  //    str.reserve(t.tellg());
  //    t.seekg(0, std::ios::beg);

  //    str.assign((std::istreambuf_iterator<char>(t)),
  //                std::istreambuf_iterator<char>());

  //    //cout<<str;
  //    string st = "";

  std::getline(t, str);
  userinfor.name = str;
  std::getline(t, str);
  userinfor.gender = str;
  std::getline(t, str);
  userinfor.age = str;
  std::getline(t, str);
  userinfor.height = str;
  std::getline(t, str);
  userinfor.other = str;

  t.close();

  //        userinfor.name = "Name:";
  //        userinfor.gender = "Gender:";
  //        userinfor.age = "Age:";
  //        userinfor.height = "Height:";

  return userinfor;
}

void Auto_Save_Face_Images(matrix<rgb_pixel> face, bool save_all_or_unknown) {
  time_t now = time(0);
  static char name[50];
  if (save_all_or_unknown)
    strftime(name, sizeof(name), "AutoSave/%Y%m%d-%H%M%S.jpg", localtime(&now));
  else
    strftime(name, sizeof(name), "AutoSave_Unknown/%Y%m%d-%H%M%S.jpg",
             localtime(&now));
  dlib::save_jpeg(face, name);
}
dlib::matrix<dlib::rgb_pixel> Convert_Mat2Dlib(Mat image) {
  dlib::cv_image<bgr_pixel> cv_temp(image);
  dlib::matrix<dlib::rgb_pixel> spimg;
  dlib::assign_image(spimg, cv_temp);
  return spimg;
}

// Mat Convert_Dlib2Mat(dlib::matrix<dlib::rgb_pixel> dimg){
//    dlib::matrix<bgr_pixel> cv_temp;
//    dlib::assign_image(cv_temp, dimg);
//    cv::Mat img = dlib::toMat(cv_temp);
//    return img;
//}

// This to remove camera lagging time
void Flush_Opencv_Buffer(VideoCapture &camera) {
  int delay = 0;

  int framesWithDelayCount = 0;

  while (framesWithDelayCount <= 1) {
    clock_t begin = clock();

    camera.grab();

    clock_t end = clock();
    delay = double(end - begin) / CLOCKS_PER_SEC;

    if (delay > 0) {
      framesWithDelayCount++;
    }
  }
}

// This function takes image object, histogram and size
// Function reads the image and creates a histogram.
void imageToHistogram(Mat image, float histogram[], int size) {

  for (int i = 0; i < LEVEL; i++) {
    histogram[i] = 0;
  }

  for (int y = 0; y < image.rows; y++) {
    for (int x = 0; x < image.cols; x++) {
      histogram[(int)image.at<uchar>(y, x)]++;
    }
  }

  for (int i = 0; i < LEVEL; i++) {
    histogram[i] = histogram[i] / size;
  }

  return;
}

// This fucnction is used to calculate the transfer function of from a given
// histogram. The transfer function created is just the cumulative frequency
// distribution
void calTranFunc(float histogram[], float tranFunc[]) {

  tranFunc[0] = histogram[0];
  for (int i = 1; i < LEVEL; i++) {
    tranFunc[i] = histogram[i] + tranFunc[i - 1];
    // histogram[i] += histogram[i-1];
  }
  return;
}

// This funtion prints the histogram on the console
void printHistogram(float histogram[]) {
  for (int i = 0; i < LEVEL; i++) {
    cout << histogram[i] << ",";
  }
  cout << endl;
  return;
}

// THis function is used to map the histogram to the intensity values that will
// be displayed on the image
void intensityMapping(float tranFunc[], float histogram[]) {
  float tranFuncMin = INTENSITY_MAX + 1;
  for (int i = 0; i < LEVEL; i++) {
    if (tranFuncMin > tranFunc[i]) {
      tranFuncMin = tranFunc[i];
    }
  }

  for (int i = 0; i < LEVEL; i++) {
    histogram[i] =
        (((tranFunc[i] - tranFuncMin) / (1 - tranFuncMin)) * (LEVEL - 1) + 0.5);
  }

  return;
}

// Function to convert an Red Grenn Blue(RGB) space to Hue Saturation
// Intensity(HSI) space
void convert_RGB_To_HSI(Mat inputImage, Mat inputImageHSI, float **H, float **S,
                        float **I) {
  double r, g, b, h, s, in;

  for (int i = 0; i < inputImage.rows; i++) {
    for (int j = 0; j < inputImage.cols; j++) {

      b = inputImage.at<Vec3b>(i, j)[0];
      g = inputImage.at<Vec3b>(i, j)[1];
      r = inputImage.at<Vec3b>(i, j)[2];

      float min_val = 0.0;
      min_val = min(r, min(b, g));
      s = 1 - 3 * (min_val / (b + g + r));

      in = (b + g + r) / 3; // TO SEE

      if (s < 0.00001) {
        s = 0.0;
      } else if (s > 0.99999) {
        s = 1.0;
      }

      if (s != 0.0) {
        h = 0.5 * ((r - g) + (r - b)) /
            sqrt(((r - g) * (r - g)) + ((r - b) * (g - b)));
        h = acos(h);

        if (b <= g) {
          h = h;
        } else {
          h = ((360 * 3.14159265) / 180.0) - h;
        }
      } else {
        h = 0.0;
      }

      inputImageHSI.at<Vec3b>(i, j)[0] = H[i][j] = (h * 180) / 3.14159265;
      inputImageHSI.at<Vec3b>(i, j)[1] = S[i][j] = s * 100;
      inputImageHSI.at<Vec3b>(i, j)[2] = I[i][j] = in;
    }
  }
  return;
}

// Function to convert an  Hue Saturation Intensity(HSI) space to Red Grenn
// Blue(RGB) space
void convert_HSI_To_RGB(Mat outputImage, Mat inputImageHSI, float **H,
                        float **S, float **I) {
  float r, g, b, h, s, in;

  for (int i = 0; i < inputImageHSI.rows; i++) {
    for (int j = 0; j < inputImageHSI.cols; j++) {

      h = H[i][j];
      s = S[i][j] / 100;
      in = I[i][j];

      if (h >= 0.0 && h < 120.0) {
        b = in * (1 - s);
        r = in * (1 + (s * cos(h * 3.14159265 / 180.0) /
                       cos((60 - h) * 3.14159265 / 180.0)));
        g = 3 * in - (r + b);
      } else if (h >= 120.0 && h < 240.0) {
        h = h - 120;
        r = in * (1 - s);
        g = in * (1 + (s * cos(h * 3.14159265 / 180.0) /
                       cos((60 - h) * 3.14159265 / 180.0)));
        b = 3 * in - (r + g);
      } else {
        h = h - 240;
        g = in * (1 - s);
        b = in * (1 + (s * cos(h * 3.14159265 / 180.0) /
                       cos((60 - h) * 3.14159265 / 180.0)));
        r = 3 * in - (g + b);
      }

      if (b < INTENSITY_MIN)
        b = INTENSITY_MIN;
      if (b > INTENSITY_MAX)
        b = INTENSITY_MAX;

      if (g < INTENSITY_MIN)
        g = INTENSITY_MIN;
      if (g > INTENSITY_MAX)
        g = INTENSITY_MAX;

      if (r < INTENSITY_MIN)
        r = INTENSITY_MIN;
      if (r > INTENSITY_MAX)
        r = INTENSITY_MAX;

      outputImage.at<Vec3b>(i, j)[0] = round(b);
      outputImage.at<Vec3b>(i, j)[1] = round(g);
      outputImage.at<Vec3b>(i, j)[2] = round(r);
    }
  }
  return;
}

// Functio to create histogram from intensity value calculated from the RGB to
// HSI function
void intensityToHistogram(float **I, float histogram[], int rows, int cols) {

  for (int i = 0; i < LEVEL; i++) {
    histogram[i] = 0;
  }

  for (int y = 0; y < rows; y++) {
    for (int x = 0; x < cols; x++) {
      histogram[(int)I[y][x]]++;
    }
  }

  for (int i = 0; i < LEVEL; i++) {
    histogram[i] = histogram[i] / (rows * cols);
  }

  return;
}

// Function to match histogram of the input image to the target image
void histogramMatching(float inputTranFunc[], float targetTranFunc[],
                       float histogram[], float targetHistogram[]) {

  for (int i = 0; i < LEVEL; i++) {
    int j = 0;
    do {
      histogram[i] = j;
      j++;
    } while (inputTranFunc[i] > targetTranFunc[j]);
  }
  return;
}

// Function to display histogram of an image and to write the historam in the
// outout file
void showHistogram(Mat &image, string fileName) {
  int bins = 256;                 // number of bins
  int nc = image.channels();      // number of channels
  std::vector<Mat> histogram(nc); // array for storing the histograms
  std::vector<Mat> canvas(nc);    // images for displaying the histogram
  int hmax[3] = {0, 0, 0};        // peak value for each histogram

  // The rest of the code will be placed here
  for (size_t i = 0; i < histogram.size(); i++)
    histogram[i] = Mat::zeros(1, bins, CV_32SC1);

  for (int i = 0; i < image.rows; i++) {
    for (int j = 0; j < image.cols; j++) {
      for (int k = 0; k < nc; k++) {
        uchar val = nc == 1 ? image.at<uchar>(i, j) : image.at<Vec3b>(i, j)[k];
        histogram[k].at<int>(val) += 1;
      }
    }
  }

  for (int i = 0; i < nc; i++) {
    for (int j = 0; j < bins - 1; j++)
      hmax[i] =
          histogram[i].at<int>(j) > hmax[i] ? histogram[i].at<int>(j) : hmax[i];
  }

  const char *wname[3] = {"Blue", "Green", "Red"};
  Scalar colors[3] = {Scalar(255, 0, 0), Scalar(0, 255, 0), Scalar(0, 0, 255)};

  for (int i = 0; i < nc; i++) {
    canvas[i] = Mat::ones(125, bins, CV_8UC3);

    for (int j = 0, rows = canvas[i].rows; j < bins - 1; j++) {
      line(canvas[i], Point(j, rows),
           Point(j, rows - (histogram[i].at<int>(j) * rows / hmax[i])),
           nc == 1 ? Scalar(255, 255, 255) : colors[i], 1, 8, 0);
    }

    imshow(nc == 1 ? fileName : wname[i] + fileName, canvas[i]);
    // string name = string(wname[i])+".jpg";
    // imwrite(nc == 1 ? fileName+".jpg" : name, canvas[i]);
  }
}

Mat readImage(string &fileName, string type) {
  cout << endl << "Please Select " << type << " Image." << endl;
  cout << "Example <grayscale/lena.bmp> <grayscale/Baboon_dull.jpg>" << endl;
  cout << "Example <lena_color.jpg>  <PeppersRGB.bmp>" << endl << endl;
  cin >> fileName;

  //  fileName = "TestImages/" + fileName;
  cout << "File Selected: " << fileName << endl;
  Mat inputImage = imread(fileName, CV_LOAD_IMAGE_UNCHANGED);

  return inputImage;
}

Mat Histogram_Equalization(Mat inputImage) {
  if (inputImage.channels() == 1) {
    cout << "Grayscale Image" << endl;
    int size = inputImage.rows * inputImage.cols;

    float histogram[LEVEL];
    cout << "HISTOGRAM" << endl;
    imageToHistogram(inputImage, histogram, size);
    printHistogram(histogram);

    float tranFunc[LEVEL];
    cout << "CUMULATIVE HISTOGRAM" << endl;
    calTranFunc(histogram, tranFunc);
    printHistogram(tranFunc);

    float outHistogram[LEVEL];
    cout << "OUTPUT HISTOGRAM" << endl;
    intensityMapping(tranFunc, outHistogram);
    for (int i = 0; i < LEVEL; i++) {
      cout << (int)outHistogram[i] << ",";
    }

    Mat outputImage = inputImage.clone();
    for (int y = 0; y < inputImage.rows; y++) {
      for (int x = 0; x < inputImage.cols; x++) {
        outputImage.at<uchar>(y, x) = saturate_cast<uchar>(
            saturate_cast<int>(outHistogram[inputImage.at<uchar>(y, x)]));
      }
    }

    namedWindow("Original Image");
    imshow("Original Image", inputImage);
    showHistogram(inputImage, " Original Histogram");

    namedWindow("Histogram Equilized Image");
    imshow("Histogram Equilized Image", outputImage);
    showHistogram(outputImage, " Equalized Histogram");

    return outputImage;
  }
  if (inputImage.channels() == 3) {
    cout << "RGB Image" << endl;

    Mat inputImageHSI(inputImage.rows, inputImage.cols, inputImage.type());

    float **H = new float *[inputImage.rows];
    float **S = new float *[inputImage.rows];
    float **I = new float *[inputImage.rows];
    for (int i = 0; i < inputImage.rows; i++) {
      H[i] = new float[inputImage.cols];
      S[i] = new float[inputImage.cols];
      I[i] = new float[inputImage.cols];
    }

    convert_RGB_To_HSI(inputImage, inputImageHSI, H, S, I);

    float histogram[LEVEL];
    cout << "HISTOGRAM" << endl;
    intensityToHistogram(I, histogram, inputImage.rows, inputImage.cols);
    printHistogram(histogram);

    float tranFunc[LEVEL];
    cout << "CUMULATIVE HISTOGRAM" << endl;
    calTranFunc(histogram, tranFunc);
    printHistogram(tranFunc);

    float outHistogram[LEVEL];
    cout << "OUTPUT HISTOGRAM" << endl;
    intensityMapping(tranFunc, outHistogram);
    for (int i = 0; i < LEVEL; i++) {
      cout << (int)outHistogram[i] << ",";
    }

    float **outI = new float *[inputImage.rows];
    for (int i = 0; i < inputImage.rows; i++) {
      outI[i] = new float[inputImage.cols];
    }

    for (int i = 0; i < inputImage.rows; i++) {
      for (int j = 0; j < inputImage.cols; j++) {
        outI[i][j] = (int)outHistogram[(int)I[i][j]];
      }
    }

    Mat outputImage(inputImage.rows, inputImage.cols, inputImage.type());
    convert_HSI_To_RGB(outputImage, inputImageHSI, H, S, outI);

    namedWindow("Original Image", CV_WINDOW_AUTOSIZE);
    imshow("Original Image", inputImage);
    showHistogram(inputImage, " Original Histogram");

    namedWindow("HSI Image", CV_WINDOW_AUTOSIZE);
    imshow("HSI Image", inputImageHSI);

    namedWindow("RGB Histogram Equilized Image", CV_WINDOW_AUTOSIZE);
    imshow("RGB Histogram Equilized Image", outputImage);
    showHistogram(outputImage, " Equalized Histogram");

    return outputImage;
  }
}

Mat Histogram_Matching(Mat inputImage, Mat targetImage) {

  if (inputImage.channels() == 1 && targetImage.channels() == 1) {
    int inputSize = inputImage.rows * inputImage.cols;

    float inputHistogram[LEVEL];
    cout << "INPUT HISTOGRAM" << endl;
    imageToHistogram(inputImage, inputHistogram, inputSize);
    printHistogram(inputHistogram);

    float inputTranFunc[LEVEL];
    cout << "INPUT CUMULATIVE HISTOGRAM" << endl;
    calTranFunc(inputHistogram, inputTranFunc);
    printHistogram(inputTranFunc);

    int targetSize = targetImage.rows * targetImage.cols;
    float targetHistogram[LEVEL];
    cout << "TARGET HISTOGRAM" << endl;
    imageToHistogram(targetImage, targetHistogram, targetSize);
    printHistogram(targetHistogram);

    float targetTranFunc[LEVEL];
    cout << "TARGET CUMULATIVE HISTOGRAM" << endl;
    calTranFunc(targetHistogram, targetTranFunc);
    printHistogram(targetTranFunc);

    float outHistogram[LEVEL];
    cout << "OUTPUT HISTOGRAM" << endl;
    histogramMatching(inputTranFunc, targetTranFunc, outHistogram,
                      targetHistogram);
    for (int i = 0; i < LEVEL; i++) {
      cout << outHistogram[i] << ",";
    }

    Mat outputImage = inputImage.clone();

    for (int y = 0; y < inputImage.rows; y++) {
      for (int x = 0; x < inputImage.cols; x++) {
        outputImage.at<uchar>(y, x) =
            (int)(outHistogram[inputImage.at<uchar>(y, x)]);
      }
    }

    namedWindow("Original Image");
    imshow("Original Image", inputImage);
    showHistogram(inputImage, " Original Histogram");

    namedWindow("Target Image");
    imshow("Target Image", targetImage);
    showHistogram(targetImage, " Target Histogram");

    namedWindow("Histogram Matched Image");
    imshow("Histogram Matched Image", outputImage);
    showHistogram(outputImage, " Matched Histogram");

    return outputImage;

  } else {
    Mat inputImageHSI(inputImage.rows, inputImage.cols, inputImage.type());

    float **inputImage_H = new float *[inputImage.rows];
    float **inputImage_S = new float *[inputImage.rows];
    float **inputImage_I = new float *[inputImage.rows];
    for (int i = 0; i < inputImage.rows; i++) {
      inputImage_H[i] = new float[inputImage.cols];
      inputImage_S[i] = new float[inputImage.cols];
      inputImage_I[i] = new float[inputImage.cols];
    }

    convert_RGB_To_HSI(inputImage, inputImageHSI, inputImage_H, inputImage_S,
                       inputImage_I);

    float inputHistogram[LEVEL];
    // cout << "INPUT HISTOGRAM" << endl ;
    intensityToHistogram(inputImage_I, inputHistogram, inputImage.rows,
                         inputImage.cols);
    // printHistogram(inputHistogram);

    float inputTranFunc[LEVEL];
    // cout << "INPUT CUMULATIVE HISTOGRAM" << endl;
    calTranFunc(inputHistogram, inputTranFunc);
    // printHistogram(inputTranFunc);

    Mat targetImageHSI(targetImage.rows, targetImage.cols, targetImage.type());

    float **targetImage_H = new float *[targetImage.rows];
    float **targetImage_S = new float *[targetImage.rows];
    float **targetImage_I = new float *[targetImage.rows];
    for (int i = 0; i < targetImage.rows; i++) {
      targetImage_H[i] = new float[targetImage.cols];
      targetImage_S[i] = new float[targetImage.cols];
      targetImage_I[i] = new float[targetImage.cols];
    }

    convert_RGB_To_HSI(targetImage, targetImageHSI, targetImage_H,
                       targetImage_S, targetImage_I);

    float targetHistogram[LEVEL];
    // cout << "TARGET HISTOGRAM" << endl ;
    intensityToHistogram(targetImage_I, targetHistogram, targetImage.rows,
                         targetImage.cols);
    // printHistogram(targetHistogram);

    float targetTranFunc[LEVEL];
    // cout << "TARGET CUMULATIVE HISTOGRAM" << endl;
    calTranFunc(targetHistogram, targetTranFunc);
    // printHistogram(targetTranFunc);

    float outHistogram[LEVEL];
    // cout << "OUTPUT HISTOGRAM" << endl;
    histogramMatching(inputTranFunc, targetTranFunc, outHistogram,
                      targetHistogram);
    // for(int i = 0 ; i < LEVEL ; i++){
    //      cout << outHistogram[i] << ",";
    //}

    float **outI = new float *[inputImage.rows];
    for (int i = 0; i < inputImage.rows; i++) {
      outI[i] = new float[inputImage.cols];
    }

    for (int i = 0; i < inputImage.rows; i++) {
      for (int j = 0; j < inputImage.cols; j++) {
        outI[i][j] = (int)outHistogram[(int)inputImage_I[i][j]];
      }
    }

    Mat outputImage(inputImage.rows, inputImage.cols, inputImage.type());
    convert_HSI_To_RGB(outputImage, inputImageHSI, inputImage_H, inputImage_S,
                       outI);

    //          namedWindow("Original Image");
    //          imshow("Original Image", inputImage);
    //          showHistogram(inputImage, " Original Histogram");

    //          namedWindow("Target Image");
    //          imshow("Target Image", targetImage);
    //          showHistogram(targetImage, " Target Histogram");

    //          namedWindow("Histogram Matched Image");
    imshow("Histogram Matched Image", outputImage);
    //          showHistogram(outputImage, " Matched Histogram");

    waitKey(-1);
    return outputImage;
  }
}

bool isHidden1(const path &p) {
  string name = p.filename().string();
  if (name != ".." && name != "." && name[0] == '.') {
    return true;
  }

  return false;
}

// Using the available model
// This function is to cluster a number of images inside a folder
// There might be existing some persons inside as noise or rubbis which we need
// to remove

bool copyDir(boost::filesystem::path const &source,
             boost::filesystem::path const &destination) {
  namespace fs = boost::filesystem;
  try {
    // Check whether the function call is valid
    if (!fs::exists(source) || !fs::is_directory(source)) {
      std::cerr << "Source directory " << source.string()
                << " does not exist or is not a directory." << '\n';
      return false;
    }
    if (fs::exists(destination)) {
      std::cerr << "Destination directory " << destination.string()
                << " already exists." << '\n';
      return false;
    }
    // Create the destination directory
    if (!fs::create_directory(destination)) {
      std::cerr << "Unable to create destination directory"
                << destination.string() << '\n';
      return false;
    }
  } catch (fs::filesystem_error const &e) {
    std::cerr << e.what() << '\n';
    return false;
  }
  // Iterate through the source directory
  for (fs::directory_iterator file(source); file != fs::directory_iterator();
       ++file) {
    try {
      fs::path current(file->path());
      if (fs::is_directory(current)) {
        // Found directory: Recursion
        if (!copyDir(current, destination / current.filename())) {
          return false;
        }
      } else {
        // Found file: Copy
        fs::copy_file(current, destination / current.filename());
      }
    } catch (fs::filesystem_error const &e) {
      std::cerr << e.what() << '\n';
    }
  }
  return true;
}

void Clustering_Then_Removing_Minority(string directory) {

  frontal_face_detector detector = get_frontal_face_detector();
  shape_predictor sp;
  deserialize("dlibface_landmark/shape_predictor.dat") >> sp;
  anet_type net;
  deserialize("dlibface_landmark/face_recognition.dat") >> net;
  //    anet_type_vp9 net;
  //    deserialize("dlibface_landmark/lossmetric_vp9facnet.dat") >> net;
  int count = 0;
  path dpath(directory);

  std::vector<string> folderlist;

  BOOST_FOREACH (const path &p, std::make_pair(directory_iterator(dpath),
                                               directory_iterator())) {
    if (is_directory(p) && !isHidden1(p)) {
      std::vector<string> filelist;

      cout << count++ << " : " << p.string() << endl;
      BOOST_FOREACH (const path &sp, std::make_pair(directory_iterator(p),
                                                    directory_iterator())) {
        if (is_directory(sp))
          continue;
        if (is_regular_file(sp) && !isHidden1(sp))
          filelist.push_back(sp.string());
        // cout<<sp.string()<<endl;
      }

      std::vector<matrix<rgb_pixel>> faces;
      for (size_t i = 0; i < filelist.size(); ++i) {
        cv::Mat cvim = cv::imread(filelist[i]);
        // cv::resize(cvim, cvim, cv::Size(), 150, 150);

        // Change to dlib's image format. No memory is copied.
        cv_image<bgr_pixel> img(cvim);

        // Now tell the face detector to give us a list of bounding boxes
        // around all the faces in the image.

        for (auto face : detector(img)) {
          auto shape = sp(img, face);
          matrix<rgb_pixel> face_chip;

          extract_image_chip(img, get_face_chip_details(shape, 150, 0.25),
                             face_chip);
          faces.push_back(move(face_chip));
        }
        // cout<<"Done: " <<filelist[i]<<endl;
      }

      // cout << "number of files and people:
      // "<<filelist.size()<<"===="<<faces.size()<< endl;
      // if (filelist.size()!=faces.size()) cout<<"ERROR WARNING!"<<endl;

      std::vector<matrix<float, 0, 1>> face_descriptors = net(faces);

      // In particular, one simple thing we can do is face clustering.  This
      // next bit of code
      // creates a graph of connected faces and then uses the Chinese whispers
      // graph clustering
      // algorithm to identify how many people there are and which faces belong
      // to whom.
      std::vector<sample_pair> edges;
      for (size_t i = 0; i < face_descriptors.size(); ++i) {
        for (size_t j = i + 1; j < face_descriptors.size(); ++j) {
          // Faces are connected in the graph if they are close enough.  Here we
          // check if
          // the distance between two face descriptors is less than 0.6, which
          // is the
          // decision threshold the network was trained to use.  Although you
          // can
          // certainly use any other threshold you find useful.
          if (length(face_descriptors[i] - face_descriptors[j]) < 0.4)
            edges.push_back(sample_pair(i, j));
        }
      }
      std::vector<unsigned long> labels;
      const auto num_clusters = chinese_whispers(edges, labels);
      // This will correctly indicate that there are 4 people in the image.
      cout << "number of clusters found: " << num_clusters << endl;

      if (num_clusters == 0) { // The number of cluster == number of images, So
                               // remove this folder
        //                folderlist.push_back(p.string());
        boost::filesystem::remove_all(p.string());
        continue;
      }

      if (num_clusters > 1) {
        // copyDir(p.string(),"/home/tucnv/Downloads/Faces/MsCeleV2/"+p.filename().string());
        // continue;

        // Now let's display the face clustering results on the screen.  You
        // will see that it
        // correctly grouped all the faces.
        std::vector<int> group;

        //      std::vector<std::vector<matrix<rgb_pixel>>> group_faces;
        for (size_t cluster_id = 0; cluster_id < num_clusters; ++cluster_id)
          group.push_back(0);

        //            std::vector<image_window> win_clusters(num_clusters);
        for (size_t cluster_id = 0; cluster_id < num_clusters; ++cluster_id) {
          //                std::vector<matrix<rgb_pixel>> temp;
          for (size_t j = 0; j < labels.size(); ++j) {
            if (cluster_id == labels[j]) {
              group[cluster_id]++;
              //                        temp.push_back(faces[j]);
            }
          }
          //          group_faces.push_back(temp);
          //                win_clusters[cluster_id].set_title("face cluster " +
          //                cast_to_string(cluster_id));
          //                win_clusters[cluster_id].set_image(tile_images(temp));
        }

        int max = 0;
        int max_cluster_id = -1;
        for (size_t cluster_id = 0; cluster_id < num_clusters; ++cluster_id) {
          if (group[cluster_id] > max) {
            max = group[cluster_id];
            max_cluster_id = cluster_id;
          }
        }
        cout << "Winning cluster: " << max_cluster_id << " with "
             << group[max_cluster_id] << " faces" << endl;

        //            image_window win_cluster;//Show the cluster with most
        //            faces
        //            win_cluster.set_title("Same");
        // std::vector<matrix<rgb_pixel>> temp;
        for (size_t j = 0; j < labels.size(); ++j) {
          if (max_cluster_id != labels[j]) {
            // delete corresponding file face[j]
            boost::filesystem::remove(filelist[j]);
          }
          // else
          //     temp.push_back(faces[j]);
        }
      }
      //            win_cluster.set_image(tile_images(temp));

      //            cout << "hit anykey to continue" << endl;
      //            cin.get();
    }

    copyDir(p.string(),
            "/home/tucnv/Downloads/Faces/MsCeleV2/" + p.filename().string());
    boost::filesystem::remove_all(p.string());
  }

  // for (size_t j = 0; j < folderlist.size(); ++j)
  //   boost::filesystem::remove_all(folderlist[j]);
}
void Detect_FaceonImage_toSavetoGallery(string newimagepath,
                                        string pathtonameid) {

  frontal_face_detector detector = get_frontal_face_detector();
  shape_predictor sp;
  deserialize("dlibface_landmark/shape_predictor.dat") >> sp;

  matrix<rgb_pixel> face_chip;
  //    std::vector<matrix<rgb_pixel>> faces;

  path dpath(pathtonameid);
  // int count =0;

  BOOST_FOREACH (const path &p, std::make_pair(directory_iterator(dpath),
                                               directory_iterator())) {
    if (is_directory(p))
      continue;
    if (!is_regular_file(p))
      continue;

    dlib::matrix<rgb_pixel> dlibimg;
    load_image(dlibimg, p.string());

    for (auto face : detector(dlibimg)) {
      auto shape = sp(dlibimg, face);
      extract_image_chip(dlibimg, get_face_chip_details(shape, 150, 0.25),
                         face_chip);
      // boost::filesystem::create_directory(pathtonameid);
      string str = newimagepath + "/" + p.filename().string();
      dlib::save_jpeg(face_chip, str);
      //        faces.push_back(move(face_chip));
      break;
    }
  }

  //    cout << faces.size()<< endl;
}

void Measure_Distance_Between_Images(string imagename1, string imagename2) {

  frontal_face_detector detector = get_frontal_face_detector();
  shape_predictor sp;
  deserialize("dlibface_landmark/shape_predictor.dat") >> sp;

  anet_type_vp9 net;
  deserialize("dlibface_landmark/vp9_face_net0.dat") >> net;

  //    anet_type_vp9 net;
  //    deserialize("dlibface_landmark/vp9_face_net1.dat") >> net;
  //    anet_type net;
  //    deserialize("dlibface_landmark/vp9_face_net2.dat") >> net;

  //    anet_type net;
  //    deserialize("dlibface_landmark/face_recognition.dat") >> net;

  dlib::matrix<rgb_pixel> dlibimg1, dlibimg2;
  load_image(dlibimg1, imagename1);
  load_image(dlibimg2, imagename2);

  matrix<rgb_pixel> face_chip1, face_chip2;
  std::vector<matrix<rgb_pixel>> faces;

  for (auto face : detector(dlibimg1)) {
    auto shape = sp(dlibimg1, face);
    extract_image_chip(dlibimg1, get_face_chip_details(shape, 150, 0.25),
                       face_chip1);
    faces.push_back(move(face_chip1));
    break;
  }
  for (auto face : detector(dlibimg2)) {
    auto shape = sp(dlibimg2, face);
    extract_image_chip(dlibimg2, get_face_chip_details(shape, 150, 0.25),
                       face_chip2);
    faces.push_back(move(face_chip2));
    break;
  }

  cout << faces.size() << endl;
  std::vector<matrix<float, 0, 1>> face_descriptors = net(faces);
  cout << length(face_descriptors[0] - face_descriptors[1]) - 0.0 << endl;
}

// To determine the Pose angle of face
void Pose_Angle_Estimation() {

  //    frontal_face_detector detector = get_frontal_face_detector();
  //    shape_predictor sp;
  //    anet_type net;
  //    deserialize("dlibface_landmark/shape_predictor.dat") >> sp;
  //    deserialize("dlibface_landmark/face_recognition.dat") >> net;

  ////    string filename= "facevideo.mp4";
  //    string filename= "rtsp://10.12.11.152:554/stream1";
  //    cv::VideoCapture cap;
  //    if (filename.empty())
  //        cap.open(0);
  //    else
  //        cap.open(filename);

  //    cv::Mat frame, cv_small;
  //    int key=0, count_frame=0;
  //    while( key != 27 ){
  //        cap >> frame;
  //        count_frame++;

  //        if (frame.empty()){
  //            cap.release();
  //            cap.open(filename);
  //            cap >> frame;
  //        }

  //        if (filename[0]=='/')
  //            int a =1;
  //            //do nothing
  //        else{
  //            if (count_frame<2) {
  //                continue;
  //            }else count_frame=0;
  //        }
  //        cv::resize(frame, frame, cv::Size(720,480));

  //        std::vector<cv::Rect> dets;
  //        std::vector<matrix<rgb_pixel>> faces;
  //        //It is said that using grey scale image speeds up detector

  ////        dlib::array2d<unsigned char> im;
  ////        dlib::assign_image(im, frame);

  //        cv::resize(frame, cv_small, cv::Size(), 1.0/FACE_DOWNSAMPLE_RATIO,
  //        1.0/FACE_DOWNSAMPLE_RATIO);

  //        // Change to dlib's image format. No memory is copied.
  //        cv_image<bgr_pixel> im(cv_small);

  //        std::vector<full_object_detection> shapes;

  //        //These values are to use to find the biggest face, for registration
  //        purpose
  //        int max_length = 0;
  //        int biggest_faceindex = -1;
  //        int ind = 0;

  //        for (auto face : detector(im)) //if using grey scale, we speed up
  //        1.5 times
  //        {
  //            cv::Rect rec(
  //                        (long)(face.left() * FACE_DOWNSAMPLE_RATIO),
  //                        //+leftpoint,
  //                        (long)(face.top() * FACE_DOWNSAMPLE_RATIO),
  //                        //+toppoint,
  //                        (long)((face.right()-face.left()) *
  //                        FACE_DOWNSAMPLE_RATIO),
  //                        (long)((face.bottom()-face.top()) *
  //                        FACE_DOWNSAMPLE_RATIO)
  //                        );
  //            dets.push_back(rec);
  //            if (rec.width > max_length){
  //                max_length = rec.width;
  //                biggest_faceindex = ind;
  //                ind++;
  //            }

  //            dlib::rectangle drec(rec.x,rec.y, rec.x+rec.width,
  //            rec.y+rec.height);
  //            auto shape = sp(im, drec);

  //            matrix<rgb_pixel> face_chip;
  //            extract_image_chip(im, get_face_chip_details(shape,150,0.25),
  //            face_chip);
  //            faces.push_back(move(face_chip));
  //            shapes.push_back(shape);
  //        }

  //        Drawing_Landmark(frame,shapes);
  //        //Assume that we have one face only
  //        if (faces.size()>0){
  //            full_object_detection landmarkpoints = shapes[0];

  //            // 2D image points. If you change the image, you need to change
  //            vector
  //            std::vector<cv::Point2d> image_points;
  //            cv::Point2d
  //            nosetip(landmarkpoints.part(30)(0),landmarkpoints.part(30)(1));
  //            cv::Point2d
  //            chin(landmarkpoints.part(8)(0),landmarkpoints.part(8)(1));
  //            cv::Point2d
  //            lefteye_leftcorner(landmarkpoints.part(36)(0),landmarkpoints.part(36)(1));
  //            cv::Point2d
  //            righteye_rightcorner(landmarkpoints.part(45)(0),landmarkpoints.part(45)(1));
  //            cv::Point2d
  //            leftmouth_corner(landmarkpoints.part(48)(0),landmarkpoints.part(48)(1));
  //            cv::Point2d
  //            rightmouth_corner(landmarkpoints.part(54)(0),landmarkpoints.part(54)(1));

  //            cv::Point2d
  //            topnose(landmarkpoints.part(27)(0),landmarkpoints.part(27)(1));
  //            cv::Point2d
  //            leftnose(landmarkpoints.part(31)(0),landmarkpoints.part(31)(1));
  //            cv::Point2d
  //            rightnose(landmarkpoints.part(35)(0),landmarkpoints.part(35)(1));

  //            image_points.push_back( nosetip );    // Nose tip
  //            image_points.push_back( chin );    // Chin
  //            image_points.push_back( lefteye_leftcorner );     // Left eye
  //            left corner
  //            image_points.push_back( righteye_rightcorner );    // Right eye
  //            right corner
  //            image_points.push_back( leftmouth_corner );    // Left Mouth
  //            corner
  //            image_points.push_back( rightmouth_corner );    // Right mouth
  //            corner

  //            // 3D model points.
  //            std::vector<cv::Point3d> model_points;
  //            model_points.push_back(cv::Point3d(0.0f, 0.0f, 0.0f));
  //            // Nose tip
  //            model_points.push_back(cv::Point3d(0.0f, -330.0f, -65.0f));
  //            // Chin
  //            model_points.push_back(cv::Point3d(-225.0f, 170.0f, -135.0f));
  //            // Left eye left corner
  //            model_points.push_back(cv::Point3d(225.0f, 170.0f, -135.0f));
  //            // Right eye right corner
  //            model_points.push_back(cv::Point3d(-150.0f, -150.0f, -125.0f));
  //            // Left Mouth corner
  //            model_points.push_back(cv::Point3d(150.0f, -150.0f, -125.0f));
  //            // Right mouth corner

  //            // Camera internals
  //            double focal_length = frame.cols; // Approximate focal length.
  //            Point2d center = cv::Point2d(frame.cols/2,frame.rows/2);
  //            cv::Mat camera_matrix = (cv::Mat_<double>(3,3) << focal_length,
  //            0, center.x, 0 , focal_length, center.y, 0, 0, 1);
  //            cv::Mat dist_coeffs =
  //            cv::Mat::zeros(4,1,cv::DataType<double>::type); // Assuming no
  //            lens distortion

  //            //        cout << "Camera Matrix " << endl << camera_matrix <<
  //            endl ;
  //            // Output rotation and translation
  //            cv::Mat rotation_vector; // Rotation in axis-angle form
  //            cv::Mat translation_vector;

  //            // Solve for pose
  //            cv::solvePnP(model_points, image_points, camera_matrix,
  //            dist_coeffs, rotation_vector, translation_vector);

  //            // Project a 3D point (0, 0, 1000.0) onto the image plane.
  //            // We use this to draw a line sticking out of the nose

  //            std::vector<Point3d> nose_end_point3D;
  //            std::vector<Point2d> nose_end_point2D;
  //            nose_end_point3D.push_back(Point3d(0,0,1000.0));

  //            projectPoints(nose_end_point3D, rotation_vector,
  //            translation_vector, camera_matrix, dist_coeffs,
  //            nose_end_point2D);

  ////            std::vector<Point3f> axes;
  ////            axes.push_back(Point3f(0,0,0));
  ////            axes.push_back(Point3f(250,0,0));
  ////            axes.push_back(Point3f(0,250,0));
  ////            axes.push_back(Point3f(0,0,250));
  ////            std::vector<Point2f> projected_axes;

  ////            projectPoints(axes, rotation_vector, translation_vector,
  /// camera_matrix, dist_coeffs, projected_axes);

  ////            line(frame, projected_axes[0], projected_axes[3],
  /// Scalar(255,0,0),2,CV_AA);
  ////            line(frame, projected_axes[0], projected_axes[2],
  /// Scalar(0,255,0),2,CV_AA);
  ////            line(frame, projected_axes[0], projected_axes[1],
  /// Scalar(0,0,255),2,CV_AA);

  //            for (auto point : nose_end_point2D) {
  //                circle(frame, point,2, Scalar(0,255,255),2);
  //            }

  //            for(int i=0; i < image_points.size(); i++)
  //            {
  //                circle(frame, image_points[i], 3, Scalar(0,0,255), -1);
  //            }

  //                    cv::line(frame,image_points[0], nose_end_point2D[0],
  //                    cv::Scalar(0,255,0), 2);
  //            //        cv::line(frame,topnose, nose_end_point2D[0],
  //            cv::Scalar(0,255,0), 2);
  //            //        cv::line(frame,leftnose, nose_end_point2D[0],
  //            cv::Scalar(0,255,0), 2);
  //            //        cv::line(frame,rightnose, nose_end_point2D[0],
  //            cv::Scalar(0,255,0), 2);

  //            //        cout << "Rotation Vector " << endl << rotation_vector
  //            << endl;

  //            //cv::putText(frame, "(" + to_string((int)
  //            (translation_vector.at<double>(0) / 100)) + "cm, " +
  //            to_string((int) (translation_vector.at<double>(1) / 100)) + "cm,
  //            " + to_string((int) (translation_vector.at<double>(2) / 100)) +
  //            "cm)", image_points[0], FONT_HERSHEY_SIMPLEX, 0.5,
  //            Scalar(0,0,255),2);

  //            cout << "Translation Vector" << endl << translation_vector <<
  //            endl;

  //            //        cout <<  nose_end_point2D << endl;
  //        }
  //        // Display image.
  //        cv::imshow("Output", frame);
  //        key = cv::waitKey(1);

  //    }
}

void MovingScene_Sensitive() {
  int key;
  Ptr<BackgroundSubtractor> pBgSub;
  Ptr<BackgroundSubtractorMOG2> pBgSubMOG2 = createBackgroundSubtractorMOG2();

  pBgSubMOG2->setDetectShadows(true);
  pBgSubMOG2->setNMixtures(3);
  pBgSubMOG2->setShadowThreshold(0.8);
  pBgSubMOG2->setShadowValue(0);
  pBgSub = pBgSubMOG2;
  //    string filename ="VGGFace/Data/gallery/anhvh0.png";
  // string filename= "rtsp://10.11.11.38:554/av0_0";
  string filename = "rtsp://192.168.0.13:554/av0_0";

  //    string filename= "/home/tucnv/facevideo.mp4";
  //    string filename= "/home/tucnv/Qt/FeatureExtract/facevideo1.mp4";
  //    string filename= "http://10.11.11.234/live/nal/0/005a20528ad8xyz14353";

  cv::Mat frame, frame_origin;
  cv::VideoCapture cap;
  if (filename.empty())
    cap.open(0);
  else
    cap.open(filename);

  cap >> frame_origin;
  bool useROI = false;
  int leftpoint = useROI == true ? (int)frame_origin.cols / 1.8 : 0;
  int toppoint = useROI == true ? (int)frame_origin.rows * 3 / 5 : 0;
  int rightpoint = useROI == true ? leftpoint + 600 : frame_origin.cols;
  int bottompoint = useROI == true ? toppoint + 300 : frame_origin.rows;
  cv::Rect rec(leftpoint, toppoint, rightpoint - leftpoint,
               bottompoint - toppoint);

  double dWidth =
      cap.get(CV_CAP_PROP_FRAME_WIDTH); // get the width of frames of the video
  double dHeight = cap.get(
      CV_CAP_PROP_FRAME_HEIGHT); // get the height of frames of the video
  //      cout << "Frame Size = " << dWidth << "x" << dHeight << endl;
  Size frameSize(static_cast<int>(dWidth), static_cast<int>(dHeight));
  VideoWriter oVideoWriter("MoveSave.avi", CV_FOURCC('D', 'I', 'V', 'X'), 24,
                           frameSize,
                           true); // initialize the VideoWriter object

  int saveframeid = 0;
  while (key != 27) {
    cap >> frame_origin;
    if (frame_origin.cols == 0 || frame_origin.rows == 0)
      continue;

    Mat1b fgMask;

    if (useROI)
      frame = frame_origin(rec);
    else {
      double ratioPyramid = 2;
      cv::resize(frame_origin, frame,
                 Size((int)frame_origin.cols / ratioPyramid,
                      (int)frame_origin.rows / ratioPyramid));
    }

    Mat kernel = getStructuringElement(MORPH_RECT, Size(2, 2));

    Mat gray;
    cvtColor(frame, gray, COLOR_BGR2GRAY);

    pBgSub->apply(gray, fgMask);

    // Clean foreground from noise
    morphologyEx(fgMask, fgMask, MORPH_OPEN, kernel);

    // Find contours
    std::vector<std::vector<Point>> contours;
    findContours(fgMask.clone(), contours, CV_RETR_TREE,
                 CV_CHAIN_APPROX_SIMPLE);

    bool checkmoving = false;
    if (!contours.empty()) {
      // Get largest contour

      for (int i = 0; i < (int)contours.size(); ++i) {
        double area = contourArea(contours[i]);

        // std::cout <<frameid++<<" : "<< area <<std::endl;
        if (area > 100) {
          checkmoving = true;
          break;
        }
      }
    }

    if (checkmoving) {
      oVideoWriter.write(frame_origin);
      saveframeid++;
    }

    if (saveframeid >= 200000)
      return;
  }
  cout << "Done!" << endl;
}
void CallBackFunc(int event, int x, int y, int flags, void *userdata) {
  if (event == EVENT_LBUTTONDOWN) {
    cout << "Left button of the mouse is clicked - position (" << x << ", " << y
         << ")" << endl;
    Point3_<int> *mouseInputs = (Point3_<int> *)userdata;
    mouseInputs->x = x;
    mouseInputs->y = y;
    mouseInputs->z = event;
    //          std::cout<<"enter string: ";
    //          std::cin>>s;
  }
  if (event == EVENT_LBUTTONUP) {
    cout << "Left button up of the mouse is clicked - position (" << x << ", "
         << y << ")" << endl;
    Point3_<int> *mouseInputs = (Point3_<int> *)userdata;
    mouseInputs->x = x;
    mouseInputs->y = y;
    mouseInputs->z = event;
    //          std::cout<<"enter string: ";
    //          std::cin>>s;
  }
  //     else if  ( event == EVENT_RBUTTONDOWN )
  //     {
  //          cout << "Right button of the mouse is clicked - position (" << x
  //          << ", " << y << ")" << endl;
  //     }
  //     else if  ( event == EVENT_MBUTTONDOWN )
  //     {
  //          cout << "Middle button of the mouse is clicked - position (" << x
  //          << ", " << y << ")" << endl;
  //     }
  //     else if ( event == EVENT_MOUSEMOVE )
  //     {
  //          cout << "Mouse move over the window - position (" << x << ", " <<
  //          y << ")" << endl;
  //     }
}

void initParameters() {
  d2 = (int)2 * FRAME_WIDTH / 1280;
  d3 = (int)3 * FRAME_WIDTH / 1280;
}

void textCentered(cv::InputOutputArray img, const std::string &text, int x1,
                  int _width, int y, int fontFace, double fontScale,
                  cv::Scalar color, int thickness, int lineType,
                  bool bottomLeftOrigin, bool background) {
  int baseline = 0;
  cv::Size _text_size = cv::getTextSize(text, cv::FONT_HERSHEY_TRIPLEX,
                                        fontScale, thickness, &baseline);
  long x = x1 + (_width - _text_size.width) / 2;

  if (background) {
    cv::Mat overlay;
    double alpha = 0.3;

    // copy the source image to an overlay
    img.copyTo(overlay);

    // draw a filled, yellow rectangle on the overlay copy
    cv::rectangle(overlay, cv::Rect(x - d2, y - _text_size.height - d2,
                                    _text_size.width + 2 * d2,
                                    _text_size.height + 2 * d2),
                  cv::Scalar(0, 125, 125), -1);

    // blend the overlay with the source image
    cv::addWeighted(overlay, alpha, img, 1 - alpha, 0, img);
  }

  cv::putText(img, text, cv::Point(x, y), fontFace, fontScale, color, thickness,
              lineType, bottomLeftOrigin);
}
