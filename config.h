#ifndef _CONFIG_H_
#define _CONFIG_H_

#include <opencv2/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

// Frame resolution
// HD
extern int FRAME_WIDTH;
extern int FRAME_HEIGHT;

// Recognition area
extern int AREA_TEXT_DY;
extern float area_name_font_scale;

// For moving recognition area
// moving up & down step
#define RECOGNITION_UP_DOWN_STEP 10

#endif // _CONFIG_H_
