#include <dlib/image_processing.h>
#include <dlib/image_processing/frontal_face_detector.h>
#include <dlib/opencv.h>

#include <queue>
#include <string.h>

#include "Ctracker.hpp"
#include "Dtracker.hpp"
#include "Models.hpp"
#include "RecentFaceList.hpp"
#include "UserFacialInfor.hpp"
#include <boost/filesystem.hpp>

// #include "dbinfo.h"

#define FACE_DOWNSAMPLE_RATIO 1.0
#define SKIP_FRAMES 1

//
// ----------------------------------------------------------------------------------------
struct ImageInfo {
  std::string url;
  cv::Mat image;
  dlib::matrix<float, 0, 1> vec128;
};
class MatchAgainstGallery {
public:
  // string filefeature;
  string folderpath;
  string trainingimagepath;

  // std::vector<std::vector<float>> outputs;
  std::map<string, int> unique_faceid_list;
  std::map<string, User_Infor> output_inforlist;
  std::map<string, User_Facial_Infor> output_person_facial_inforlist;
  std::map<string, std::map<string, matrix<float, 0, 1>>>
      gallery_face_descriptors; //
                                // this
                                // and
                                // outputs
                                // can
                                // be
                                // used
                                // interchangably
  std::map<std::string, std::map<std::string, int>> faceid_pose_count;

  float threshold;
  PredictionModels *models;

  string inputstring;
  cv::Mat frame;
  bool use_multiface_samples_autosave;
  int max_numberof_faceimages_for1faceid;

  int maxOfVec;
  std::vector<ImageInfo> savedImage;

public:
  explicit MatchAgainstGallery();
  ~MatchAgainstGallery() {}

  void Set_Single_Level_Detector(frontal_face_detector &detector_);

  void Init_Gallery_Folder_Based();

  void updateProfile(std::string img_url, std::string face_id,
                     dlib::matrix<float, 0, 1> vector);

  string Calculate_ACC_Rate(float x);
  std::vector<matrix<rgb_pixel>> Jitter_Image(const matrix<rgb_pixel> &img,
                                              int number);
  std::vector<matrix<float, 0, 1>>
  Jitter_Based_Testing(matrix<rgb_pixel> image);

  User_Infor Load_User_Infor(string folder_faceid);
  void Load_User_Infor_From_DB(string folder_faceid, User_Infor &userinfor);

  // void Write_User_Infor_To_DB(string folder_faceid);

  void SingleFace_LookUp_Gallery(Mat &frame, dlib::rectangle d_roi,
                                 Detected_Faces_List &detected_face_list);
  void MultiFace_LookUp_Gallery(Mat &frame, dlib::rectangle d_roi,
                                Detected_Faces_List &detected_face_list,
                                DtrackerManager &dtracker_manager,
                                int count_frame);

  bool One_Session_Face_Scan(cv::Mat &frame, CTracker &tracker);

  RecentFaceInfo
  Matching_OneFace_Against_Gallery(matrix<float, 0, 1> face_descriptors);

  std::string Matching_OneFace_Against_Gallery_FacePoseDriven(
      matrix<float, 0, 1> face_descriptor, string file_prefix);

  bool Add_More_FaceImages_That_Have_Certain_Distance(
      matrix<float, 0, 1> face_descriptors, string face_id);
  std::string getPose(std::string file_name);

  void ProcessVideo_MultiFace_ThangMay_VP9();

  inline int getNumberOfPose() { return pose_list.size(); }

  inline std::string getPose(int i) {
    return ((i >= 0) && (i < pose_list.size())) ? pose_list.at(i) : "";
  }

  int getPoseCount(std::string face_id, std::string pose);
  inline float getMaxDistance() { return max_distance_ever; }
  int convertDistanceToPercent(float _d);

  void readFromBinary(string file_path);

  int getNewProfile(std::string URL);

private:
  float HISTOGRAM_X0;
  float HISTOGRAM_X1;
  int HISTOGRAM_N;
  float HISTOGRAM_DX;
  std::vector<std::string> pose_list;

  int GRAPH_N;
  float max_distance_ever;
};
