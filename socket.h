#ifndef SOCKET_H
#define SOCKET_H
#include "SocketIoUtils.hpp"
#include <QMutex>
#include <QString>
#include <QThread>
#include <QWaitCondition>
using namespace sio;
class Socket : public QThread {
  Q_OBJECT
public:
  Socket(QObject *parent = 0);
  // Destructor
  ~Socket();
  //  void setUrl(QString url);
  void connect();

protected:
  void run();

private:
  QMutex mutex;
  QWaitCondition condition;

  QString socketUrl;
  SocketIoUtils socket;
signals:

  void newProfile(QVector<QString> listUrl, QString faceId);
};

#endif // SOCKET_H
