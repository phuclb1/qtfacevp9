#include "config.h"
#include <opencv2/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <dlib/dnn.h>
#include <dlib/image_processing.h>
#include <dlib/image_processing/frontal_face_detector.h>
#include <dlib/opencv.h>

#include "Models.hpp"

using namespace dlib;
using namespace cv;
using namespace std;

PredictionModels::PredictionModels() {
  detector = get_frontal_face_detector();
  // Set_Single_Level_Detector(detector_onecascade);
  // Set_Single_Level_Detector(detector);

  deserialize("dlibface_landmark/shape_predictor.dat") >> sp;

  deserialize("dlibface_landmark/face_recognition.dat") >> net;
  // deserialize("dlibface_landmark/vp9_face_net2.dat") >> net;
  net_threshold = 0.30; // 0.35; // to decide the faceid if distance smaller
  // than this value

  use_track_on_detection_and_recognition = true;
  use_track_on_detection = true;
  number_frames_only_track = 6;
}

void PredictionModels::Set_Single_Level_Detector(
    frontal_face_detector &detector_) {
  detector_ = get_frontal_face_detector();

  // dlib::serialize("current.svm")<< detector;

  std::vector<frontal_face_detector> parts;
  // Split into parts and serialize to disk
  for (unsigned long i = 0; i < detector_.num_detectors(); ++i) {
    dlib::frontal_face_detector part(detector_.get_scanner(),
                                     detector_.get_overlap_tester(),
                                     detector_.get_w(i));
    // dlib::serialize("part" + std::to_string(i) + ".svm") << part;

    //    if (i == 0 || i == 3 || i == 4) { // Seem that this is frontal one
    if (i == 0) { // Seem that this is frontal one
      parts.push_back(part);
      // break;
    }
  }

  // Reconstruct original detector
  frontal_face_detector reconstructed(parts);
  // dlib::serialize("reconstructed.svm") << reconstructed;

  // Create detector that will work only on one level of pyramid
  typedef dlib::scan_fhog_pyramid<dlib::pyramid_down<6>> image_scanner_type;
  image_scanner_type scanner;
  scanner.copy_configuration(reconstructed.get_scanner());
  scanner.set_max_pyramid_levels(7); // try setting to 2, 3...
  // scanner.set_min_pyramid_layer_size();
  frontal_face_detector one_level_detector =
      dlib::object_detector<image_scanner_type>(
          scanner, reconstructed.get_overlap_tester(), reconstructed.get_w());

  detector_ = one_level_detector;
}

///
/// \brief MatchAgainstGallery::Face_and_Shape_Detection --> implement the face
/// detection and landmark detection, all results stored in detected_face_list
/// \param img
/// \param d_roi
/// \param set_single_level_face_detector
/// \param detected_face_list
///
void PredictionModels::Face_and_Shape_Detection(
    cv_image<bgr_pixel> img, dlib::rectangle d_roi,
    Detected_Faces_List &detected_face_list) {

  dlib::matrix<rgb_pixel> img_roi;
  chip_details chip(d_roi);
  extract_image_chip(img, chip, img_roi);

  // Sure to use grey scale
  dlib::array2d<unsigned char> img_gray;
  dlib::assign_image(img_gray, img_roi);

  for (auto face :
       detector(img_gray)) { // if using grey scale, we speed up 1.5 times
    cv::Rect rec((long)(face.left() + d_roi.left()),
                 (long)(face.top() + d_roi.top()),
                 (long)((face.right() - face.left())),
                 (long)((face.bottom() - face.top())));
    detected_face_list.det_list.push_back(rec);

    dlib::rectangle drec(rec.x, rec.y, rec.x + rec.width, rec.y + rec.height);
    auto shape = sp(img, drec);

    matrix<rgb_pixel> face_chip;
    extract_image_chip(img, get_face_chip_details(shape, 150, 0.25), face_chip);

    detected_face_list.face_list.push_back(move(face_chip));
    detected_face_list.shape_list.push_back(shape);
  }

  // std::cout<<detected_face_list.face_list.size()<<endl;
}

void PredictionModels::Face_and_Shape_Detection_With_Track(
    cv_image<bgr_pixel> img, dlib::rectangle d_roi,
    Detected_Faces_List &detected_face_list, DtrackerManager &dtrackers,
    int count_frame) {

  dlib::matrix<rgb_pixel> img_roi;
  chip_details chip(d_roi);
  extract_image_chip(img, chip, img_roi);

  // Sure to use grey scale
  dlib::array2d<unsigned char> img_gray;
  dlib::assign_image(img_gray, img_roi);
  // cout << "Size:" << dtrackers.getTrackerList().size() << endl;

  // check if we can delete track which has low quality
  if (dtrackers.getTrackerList().size() > 0) {
    for (size_t i = 0; i < dtrackers.getTrackerList().size(); ++i) {
      dtrackers.getTrackerList()[i].doSingleTrack(img_gray);
      // cout << "id" << ptr.get()->getTrackID() << endl;
      // cout << "Quality" << ptr.get()->getTrackQuality() <<
      // endl;
      if (dtrackers.getTrackerList()[i].isNeedToDeleted()) {
        dtrackers.deleteTracker(dtrackers.getTrackerList()[i].getTrackID());
      }
    }
  }
  // push all resulting face frome tracks to detected_face_list, calculate the
  // shape and align face
  if (dtrackers.getTrackerList().size() > 0) {
    for (size_t i = 0; i < dtrackers.getTrackerList().size(); ++i) {
      dlib::rectangle det = dtrackers.getTrackerList()[i].getRect();

      cv::Rect rec((long)(det.left() + d_roi.left()),
                   (long)(det.top() + d_roi.top()), (long)(det.width()),
                   (long)(det.height()));
      dtrackers.getTrackerList()[i].cvRect = rec;
      detected_face_list.det_list.push_back(rec);
      dlib::rectangle drec(rec.x, rec.y, rec.x + rec.width, rec.y + rec.height);
      auto shape = sp(img, drec);

      matrix<rgb_pixel> face_chip;
      extract_image_chip(img, get_face_chip_details(shape, 150, 0.25),
                         face_chip);

      detected_face_list.face_list.push_back(move(face_chip));
      detected_face_list.shape_list.push_back(shape);
    }
  }

  // now sometime we update tracks with new detection results
  if (count_frame % number_frames_only_track == 0) {
    std::vector<dlib::rectangle> dets = detector(img_gray);
    for (dlib::rectangle d : dets) {
      int find_matched_track_id = dtrackers.findMatchedTracker(d);
      dtrackers.insertTracker(
          img_gray, d, find_matched_track_id); // insert new track or update
                                               // current track with new det
    }
  }
}
///
/// \brief MatchAgainstGallery::SingleFace_Recognition --> given the
/// detected_face_list, return the all cur_sth
/// \param frame
/// \param img
/// \param d_roi
/// \param detected_face_list
///
void PredictionModels::Face_Recognition(Mat &frame, dlib::rectangle d_roi,
                                        Detected_Faces_List &detected_face_list,
                                        bool multiple_face) {
  // Claim all the detected_face_list from the image
  cv_image<bgr_pixel> img(frame);
  Face_and_Shape_Detection(img, d_roi, detected_face_list);
  // Remove_BackgroundNoise(frame, detected_face_list.face_list,
  // detected_face_list.shape_list, detected_face_list.det_list);

  if (detected_face_list.face_list.size() == 0)
    return;

  int max_length = 0;
  int biggest_faceindex = -1;
  int ind = 0;

  if (multiple_face == false) { // Only consider the largest face
    for (auto det : detected_face_list.det_list) {
      if (det.width > max_length) {
        max_length = det.width;
        biggest_faceindex = ind;
      }
      ind++;
      // detected_face_list.face_id_list.push_back("");
      RecentFaceInfo _face_info;
      _face_info.face_id = "";
      _face_info.score = 0;
      _face_info.max_distance_other_face_id = 0;
      detected_face_list.face_info_list.push_back(_face_info);
    }

    matrix<float, 0, 1> face_descriptor;
    face_descriptor = net(detected_face_list.face_list[biggest_faceindex]);

    // Only add a single vector here
    detected_face_list.face_discriptor_list.push_back(face_descriptor);
    detected_face_list.biggest_face_index = biggest_faceindex;
  } else { // Store all the face, does not matter what sizes
    // std::vector<matrix<float,0,1>> face_descriptors;
    detected_face_list.face_discriptor_list = net(detected_face_list.face_list);
  }
}

// This function uses the correlation track attached
void PredictionModels::Face_Recognition_With_Track(
    Mat &frame, dlib::rectangle d_roi, Detected_Faces_List &detected_face_list,
    bool multiple_face, DtrackerManager &dtrackers, int count_frame) {
  bool debug = false;
  if (debug) {
    std::cout
        << "PredictionModels::Face_Recognition_With_Track >> count_frame = "
        << count_frame << std::endl;
  }
  // Claim all the detected_face_list from the image
  cv_image<bgr_pixel> img(frame);

  if (this->use_track_on_detection) {
    Face_and_Shape_Detection_With_Track(img, d_roi, detected_face_list,
                                        dtrackers, count_frame);
  } else {
    Face_and_Shape_Detection(img, d_roi, detected_face_list);
  }
  // Remove_BackgroundNoise(frame, detected_face_list.face_list,
  // detected_face_list.shape_list, detected_face_list.det_list);

  if (detected_face_list.face_list.size() == 0)
    return;

  int max_length = 0;
  int biggest_faceindex = -1;
  int ind = 0;

  if (multiple_face ==
      false) { // Only consider the largest face -> USED FOR TRAINING AREA
    for (auto det : detected_face_list.det_list) {
      if (det.width > max_length) {
        max_length = det.width;
        biggest_faceindex = ind;
      }
      ind++;
      // detected_face_list.face_id_list.push_back("");
      RecentFaceInfo _face_info;
      _face_info.face_id = "";
      _face_info.score = 0;
      _face_info.max_distance_other_face_id = 0;
      detected_face_list.face_info_list.push_back(_face_info);
    }

    matrix<float, 0, 1> face_descriptor;
    face_descriptor = net(detected_face_list.face_list[biggest_faceindex]);

    // Only add a single vector here
    detected_face_list.face_discriptor_list.push_back(face_descriptor);
    detected_face_list.biggest_face_index = biggest_faceindex;

    if (debug) {
      std::cout << "PredictionModels::Face_Recognition_With_Track >> 2 >> "
                << detected_face_list.face_list.size() << " - "
                << detected_face_list.face_info_list.size() << std::endl;
    }
  } else { // Store all the face, does not matter what sizes -> USED FOR
           // RECOGNITION AREA
    // std::vector<matrix<float,0,1>> face_descriptors;
    if (this->use_track_on_detection_and_recognition) {
      if (count_frame % number_frames_only_track == 0) {
        if (debug) {
          std::cout
              << "PredictionModels::Face_Recognition >> 0 >> count_frame = "
              << count_frame << std::endl;
        }
        detected_face_list.face_discriptor_list =
            net(detected_face_list.face_list);
      } else {
        for (auto det : detected_face_list.det_list) {
          if (det.width > max_length) {
            max_length = det.width;
            biggest_faceindex = ind;
          }
          ind++;
          // detected_face_list.face_id_list.push_back("");
          RecentFaceInfo _face_info;
          _face_info.face_id = "";
          _face_info.score = 0;
          _face_info.max_distance_other_face_id = 0;
          detected_face_list.face_info_list.push_back(_face_info);
        }
        if (debug) {
          std::cout
              << "PredictionModels::Face_Recognition >> 1 >> count_frame = "
              << count_frame << std::endl;
        }
        for (size_t i = 0; i < detected_face_list.det_list.size(); ++i) {
          matrix<float, 0, 1> dsc_face;
          detected_face_list.face_discriptor_list.push_back(dsc_face);
        }
      }
      if (debug) {
        std::cout << "PredictionModels::Face_Recognition_With_Track >> 3 >> "
                  << detected_face_list.face_list.size() << " - "
                  << detected_face_list.face_info_list.size() << std::endl;
      }
    } else { // if not use_track_on_recognition
      detected_face_list.face_discriptor_list =
          net(detected_face_list.face_list);
      if (debug) {
        std::cout << "PredictionModels::Face_Recognition_With_Track >> 4 >> "
                  << detected_face_list.face_list.size() << " - "
                  << detected_face_list.face_info_list.size() << std::endl;
      }
    }
  }
}
