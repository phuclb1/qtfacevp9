#ifndef COMMON_HPP
#define COMMON_HPP

#include <dlib/image_processing.h>
#include <stdio.h>
#include <opencv2/core.hpp>
#include <boost/filesystem.hpp>

using namespace boost::filesystem;
using namespace dlib;
using namespace std;
using namespace cv;

class User_Infor{
public:
    string faceid;
    string name;
    string gender;
    string age;
    string height; //cm
    string other;    

public:
   User_Infor(){
       //faceid = "";
       name = "";
       gender="";
       age="";
       height="";
       other="";
   }
};

class User_Facial_Infor{
public:
    string faceid;
    string direction_h;     //store text left or right or empty for straight face
    string direction_v;     //store text up or down or empty for straight face
    int angle_h;            //The corresponding angle
    int angle_v;
    bool smiling;           //true if you are smiling (when smiling, eyes narrower and mouth larger)
    bool eyeopening;         //true if you are openning both eyes, if eyes close, we do not save images, because this is noise
    bool saved_straight_face;
    bool saved_right30_face;
    bool saved_right45_face;
    bool saved_left30_face;
    bool saved_left45_face;
    bool saved_up30_face;
    bool saved_down30_face;
    bool saved_smiling_face;

    int max_number_of_faces; //to save

    //If both of following increase --> smiling
    //float eye_open_rate;    // = distance (between 2 eyes) / distance (upper_eye_point to lower_eye_point)
    float mouth_open_rate; // = distance (left_mouth_point to right_mouth_point) / distance (between 2 eyes)
    //Should add more slots here to cover more detail of a person's face

public:
     User_Facial_Infor();
     User_Facial_Infor(matrix<rgb_pixel> first_image_saving, string folderpath, string filename);
     bool Save_More_FaceImage_For1FaceID(std::map<string,matrix<float,0,1>>& gallery_face_descriptor,
                                         matrix<float,0,1> vector_face_descriptor, matrix<rgb_pixel> image_saving,
                                         string folderpath, string file_prefix);
     bool Is_Looking_Straight();
     bool Existing_FaceImage_InGallery(std::vector<bool>& index_saved_face);
     bool Is_Enough_Face_Samples(std::vector<bool>& index_saved_face);
     void Second_Image_Saving(matrix<rgb_pixel> second_image_saving, string folderpath);
     void Update_Saved_xxx_Face(path p);
     void Update_Smile_Ratio(const full_object_detection shape);
     bool Is_Smiling_Face(const full_object_detection shape);
     bool Is_Eyes_Openning(const full_object_detection shape);
     bool Calculate_Angle_Direction_Landmarkbased(const full_object_detection shape);
};

#endif // COMMON_HPP
