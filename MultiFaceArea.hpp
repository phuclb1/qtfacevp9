#ifndef MULTIFACEAREA_HPP
#define MULTIFACEAREA_HPP

#include <dlib/opencv.h>

#include <opencv2/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <dlib/image_processing.h>
#include <dlib/image_processing/frontal_face_detector.h>

#include <queue>
#include <string.h>

#include "Models.hpp"
#include "RecentFaceList.hpp"
#include "SingleFaceArea.hpp"
#include "UserFacialInfor.hpp"
#include <boost/filesystem.hpp>

#include <QObject>

#define MAX_LIST_TEMP_UNKOWN_FACE 200
// ----------------------------------------------------------------------------------------

class Multi_Face_Area : public QObject {
  Q_OBJECT
public:
  int max_length_recent_faces_list;
  std::vector<std::pair<string, RecentFacesPresent>> recent_faces_list;
  std::vector<std::pair<string, RecentFacesPresent>> recent_faceid_time_list;

  // recent_face_time[face_id] = queue([last_begin, last_time])
  std::map<std::string, std::queue<std::pair<time_t, time_t>>> recent_face_time;

  std::map<std::string, int> recent_face_count;

  dlib::rectangle recognition_roi;

  MatchAgainstGallery *match_gallery;

  std::vector<std::string>
      temporary_store_unknown_faces; // store facefilename_auto
  // store facefilename_auto and 128 vector for fast looking up
  std::map<std::string, dlib::matrix<float, 0, 1>>
      temporary_store_unknown_faces_lookup;

public:
  // All of these functions are for many image ROI
  explicit Multi_Face_Area();
  ~Multi_Face_Area();
  void init(int rrl, int rrt, int rrr, int rrb);
  void move(int dx, int dy);
  void Add_Face_to_Recent_Face_List(Mat &frame,
                                    Detected_Faces_List detect_face_list,
                                    string folderpath);
  void Drawing_Stuff(cv::Mat &frame, Detected_Faces_List detected_face_list);
  void DrawHistogram(cv::Mat &frame, int _x0, int _y0,
                     std::vector<int> _histogram);
  void DrawGraph(cv::Mat &frame, int _x0, int _y0, std::vector<int> _graph,
                 float _max_value);
  inline void setRandom(int _random) { random = _random; }
  inline void setShowPose(int _show_pose) { show_pose = _show_pose; }
  std::string getPose(Detected_Faces_List detected_face_list, int i);

  void Temporary_Store_Unknown_Faces(Detected_Faces_List detected_face_list);
  void Add_New_Face_Profile_From_List(
      std::string faceid_received,
      std::vector<std::string> list_facefilename_autos_received);

Q_SIGNALS:
  void faceAdded(const QString &face_id, const QImage &face_image);

private:
  int random;
  int show_pose;

  int recent_face_size;
  int recent_face_padding_top_bottom;
  int recent_face_padding_top_1;
  int recent_face_padding_top_2;
  int recent_face_padding_top_3;
  int recent_face_padding_top_4;
  int recent_face_padding_left_1;
  int recent_face_padding_left_2;
  int recent_face_padding_left_3;
  int recent_face_padding_left_4;
  int recent_face_padding_top_max;
  int recent_face_padding_left_max;
  int recent_face_detected_padding_top;
  int recent_face_detected_padding_left;

  int histogram_x0;
  int histogram_y0;
  int histogram_column_width;
  int histogram_column_dx;
  int histogram_column_max_height;
  cv::Scalar histogram_color_0;
  cv::Scalar histogram_color_1;

  std::vector<cv::Scalar> graph_color;

  double recent_face_font_scale;

  int info_x0;
  int info_y0;
  int info_dy;
  std::vector<std::string> info;
  std::vector<cv::Scalar> info_color;

  int info_x1;
  int info_y1;
  std::vector<std::string> info_1;
};

#endif // MULTIFACEAREA_HPP
