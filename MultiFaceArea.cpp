#include "config.h"

#include <dlib/opencv.h>

#include <opencv2/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <dlib/image_processing.h>
#include <dlib/image_processing/frontal_face_detector.h>

#include <queue>
#include <string.h>

#include "MultiFaceArea.hpp"
#include "utils.hpp"
#include <boost/filesystem.hpp>

#include <QImage>

#define MAX_ABSENT_TIME 300
#define MIN_UPDATE_TIME 5
#define MAX_TIME_QUEUE_SIZE 1000

using namespace boost::filesystem;
using namespace dlib;
using namespace std;
using namespace cv;

Multi_Face_Area::Multi_Face_Area() : QObject() {
  random = 0;
  show_pose = 0;
  max_length_recent_faces_list = 6;
  recent_faces_list.clear();
  recent_faceid_time_list.clear();
  recent_face_time.clear();
  recent_face_count.clear();

  //@ Recent face list - gallery:
  recent_face_size = (int)FRAME_HEIGHT * 70 / 720;
  recent_face_padding_top_bottom = (int)FRAME_HEIGHT * 35 / 720;

  recent_face_padding_top_1 = (int)FRAME_HEIGHT * (-18) / 720;
  recent_face_padding_left_1 = (int)FRAME_WIDTH * 12 / 1280;
  recent_face_padding_top_2 = (int)FRAME_HEIGHT * (-6) / 720;
  recent_face_padding_left_2 = (int)FRAME_WIDTH * 13 / 1280;
  recent_face_padding_top_3 = (int)FRAME_HEIGHT * (-10) / 720;
  recent_face_padding_left_3 = recent_face_size; // + recent_face_padding_left_1
                                                 // + (int) FRAME_WIDTH * 12 /
                                                 // 1280;
  recent_face_padding_top_4 =
      -recent_face_size / 2 - recent_face_padding_top_bottom;
  recent_face_padding_left_4 =
      2 * recent_face_size + (int)FRAME_WIDTH * 5 / 1280;
  recent_face_padding_top_max = recent_face_padding_top_3;
  recent_face_padding_left_max =
      2 * recent_face_size - (int)FRAME_WIDTH * 20 / 1280;
  recent_face_detected_padding_top = (int)FRAME_HEIGHT * (-40) / 720;
  recent_face_detected_padding_left = recent_face_padding_left_1;

  histogram_x0 = (int)recent_face_size + FRAME_WIDTH * 27 / 1280;
  histogram_y0 = (int)FRAME_HEIGHT * (-10) / 720;
  histogram_column_width = (int)FRAME_WIDTH * 3 / 1280;
  histogram_column_dx = (int)FRAME_WIDTH * 2 / 1280;
  histogram_column_max_height = (int)FRAME_HEIGHT * 20 / 720;

  recent_face_font_scale = 0.32 * FRAME_WIDTH / 1280;
  histogram_color_0 = Scalar(255, 255, 0);
  histogram_color_1 = Scalar(255, 0, 255);

  graph_color.clear();
  graph_color.push_back(Scalar(0, 255, 0));
  graph_color.push_back(Scalar(0, 0, 255));
  graph_color.push_back(Scalar(255, 255, 255));
  graph_color.push_back(Scalar(0, 255, 0));
  graph_color.push_back(Scalar(0, 0, 255));
  graph_color.push_back(Scalar(0, 255, 0));
  graph_color.push_back(Scalar(0, 0, 255));
  graph_color.push_back(Scalar(255, 255, 255));

  info.clear();
  info.push_back("Green: Same pose");
  info.push_back("Red: Same person, other pose");
  info.push_back("White: Other person");

  info_color.clear();
  info_color.push_back(Scalar(0, 255, 0));
  info_color.push_back(Scalar(0, 0, 255));
  info_color.push_back(Scalar(255, 255, 255));

  info_1.clear();
  info_1.push_back("Slot 1: Max");
  info_1.push_back("Slot 2: Average");
  info_1.push_back("Slot 3: Min");
}

Multi_Face_Area::~Multi_Face_Area() {}

void Multi_Face_Area::init(int rrl, int rrt, int rrr, int rrb) {
  recognition_roi.left() = rrl;
  recognition_roi.top() = rrt;
  recognition_roi.right() = rrr;
  recognition_roi.bottom() = rrb;

  info_x0 = rrl;
  info_y0 = rrb;
  info_dy = (int)30 * FRAME_HEIGHT / 720;
  info_x1 = rrl + (int)290 * FRAME_WIDTH / 1280;
  info_y1 = rrb;
}

void Multi_Face_Area::move(int dx, int dy) {
  bool debug = false;
  if (debug) {
    std::cout << "Multi_Face_Area::move::" << dy << std::endl;
  }
  if (recognition_roi.left() + dx >= 0 &&
      recognition_roi.right() + dx < FRAME_WIDTH &&
      recognition_roi.top() + dy >= 0 &&
      recognition_roi.bottom() + dy < FRAME_HEIGHT) {
    recognition_roi.set_left(recognition_roi.left() + dx);
    recognition_roi.set_right(recognition_roi.right() + dx);
    recognition_roi.set_top(recognition_roi.top() + dy);
    recognition_roi.set_bottom(recognition_roi.bottom() + dy);
  }
}

void Multi_Face_Area::DrawHistogram(cv::Mat &frame, int _x0, int _y0,
                                    std::vector<int> _histogram) {
  int historam_max_value = 0;
  for (size_t i = 0; i < _histogram.size(); ++i) {
    if (historam_max_value < _histogram.at(i)) {
      historam_max_value = _histogram.at(i);
    }
  }
  for (size_t i = 0; i < _histogram.size(); ++i) {
    int _h = (int)_histogram.at(i) * histogram_column_max_height /
             historam_max_value;
    cv::Point vertices[4];
    vertices[0].x = _x0 + i * histogram_column_width;
    vertices[0].y = _y0;
    vertices[1].x = _x0 + i * histogram_column_width;
    vertices[1].y = _y0 - _h;
    vertices[2].x = _x0 + (i + 1) * histogram_column_width - 1;
    vertices[2].y = _y0 - _h;
    vertices[3].x = _x0 + (i + 1) * histogram_column_width - 1;
    vertices[3].y = _y0;
    if (i % 2 == 0) {
      cv::fillConvexPoly(frame, vertices, 4, histogram_color_0);
    } else {
      cv::fillConvexPoly(frame, vertices, 4, histogram_color_1);
    }
  }
}

void Multi_Face_Area::DrawGraph(cv::Mat &frame, int _x0, int _y0,
                                std::vector<int> _graph, float _max_value) {
  bool debug = false;
  if (debug) {
    std::cout << "DrawGraph 0" << std::endl;
  }
  if (_graph.size() != 8) {
    return;
  }
  if (debug) {
    std::cout << "graph: " << std::endl;
  }
  int _max = _graph.at(0);
  for (size_t i = 1; i < 8; ++i) {
    if (_max < _graph.at(i)) {
      _max = _graph.at(i);
    }
    if (debug) {
      std::cout << _graph.at(i) << ", ";
    }
  }
  if (debug) {
    std::cout << std::endl;
  }

  if (debug) {
    std::cout << "DrawGraph 1 >> max = " << _max << std::endl;
  }
  int _x = _x0;
  for (size_t i = 1; i < 7; ++i) {
    //        cout << "_max_value = " << _max_value << endl;
    if (debug) {
      std::cout << _graph.at(i);
    }
    //        int _h = (int) _graph.at(i) * histogram_column_max_height / _max;
    int _h = (int)_graph.at(i) * histogram_column_max_height / 100;
    if (debug) {
      std::cout << " >> h = " << _h << std::endl;
    }
    cv::Point vertices[4];
    vertices[0].x = _x;
    vertices[0].y = _y0;
    vertices[1].x = _x;
    vertices[1].y = _y0 - _h;
    vertices[2].x = _x + histogram_column_width - 1;
    vertices[2].y = vertices[1].y;
    vertices[3].x = vertices[2].x;
    vertices[3].y = _y0;
    //        if (i % 2 == 0) {
    //            cv::fillConvexPoly(frame, vertices, 4, histogram_color_0);
    //        } else {
    //            cv::fillConvexPoly(frame, vertices, 4, histogram_color_1);
    //        }
    //        if (debug) {
    //            std::cout << vertices[0].x << " - " << vertices[1].x << " - "
    //            << vertices[2].x << " - " << vertices[3].x << "; ";
    //        }
    cv::fillConvexPoly(frame, vertices, 4, graph_color.at(i));
    _x += histogram_column_width;
    if (i == 2 || i == 4) {
      _x += histogram_column_dx;
    }
  }
  cv::line(frame, Point(_x0, _y0 - histogram_column_max_height),
           Point(_x, _y0 - histogram_column_max_height), Scalar(255, 255, 255),
           1);
  //    if (debug) {
  //        std::cout << std::endl;
  //    }
  if (debug) {
    std::cout << "DrawGraph ok" << std::endl;
  }
}

QImage mat_to_qimage_cpy(cv::Mat const &mat, QImage::Format format) {
  return QImage(mat.data, mat.cols, mat.rows, mat.step, format).copy();
}

// Tuc add this one based on Socket transaction purpose
void Multi_Face_Area::Temporary_Store_Unknown_Faces(
    Detected_Faces_List detected_face_list) {
  // Add faces_auto_id and descriptors to list
  for (int i = 0; i < detected_face_list.face_list.size(); i++) {
    if (detected_face_list.face_info_list[i].face_id.empty()) {
      static char auto_facefilename[30];
      time_t now = time(0);
      strftime(auto_facefilename, sizeof(auto_facefilename), "%d%H%M%S.jpg",
               localtime(&now));
      std::string str(auto_facefilename);
      temporary_store_unknown_faces.push_back(str);
      temporary_store_unknown_faces_lookup.insert(
          std::pair<std::string, dlib::matrix<float, 0, 1>>(
              str, detected_face_list.face_discriptor_list[i]));

      // Ha to do
      // Send all the pair<auto_facefilename, detected_face_list.face_list[i]>
    }
  }

  // If list.size > max_list_length_temporary_store_unknown_faces -> remove some
  if ((int)temporary_store_unknown_faces.size() > MAX_LIST_TEMP_UNKOWN_FACE) {

    for (int i = MAX_LIST_TEMP_UNKOWN_FACE;
         i < temporary_store_unknown_faces.size(); i++) {
      // Remove from lookup table
      std::map<std::string, dlib::matrix<float, 0, 1>>::iterator it;
      it = temporary_store_unknown_faces_lookup.find(
          temporary_store_unknown_faces[i]);
      temporary_store_unknown_faces_lookup.erase(it);
    }

    // Remove all the faces that exceeds max_length
    temporary_store_unknown_faces.erase(temporary_store_unknown_faces.begin(),
                                        temporary_store_unknown_faces.end() -
                                            MAX_LIST_TEMP_UNKOWN_FACE);
  }
}

void Multi_Face_Area::Add_New_Face_Profile_From_List(
    std::string faceid_received,
    std::vector<std::string> list_facefilename_autos_received) {
  time_t now = time(0);
  // check if faceid is unique
  if (match_gallery->unique_faceid_list.count(faceid_received) == 0)
    match_gallery->unique_faceid_list.insert(
        std::pair<string, int>(faceid_received, 1));
  else
    return;

  //    //Create folder to store all faceimages
  //    strftime(auto_filename, sizeof(auto_filename), "%M%S", localtime(&now));
  //    string str = match_gallery->folderpath + faceid_received;
  //    boost::filesystem::create_directory(str);

  for (int i = 0; i < list_facefilename_autos_received.size(); i++) {
    std::map<string, matrix<float, 0, 1>> gallery_face_descriptor;
    dlib::matrix<float, 0, 1> face_descriptor =
        temporary_store_unknown_faces_lookup
            [list_facefilename_autos_received[i]];

    gallery_face_descriptor.insert(
        std::pair<string, matrix<float, 0, 1>>("straight", face_descriptor));
    match_gallery->gallery_face_descriptors.insert(
        std::pair<string, std::map<string, matrix<float, 0, 1>>>(
            faceid_received, gallery_face_descriptor));

    std::map<std::string, int> pose_count;
    pose_count.insert(std::pair<string, int>("straight", 1));
    match_gallery->faceid_pose_count.insert(
        std::pair<std::string, std::map<string, int>>(faceid_received,
                                                      pose_count));

    User_Infor user_auto;
    user_auto.faceid = faceid_received;
    // We do not add to list because we want to access DB for infor intervally
    // If using this on-mem list, then for each image saved of one faceID, we
    // need to add one User_Infor
    match_gallery->output_inforlist.insert(
        std::pair<string, User_Infor>(faceid_received, user_auto));

    // Store the first and straight face here at declaration
    User_Facial_Infor user_facial_new; // do not save image here, otherwise need
                                       // have arguments
    match_gallery->output_person_facial_inforlist.insert(
        std::pair<string, User_Facial_Infor>(faceid_received, user_facial_new));

    // Now remove all the faceimage from temporary list
    std::map<std::string, dlib::matrix<float, 0, 1>>::iterator it;
    it = temporary_store_unknown_faces_lookup.find(
        list_facefilename_autos_received[i]);
    temporary_store_unknown_faces_lookup.erase(it);

    // find value and remove from vector
    std::vector<std::string>::iterator position =
        std::find(temporary_store_unknown_faces.begin(),
                  temporary_store_unknown_faces.end(),
                  list_facefilename_autos_received[i]);
    if (position !=
        temporary_store_unknown_faces
            .end()) // == myVector.end() means the element was not found
      temporary_store_unknown_faces.erase(position);
  }
}

// Render recent face list
void Multi_Face_Area::Add_Face_to_Recent_Face_List(
    Mat &frame, Detected_Faces_List detect_face_list, string folderpath) {
  bool debug = false;
  bool debug_1 = false;
  bool debug_2 = false;
  if (debug) {
    std::cout << "Add_Face_to_Recent_Face_List >> 0" << std::endl;
  }
  int recentfacelist_size = (int)recent_faces_list.size();
  int facelist_size =
      (int)detect_face_list.face_list
          .size(); // @TODO: face_list.size() != face_info_list.size()
  //  int facelist_size = (int)detect_face_list.face_info_list.size();
  // cout<<recentfacelist_size<<" "<<facelist_size<<"
  // "<<detect_face_list.face_id_list.size()<<endl;

  if (recentfacelist_size == 0 && facelist_size == 0)
    return;

  // check face_id
  std::set<std::string> face_id_set; // detected face set
  face_id_set.clear();

  if (debug) {
    std::cout << "Add_Face_to_Recent_Face_List >> 0.1 >> facelist_size = "
              << facelist_size
              << " >> x.size = " << detect_face_list.face_info_list.size()
              << std::endl;
  }
  for (int fl = 0; fl < facelist_size; fl++) {
    if (debug) {
      std::cout << "Add_Face_to_Recent_Face_List >> 0.1.0 >> fl = " << fl
                << std::endl;
    }
    std::string face_id = detect_face_list.face_info_list[fl].face_id;
    if (debug) {
      std::cout << "Add_Face_to_Recent_Face_List >> 0.1.1 >> face_id = "
                << face_id << std::endl;
    }
    if (face_id.empty()) {
      continue;
    }
    face_id_set.insert(face_id);

    if (recent_face_count.find(face_id) == recent_face_count.end()) {
      recent_face_count.insert(std::pair<std::string, int>(face_id, 1));
    } else if (recent_face_count.at(face_id) < 100) {
      recent_face_count.at(face_id)++;
    }
    if (debug) {
      std::cout << "Add_Face_to_Recent_Face_List >> 0.1.2" << std::endl;
    }
  }

  if (debug) {
    std::cout << "Add_Face_to_Recent_Face_List >> 1" << std::endl;
  }
  for (std::vector<std::pair<string, RecentFacesPresent>>::iterator rit =
           recent_faces_list.begin();
       rit != recent_faces_list.end(); ++rit) {
    string face_id = rit->first;
    if (face_id_set.find(face_id) == face_id_set.end() &&
        recent_face_count.find(face_id) != recent_face_count.end() &&
        recent_face_count.at(face_id) > 0) {
      recent_face_count.at(face_id)--;
    }
  }

  if (debug) {
    std::cout << "Add_Face_to_Recent_Face_List 2" << std::endl;
  }

  for (int fl = 0; fl < facelist_size; fl++) {
    if (detect_face_list.face_info_list[fl].face_id.empty()) {
      continue;
    }
    std::string face_id = detect_face_list.face_info_list[fl].face_id;
    QString qt_face_id = QString::fromStdString(face_id);

    if (recent_face_count.find(face_id) == recent_face_count.end() ||
        recent_face_count.at(face_id) < 3) {
      continue;
    }

    if (debug_1) {
      std::cout << "Multi_Face_Area::Add_Face_to_Recent_Face_List >> face_id = "
                << face_id << std::endl;
    }

    // calculate parameters for detected face
    cv::Mat cv_image = toMat(detect_face_list.face_list[fl]);
    cv::cvtColor(cv_image, cv_image, CV_BGR2RGB);
    cv::resize(cv_image, cv_image, Size(recent_face_size, recent_face_size));

    QImage qimg = QImage((const unsigned char *)(cv_image.data), cv_image.cols,
                         cv_image.rows, cv_image.step, QImage::Format_RGB888);
    if (debug_2) {
      std::cout
          << "Multi_Face_Area::Add_Face_to_Recent_Face_List >> add face >> "
          << face_id << std::endl;
    }
    Q_EMIT faceAdded(qt_face_id, qimg);

    RecentFacesPresent recent_face;
    recent_face.setRandom(random);
    recent_face.face_on_stage = cv_image;
    std::stringstream stream;
    stream << std::fixed << std::setprecision(0)
           << detect_face_list.face_info_list[fl].score << "%";
    recent_face.score = stream.str();
    if (debug_1) {
      std::cout << "Multi_Face_Area::Add_Face_to_Recent_Face_List >> "
                   "recent_face.score = "
                << recent_face.score << std::endl;
    }

    std::stringstream stream_1;
    stream_1 << std::fixed << std::setprecision(0)
             << detect_face_list.face_info_list[fl].max_distance_other_face_id
             << "%";
    recent_face.max_distance_other_face_id = stream_1.str();
    if (debug_1) {
      std::cout << "Multi_Face_Area::Add_Face_to_Recent_Face_List >> "
                   "recent_face.max_distance_other_face_id = "
                << recent_face.max_distance_other_face_id << std::endl;
    }

    //        recent_face.histogram =
    //        detect_face_list.face_info_list[fl].histogram;
    recent_face.graph = detect_face_list.face_info_list[fl].graph;
    if (debug_1) {
      std::cout
          << "Multi_Face_Area::Add_Face_to_Recent_Face_List >> graph.size = "
          << recent_face.graph.size() << std::endl;
    }

    //            recent_face.Get_Face_inGallery(folderpath,
    //            detect_face_list.face_id_list[fl]);
    std::string face_pose = getPose(detect_face_list, fl);
    recent_face.Get_Face_inGallery(
        folderpath, detect_face_list.face_info_list[fl].face_id,
        detect_face_list.face_info_list[fl].face_image_id, face_pose);

    //        recent_face.Get_Time_Present();

    // @TODO: time draft, to be optimized
    time_t now = time(0);
    time_t last_end_time = 0;
    if (recent_face_time.find(face_id) == recent_face_time.end()) {
      std::queue<std::pair<time_t, time_t>> time_queue;
      time_queue.push(std::pair<time_t, time_t>(now, now));
      recent_face_time[face_id] = time_queue;
      if (debug) {
        std::cout << face_id << ": Time update 1: " << now << " " << now
                  << std::endl;
      }
    } else {
      std::queue<std::pair<time_t, time_t>> time_queue =
          recent_face_time.at(face_id);
      if (recent_face_time.at(face_id).empty()) {
        recent_face_time.at(face_id).push(std::pair<time_t, time_t>(now, now));
        if (debug) {
          std::cout << face_id << ": Time update 2: " << now << " " << now
                    << std::endl;
        }
      } else {
        std::pair<time_t, time_t> last_time = time_queue.back();
        // update only after MIN_UPDATE_TIME seconds
        if (now - last_time.second >= MIN_UPDATE_TIME) {
          // absent > MAX_ABSENT_TIME seconds >> new session
          if (now - last_time.second > MAX_ABSENT_TIME) {
            recent_face_time.at(face_id).push(
                std::pair<time_t, time_t>(now, now));
            last_end_time = last_time.second;
            if (debug) {
              std::cout << face_id << ": Time update 3: " << now << " " << now
                        << std::endl;
            }
          } else {
            recent_face_time.at(face_id).push(
                std::pair<time_t, time_t>(last_time.first, now));
            if (debug) {
              std::cout << face_id << ": Time update 4: " << last_time.first
                        << " " << now << std::endl;
            }
          }
          if (recent_face_time.at(face_id).size() > MAX_TIME_QUEUE_SIZE) {
            recent_face_time.at(face_id).pop();
          }
        }
      }
    }
    time_t begin_session_time = recent_face_time.at(face_id).back().first;
    if (debug) {
      std::cout << face_id << ": begin_session_time = " << begin_session_time
                << std::endl;
    }
    recent_face.setTimePresent(begin_session_time);
    //        recent_face.setLastTimePresent(last_end_time);

    recent_face.pose = face_pose;

    bool exist_person = false;
    if (recentfacelist_size > 0) {
      for (int i = recentfacelist_size - 1; i >= 0; i--) {
        // if found person in recent list >> replace detected face only
        if (recent_faces_list[i].first ==
            detect_face_list.face_info_list[fl].face_id) {
          // if new graph, score & max_distance_other_face_id are not available
          // >> keep old values
          RecentFacesPresent temp_face_present = recent_faces_list[i].second;
          if (!recent_face.graph.size()) {
            recent_face.graph = temp_face_present.graph;
            recent_face.score = temp_face_present.score;
            recent_face.max_distance_other_face_id =
                temp_face_present.max_distance_other_face_id;
          }
          recent_faces_list[i].second = recent_face;
          exist_person = true;
          //					if (debug_1) {
          //						std::cout << "exist_person =
          //true"
          //<<
          // std::endl;
          //					}
          break;
        }
      }
    }
    // if person not in recent list >> add pair <new_recent_face, detected_face>
    if (!exist_person) {
      recent_faces_list.push_back(std::make_pair(
          detect_face_list.face_info_list[fl].face_id, recent_face));

      //@TODO ??? only store time and faceid
      RecentFacesPresent recent_faceid_time;
      recent_faceid_time.setRandom(random);
      recent_faceid_time.Get_Time_Present();
      //            recent_faceid_time_list.push_back(std::make_pair(detect_face_list.face_id_list[fl],
      //            recent_faceid_time));
      recent_faceid_time_list.push_back(std::make_pair(
          detect_face_list.face_info_list[fl].face_id, recent_faceid_time));
    }

    // if recent list is too long >> erase

    if ((int)recent_faces_list.size() > max_length_recent_faces_list) {
      recent_faces_list.erase(recent_faces_list.begin(),
                              recent_faces_list.end() -
                                  max_length_recent_faces_list);
    }
    if ((int)recent_faceid_time_list.size() > 1000) {
      recent_faceid_time_list.erase(recent_faceid_time_list.begin(),
                                    recent_faceid_time_list.end() - 1000);
    }
  }

  // render all recent faces

  if (debug) {
    std::cout << "Add_Face_to_Recent_Face_List 3" << std::endl;
  }

  if (recent_faces_list.size() > 0) {
    // cout<<recent_faces_list.size()<<endl;
    for (int i = (int)recent_faces_list.size() - 1; i >= 0; i--) {
      Rect rec(0, ((int)recent_faces_list.size() - 1 - i) *
                      (recent_face_size + recent_face_padding_top_bottom),
               recent_face_size, recent_face_size);
      Rect rec_side(recent_face_size,
                    ((int)recent_faces_list.size() - 1 - i) *
                        (recent_face_size + recent_face_padding_top_bottom),
                    recent_face_size, recent_face_size);

      // render (recent) face from recognition area
      recent_faces_list[i].second.face_on_stage.copyTo(frame(rec));

      // render (recent) face from gallery
      Mat face_ref = recent_faces_list[i].second.face_in_gallery;
      if (!face_ref.empty()) {
        // cv::imshow("t",face_ref);
        face_ref.copyTo(frame(rec_side));
      }
      cv::rectangle(frame, rec, Scalar(100, 255, 255), 1);
      cv::rectangle(frame, rec_side, Scalar(255, 153, 51), 1);

      //            cv::putText(frame, recent_faces_list[i].first,
      //            Point(recent_face_padding_left_1, ((int)
      //            recent_faces_list.size() - i) * (recent_face_size +
      //            recent_face_padding_top_bottom) +
      //            recent_face_padding_top_1), cv::FONT_HERSHEY_TRIPLEX,
      //            recent_face_font_scale, cv::Scalar(255,255,255), 1, 8);
      textCentered(frame, recent_faces_list[i].first, 0, recent_face_size,
                   ((int)recent_faces_list.size() - i) *
                           (recent_face_size + recent_face_padding_top_bottom) +
                       recent_face_padding_top_1,
                   cv::FONT_HERSHEY_TRIPLEX, recent_face_font_scale,
                   cv::Scalar(255, 255, 255), 1, 8);

      //            cv::putText(frame, recent_faces_list[i].second.time_present,
      //            Point(recent_face_padding_left_2, ((int)
      //            recent_faces_list.size() -i) * (recent_face_size +
      //            recent_face_padding_top_bottom) +
      //            recent_face_padding_top_2), cv::FONT_HERSHEY_TRIPLEX,
      //            recent_face_font_scale, cv::Scalar(255,255,255), 1, 8);
      textCentered(frame, recent_faces_list[i].second.time_present, 0,
                   recent_face_size,
                   ((int)recent_faces_list.size() - i) *
                           (recent_face_size + recent_face_padding_top_bottom) +
                       recent_face_padding_top_2,
                   cv::FONT_HERSHEY_TRIPLEX, recent_face_font_scale,
                   cv::Scalar(255, 255, 255), 1, 8);

      cv::putText(
          frame, recent_faces_list[i].second.score,
          Point(recent_face_padding_left_3,
                ((int)recent_faces_list.size() - i) *
                        (recent_face_size + recent_face_padding_top_bottom) +
                    recent_face_padding_top_3),
          cv::FONT_HERSHEY_TRIPLEX, recent_face_font_scale, graph_color.at(0),
          1, 8);
      //            textCentered(frame, recent_faces_list[i].second.score,
      //            recent_face_size * 3 / 2, recent_face_size / 2, ((int)
      //            recent_faces_list.size() - i) * (recent_face_size +
      //            recent_face_padding_top_bottom) + recent_face_padding_top_3,
      //            cv::FONT_HERSHEY_TRIPLEX, recent_face_font_scale,
      //            cv::Scalar(255,255,255), 1, 8);

      //            DrawHistogram(frame, histogram_x0, histogram_y0 + ((int)
      //            recent_faces_list.size() - i) * (recent_face_size +
      //            recent_face_padding_top_bottom),
      //            recent_faces_list[i].second.histogram);

      DrawGraph(frame, histogram_x0,
                histogram_y0 +
                    ((int)recent_faces_list.size() - i) *
                        (recent_face_size + recent_face_padding_top_bottom),
                recent_faces_list[i].second.graph,
                match_gallery->getMaxDistance());

      cv::putText(
          frame, recent_faces_list[i].second.max_distance_other_face_id,
          Point(recent_face_padding_left_max,
                ((int)recent_faces_list.size() - i) *
                        (recent_face_size + recent_face_padding_top_bottom) +
                    recent_face_padding_top_max),
          cv::FONT_HERSHEY_TRIPLEX, recent_face_font_scale,
          cv::Scalar(255, 255, 255), 1, 8);

      if (!recent_faces_list[i].second.last_time_present.empty()) {
        cv::putText(
            frame, recent_faces_list[i].second.last_time_present,
            Point(recent_face_padding_left_4,
                  ((int)recent_faces_list.size() - i) *
                          (recent_face_size + recent_face_padding_top_bottom) +
                      recent_face_padding_top_4),
            cv::FONT_HERSHEY_TRIPLEX, recent_face_font_scale,
            cv::Scalar(255, 255, 255), 1, 8);
      }

      if (show_pose) {
        textCentered(frame, recent_faces_list[i].second.pose, 0,
                     recent_face_size,
                     ((int)recent_faces_list.size() -
                      i) * (recent_face_size + recent_face_padding_top_bottom) +
                         recent_face_detected_padding_top,
                     cv::FONT_HERSHEY_TRIPLEX, recent_face_font_scale,
                     cv::Scalar(100, 255, 255), 1, 8);
      }
    }
  }
  if (debug) {
    std::cout << "Add_Face_to_Recent_Face_List ok" << std::endl;
  }
}

// get pose of i-th face in detected_face_list
std::string Multi_Face_Area::getPose(Detected_Faces_List detected_face_list,
                                     int i) {
  std::string facepose = "straight";
  User_Facial_Infor user_face_infor =
      match_gallery->output_person_facial_inforlist
          [detected_face_list.face_info_list[i].face_id];
  user_face_infor.Calculate_Angle_Direction_Landmarkbased(
      detected_face_list.shape_list[i]);
  if (!user_face_infor.direction_h.empty())
    facepose = user_face_infor.direction_h;
  else if (!user_face_infor.direction_v.empty())
    facepose = user_face_infor.direction_v;
  return facepose;
}

void Multi_Face_Area::Drawing_Stuff(cv::Mat &frame,
                                    Detected_Faces_List detected_face_list) {

  double fontscale = 0.6 * FRAME_WIDTH / 1280;
  // Draw the rectangle on the frame
  cv::Rect r_roi(recognition_roi.left(), recognition_roi.top(),
                 recognition_roi.width(), recognition_roi.height());
  cv::rectangle(frame, r_roi, cv::Scalar(0, 255, 255), 2);

  std::string recognition_text = "Recognition Area";
  int baseline = 0;
  cv::Size recognition_text_size = cv::getTextSize(
      recognition_text, cv::FONT_HERSHEY_TRIPLEX, 0.9, 2, &baseline);
  //    long recognition_text_x = recognition_roi.left() +
  //    (recognition_roi.width() - recognition_text_size.width) / 2;
  long recognition_text_y =
      recognition_roi.top() - AREA_TEXT_DY >= recognition_text_size.height
          ? recognition_roi.top() - AREA_TEXT_DY
          : recognition_roi.bottom() + AREA_TEXT_DY +
                recognition_text_size.height;
  //    cv::putText(frame, recognition_text, cv::Point(recognition_text_x,
  //    recognition_text_y),cv::FONT_HERSHEY_TRIPLEX, 0.9,
  //    cv::Scalar(255,255,255), 2, 8);

  textCentered(frame, recognition_text, recognition_roi.left(),
               recognition_roi.width(), recognition_text_y,
               cv::FONT_HERSHEY_TRIPLEX, area_name_font_scale,
               cv::Scalar(255, 255, 255), 2, 8);

  for (int i = 0; i < info.size(); ++i) {
    cv::putText(
        frame, info.at(i), cv::Point(info_x0, info_y0 + (i + 1) * info_dy),
        cv::FONT_HERSHEY_TRIPLEX, fontscale * 0.8, info_color.at(i), 1, 8);
  }

  for (int i = 0; i < info_1.size(); ++i) {
    cv::putText(frame, info_1.at(i),
                cv::Point(info_x1, info_y1 + (i + 1) * info_dy),
                cv::FONT_HERSHEY_TRIPLEX, fontscale * 0.8,
                cv::Scalar(255, 255, 255), 1, 8);
  }

  // Drawing detection RECT or landmark and faceID and other information on RECO
  // ROI
  //    for (size_t i =0;i<detected_face_list.face_id_list.size();i++){
  //        if (!detected_face_list.face_id_list[i].empty()){
  for (size_t i = 0; i < detected_face_list.face_info_list.size(); i++) {
    if (!detected_face_list.face_info_list[i].face_id.empty()) {
      int linespace = (int)30 * FRAME_HEIGHT / 720;
      //            float ratioleft = 1.3;
      Scalar color = Scalar(255, 255, 255);
      int thickness = 1;

      int baseline = 0;
      cv::Size face_id_text_size = cv::getTextSize(
          "ID: " +
              match_gallery
                  ->output_inforlist[detected_face_list.face_info_list[i]
                                         .face_id]
                  .faceid,
          cv::FONT_HERSHEY_TRIPLEX, fontscale * 1.3, thickness, &baseline);
      int y_tmp = detected_face_list.det_list[i].y - linespace;
      int y_face_id = y_tmp > face_id_text_size.height
                          ? y_tmp
                          : detected_face_list.det_list[i].y +
                                (int)detected_face_list.det_list[i].height +
                                face_id_text_size.height + linespace;
      //            Point pt2(detected_face_list.det_list[i].x- (int)
      //            ratioleft*detected_face_list.det_list[i].width, y_face_id);

      //            Point pt3(detected_face_list.det_list[i].x- (int)
      //            ratioleft*detected_face_list.det_list[i].width ,
      //                      detected_face_list.det_list[i].y - (int)
      //                      detected_face_list.det_list[i].height/2 +
      //                      2*linespace);
      //            Point pt4(detected_face_list.det_list[i].x- (int)
      //            ratioleft*detected_face_list.det_list[i].width ,
      //                      detected_face_list.det_list[i].y - (int)
      //                      detected_face_list.det_list[i].height/2 +
      //                      3*linespace);

      string facepose;
      string smiling;
      string eyeclose;
      //            User_Facial_Infor user_face_infor =
      //            match_gallery->output_person_facial_inforlist[detected_face_list.face_id_list[i]];
      User_Facial_Infor user_face_infor =
          match_gallery->output_person_facial_inforlist
              [detected_face_list.face_info_list[i].face_id];
      // Update the state of face direction
      user_face_infor.Calculate_Angle_Direction_Landmarkbased(
          detected_face_list.shape_list[i]);
      if (user_face_infor.direction_h.empty() &&
          user_face_infor.direction_v.empty())
        facepose = "straight";
      else if (!user_face_infor.direction_h.empty())
        facepose = user_face_infor.direction_h;
      else if (!user_face_infor.direction_v.empty())
        facepose = user_face_infor.direction_v;

      user_face_infor.Update_Smile_Ratio(detected_face_list.shape_list[i]);
      if (user_face_infor.Is_Smiling_Face(detected_face_list.shape_list[i]))
        smiling = "smiling";
      if (!user_face_infor.Is_Eyes_Openning(detected_face_list.shape_list[i]))
        eyeclose = "eye closing";

      //            cv::putText(frame, "ID:
      //            "+match_gallery->output_inforlist[detected_face_list.face_id_list[i]].faceid.substr(3,8),
      //            pt2, cv::FONT_HERSHEY_TRIPLEX, fontscale*1.3, color,
      //            thickness);
      //            cv::putText(frame, "ID: " +
      //            match_gallery->output_inforlist[detected_face_list.face_info_list[i].face_id].faceid,
      //            pt2, cv::FONT_HERSHEY_TRIPLEX, fontscale * 1.3, color,
      //            thickness);
      textCentered(
          frame, "ID: " +
                     match_gallery
                         ->output_inforlist[detected_face_list.face_info_list[i]
                                                .face_id]
                         .faceid,
          detected_face_list.det_list[i].x,
          detected_face_list.det_list[i].width, y_face_id,
          cv::FONT_HERSHEY_TRIPLEX, fontscale * 1.3, color, thickness,
          cv::LINE_8, false, true);
      //            if (show_pose) {
      //                cv::putText(frame, "Pose: " + facepose, pt3,
      //                cv::FONT_HERSHEY_TRIPLEX, fontscale, color, thickness);
      //            }
      //            if (!eyeclose.empty()) cv::putText(frame, eyeclose, pt4,
      //            cv::FONT_HERSHEY_TRIPLEX, fontscale, color, thickness);
      //            else if (!smiling.empty()) cv::putText(frame, smiling, pt4,
      //            cv::FONT_HERSHEY_TRIPLEX, fontscale, color, thickness);

      Drawing_Detection(frame, detected_face_list.det_list[i], color);
    } else if (i < detected_face_list.shape_list.size()) {
      Drawing_Landmark(frame, detected_face_list.shape_list[i]);
    }
  }
}
