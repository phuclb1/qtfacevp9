//#pragma once
#include <opencv2/core.hpp>
#include <iostream>
#include <vector>
#include <dlib/matrix.h>
#include <dlib/pixel.h>

#include "Kalman.hpp"
#include "HungarianAlg.hpp"

using namespace cv;
using namespace std;

class CTrack
{
public:
    std::vector<Point2d> trace;    
    //std::vector<Rect> rect_trace;    //Store the trace of all the boxes -> to interpolate smooth detection
    int frameID;                //Corresponding with frameID of the tracker

    Rect box_drawn;             //The 1 step back rect detected
    static size_t NextTrackID;
    size_t track_id;
    size_t skipped_frames;
    Point2d prediction;
    TKalmanFilter* KF;
    bool del_marked;
    double behumantrack_confidence;

    string trackname;
    dlib::matrix<dlib::rgb_pixel> face_rgb;

    //if there are more than one faceIDs stored here --> track was obcluded
    //--> we should not use this track to obtain more face image samples
    std::vector<string> all_faceIDs_onsametrack;

    //this only used for session-based face scan, one person will be scaned and taken with some images,
    //then all images will be put through the net once
    std::vector<dlib::matrix<dlib::rgb_pixel>> all_faceimages_onsametrack;

    int number_conseccutiveframes_nofacefound;

    CTrack(Point2f p, float dt, float Accel_noise_mag);
    ~CTrack();

    //Check if new trackname is same with the one stored in all_faceIDs_onsametrack
    //if no same --> add it to all_faceIDs_onsametrack --> we know that its size is 2 --> this track is not reliable/relevent
    //Return prompt showing that "ready to take more face samples, please turn your face on left, on right, up and down slowly!" OR ""
    //This function will use and check: number_conseccutiveframes_nofacefound == 0 (face had ID)
    void Update_all_faceIDs_onsametrack(string& prompt);
    bool Same_faceIDs_onsametrack();
    void Reset();
};

class CTracker
{
public:

    float dt;

    float Accel_noise_mag;

    double dist_thres;
    int maximum_allowed_skipped_frames;
    int max_trace_length;
    int max_rect_trace_length;
    //vector<int> assignment_global;

    std::vector<int> assignment;

    std::vector<Mat> frames_history; //temporary store the frames, after interpolate -> show lagged frame
    int frameID;                //Corresponding with frameID of each track

    std::vector<CTrack*> tracks;
    void Update(std::vector<Point2d>& detections, bool allow_add_erase_tracks);
    CTracker(float _dt, float _Accel_noise_mag, double _dist_thres=60, int _maximum_allowed_skipped_frames=10,int _max_trace_length=10);
    ~CTracker(void);
};

