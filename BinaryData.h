#ifndef _BINARYDATA_H_
#define _BINARYDATA_H_

#include <boost/geometry.hpp>
#include <boost/geometry/geometries/box.hpp>
#include <boost/geometry/geometries/point.hpp>

#include <boost/geometry/index/rtree.hpp>

#include <boost/foreach.hpp>
#include <boost/shared_ptr.hpp>

#include <dlib/matrix.h>

#include <cmath>
#include <iostream>
#include <memory>
#include <vector>

namespace bg = boost::geometry;
namespace bgi = boost::geometry::index;

namespace boost {
namespace geometry {
namespace index {

template <typename Box> struct indexable<boost::shared_ptr<Box>> {
  typedef boost::shared_ptr<Box> V;

  typedef Box const &result_type;
  result_type operator()(V const &v) const { return *v; }
};
}
}
} // namespace boost::geometry::index

typedef bg::model::point<float, 2, bg::cs::cartesian> point;
typedef bg::model::box<point> box;
typedef boost::shared_ptr<box> shp;
typedef shp value;

typedef bg::model::point<double, 2, bg::cs::cartesian> point_t;
typedef bg::model::box<point_t> box_t;
typedef bg::model::ring<point_t, true, false> ring_t; // clockwise, open
typedef bgi::rtree<point_t, bgi::linear<16>> rtree_t;

class Image {
public:
  Image(int _K);
  ~Image(void);
  inline float getFeature(int i) { return features->at(i); }

private:
  std::shared_ptr<std::vector<float>> features;
  int K;
};

class Pose {
public:
  Pose(int _K);
  ~Pose(void);
  void calculatePoseCenter();
  inline void addImage(std::shared_ptr<Image> image) {
    images->push_back(image);
  }
  inline std::shared_ptr<std::vector<std::shared_ptr<Image>>> getImages() {
    return images;
  }
  inline std::shared_ptr<std::vector<float>> getPoseCenter() {
    return pose_center;
  }
  inline float getCenterFeature(int i) { return pose_center->at(i); }
  inline int getPoseId() { return pose_id; }

private:
  std::shared_ptr<std::vector<std::shared_ptr<Image>>> images;
  std::shared_ptr<std::vector<float>> pose_center;
  int pose_id;
  int K;
};

class Face {
public:
  Face(int _K);
  ~Face(void);
  void calculateFaceCenter();
  inline std::shared_ptr<std::vector<std::shared_ptr<Pose>>> getPoses() {
    return poses;
  }
  inline std::shared_ptr<std::vector<float>> getFaceCenter() {
    return face_center;
  }
  inline int getFaceId() { return face_id; }

private:
  std::shared_ptr<std::vector<std::shared_ptr<Pose>>> poses;
  std::shared_ptr<std::vector<float>> face_center;
  int face_id;
  int K;
};

struct DLib128 {
  dlib::matrix<float, 0, 1> features;
  std::string face_id;
  std::string pose_id;
  std::string full_path;

  // t-SNE
  double x;
  double y;
  double z;
};

/**
 * For loading, accessing & writing 128-d vectors
 */
class Gallery {
public:
  Gallery(void);
  ~Gallery(void);

  inline int getNumberOfFeatureVectors() { return N; }
  inline int getSizeOfFeatureVector() { return K; }

  void cloneDataByFaceId(std::string faceId, std::shared_ptr<Gallery> data);

  void readFromFolder(std::string folder_path);
  void writeToTextFile(std::string file_path);

  void readFromBinaryFile(std::string file_path);
  void writeToBinaryFile(std::string file_path);

  std::string getPose(std::string file_name);

  void calculateCenterOfCluster();
  void findCloserCluster(std::string faceId,
                         std::vector<std::string> &faceIdVec);
  void tSNE(double theta, double perplexity, int no_dims, int max_iter);

  int closestVec(dlib::matrix<float, 0, 1> vec);

  void getPosOfVec(dlib::matrix<float, 0, 1> vec, std::vector<int> &posVec);

  inline int getN() { return N; }
  inline int getN1() { return C.size(); }
  inline double get_2d_X(int i) { return C.at(i).x; }
  inline double get_2d_Y(int i) { return C.at(i).y; }
  inline double get_2d_Z(int i) { return C.at(i).z; }
  inline std::string getFaceId(int i) { return C.at(i).face_id; }
  inline std::string getPoseId(int i) { return C.at(i).pose_id; }
  inline std::string getPath(int i) { return C.at(i).full_path; }
  inline dlib::matrix<float, 0, 1> get128Vec(int i) { return C.at(i).features; }

private:
  int N; // number of feature vectors
  int K; // size of feature vector
  int numPerson;
  std::map<std::string, dlib::matrix<float, 0, 1>> clusterCenters;
  std::vector<DLib128> C;
  std::map<std::string, std::shared_ptr<Face>> id_face;
  std::vector<std::string> pose_list;

  bgi::rtree<value, bgi::linear<16, 4>> rtree;
};

#endif /* _BINARYDATA_H_ */
