#ifndef MODELS_HPP
#define MODELS_HPP

#include <opencv2/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <dlib/dnn.h>
#include <dlib/image_processing.h>
#include <dlib/image_processing/frontal_face_detector.h>
#include <dlib/opencv.h>

#include "Dtracker.hpp"

using namespace dlib;
using namespace cv;
using namespace std;

// Also, the input layer was locked to images of size 150.
template <template <int, template <typename> class, int, typename> class block,
          int N, template <typename> class BN, typename SUBNET>
using residual = add_prev1<block<N, BN, 1, tag1<SUBNET> > >;

template <template <int, template <typename> class, int, typename> class block,
          int N, template <typename> class BN, typename SUBNET>
using residual_down = add_prev2<
    avg_pool<2, 2, 2, 2, skip1<tag2<block<N, BN, 2, tag1<SUBNET> > > > > >;

template <int N, template <typename> class BN, int stride, typename SUBNET>
using block =
    BN<con<N, 3, 3, 1, 1, relu<BN<con<N, 3, 3, stride, stride, SUBNET> > > > >;

template <int N, typename SUBNET>
using ares = relu<residual<block, N, affine, SUBNET> >;
template <int N, typename SUBNET>
using ares_down = relu<residual_down<block, N, affine, SUBNET> >;

template <typename SUBNET>
using alevel0 = ares_down<256, SUBNET>;
template <typename SUBNET>
using alevel1 = ares<256, ares<256, ares_down<256, SUBNET> > >;
template <typename SUBNET>
using alevel2 = ares<128, ares<128, ares_down<128, SUBNET> > >;
template <typename SUBNET>
using alevel3 = ares<64, ares<64, ares<64, ares_down<64, SUBNET> > > >;
template <typename SUBNET>
using alevel4 = ares<32, ares<32, ares<32, SUBNET> > >;

using anet_type = loss_metric<fc_no_bias<
    128, avg_pool_everything<alevel0<alevel1<alevel2<alevel3<alevel4<max_pool<
             3, 3, 2, 2,
             relu<affine<con<32, 7, 7, 2, 2,
                             input_rgb_image_sized<150> > > > > > > > > > > > >;

using anet_type_vp9 = loss_metric<fc_no_bias<
    128, avg_pool_everything<
             // alevel0<
             alevel1<alevel2<alevel3<alevel4<max_pool<
                 3, 3, 2, 2,
                 relu<affine<con<32, 7, 7, 2, 2, input_rgb_image_sized<
                                                     150> > > > > > > > > > > >;

struct RecentFaceInfo {
  std::string face_id;
  std::string face_image_id;
  float score;
  float max_distance_other_face_id;
  std::vector<int> histogram;  // historam 0-10: 0 = 0%; 10 = 100%
  std::vector<int> graph;
};

class Detected_Faces_List {
 public:
  std::vector<cv::Rect> det_list;
  std::vector<matrix<rgb_pixel> > face_list;
  std::vector<full_object_detection> shape_list;
  std::vector<matrix<float, 0, 1> > face_discriptor_list;
  // std::vector<string> face_id_list;
  std::vector<RecentFaceInfo> face_info_list;
  int biggest_face_index;

 public:
  Detected_Faces_List() {}

  ~Detected_Faces_List() {}
};

class PredictionModels {
 public:
  frontal_face_detector detector;
  frontal_face_detector detector_onecascade;
  shape_predictor sp;
  // And finally we load the DNN responsible for face recognition.
  anet_type net;
  // anet_type_vp9 net;
  float net_threshold;

  bool use_track_on_detection_and_recognition;
  bool use_track_on_detection;
  int number_frames_only_track;

 public:
  PredictionModels();
  void Set_Single_Level_Detector(frontal_face_detector &detector_);

  void Face_and_Shape_Detection(cv_image<bgr_pixel> img, dlib::rectangle d_roi,
                                Detected_Faces_List &detected_face_list);
  void Face_and_Shape_Detection_With_Track(
      cv_image<bgr_pixel> img, dlib::rectangle d_roi,
      Detected_Faces_List &detected_face_list, DtrackerManager &dtrackers,
      int count_frame);

  void Face_Recognition(Mat &frame, dlib::rectangle d_roi,
                        Detected_Faces_List &detected_face_list,
                        bool multiple_face);
  void Face_Recognition_With_Track(Mat &frame, dlib::rectangle d_roi,
                        Detected_Faces_List &detected_face_list,
                        bool multiple_face, DtrackerManager &dtrackers,
                        int count_frame);

};

#endif  // MODELS_HPP
