#-------------------------------------------------
#
# Project created by QtCreator 2018-01-25T16:02:35
#
#-------------------------------------------------

QT += core gui
QT += printsupport datavisualization 3drender

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets qml widgets quick 3dcore 3dquick 3drender 3dquickrender 3dinput 3dextras quickwidgets

TARGET = QtOpenCV
TEMPLATE = app
#CONFIG += console

SOURCES += main.cpp \
    Ctracker.cpp \
    Kalman.cpp \
    HungarianAlg.cpp \
    utils.cpp \
    MatchGallery.cpp \
    UserFacialInfor.cpp \
    RecentFaceList.cpp \
    Models.cpp \
    MultiFaceArea.cpp \
    SingleFaceArea.cpp \
        Dtracker.cpp \
        tsne.cpp \
        sptree.cpp \
        mainwindow.cpp \
        Player.cpp \
        BinaryData.cpp \
    View.cpp \
    GraphicsItem.cpp \
    Switch.cpp \
    ScatterDataModifier.cpp \
    src/qpointfield.cpp \
    src/qpointcloudreader.cpp \
    src/qpointcloudgeometry.cpp \
    src/qpointcloud.cpp

HEADERS  += utils.hpp \
    MatchGallery.hpp \
    UserFacialInfor.hpp \
    RecentFaceList.hpp \
    Models.hpp \
    MultiFaceArea.hpp \
    SingleFaceArea.hpp \
    Ctracker.hpp \
    HungarianAlg.hpp \
    Kalman.hpp \
    Dtracker.hpp \
    config.h \
mainwindow.h \
        tsne.h \
        sptree.h \
        vptree.h \
        Player.h \
        BinaryData.h \
    View.h \
    GraphicsItem.h \
    Switch.h \
    ScatterDataModifier.hpp \
    include/qpointcloud.h \
    include/qpointcloudgeometry.h \
    include/qpointcloudreader.h \
    include/qpointfield.h

#FORMS    += mainwindow.ui

CONFIG += c++11


INCLUDEPATH+= /usr/local/cuda-8.0/include
INCLUDEPATH+= /usr/local/include/opencv2
INCLUDEPATH+= /home/vp9/Desktop/FaceVP9/dlib-19.6
#INCLUDEPATH+= /home/tucnv/Qt/5.8/gcc_64/include

DEFINES += DLIB_USE_CUDA
DEFINES += DLIB_USE_BLAS
DEFINES += DLIB_JPEG_SUPPORT
DEFINES += DLIB_PNG_SUPPORT

CONFIG += link_pkgconfig
PKGCONFIG += opencv

QMAKE_CFLAGS += -fopenmp -Wpedantic -Wextra -Wfatal-errors -MMD -MP -pthread -fPIC -std=c++11 -O3 -Wall
QMAKE_LINK += -fopenmp -pthread -fPIC -std=c++11 -O3 -Wall
QMAKE_CXXFLAGS += -std=c++11
QMAKE_CXXFLAGS += -fext-numeric-literals

LIBS += -L/home/vp9/Desktop/FaceVP9/dlib-19.6/build/dlib -ldlib

#Used for Qt3d
DEFINES += WITH_PCL
PKGCONFIG += eigen3
INCLUDEPATH+= /usr/local/include/pcl-1.8
INCLUDEPATH+= /usr/include/eigen3
LIBS += -L/usr/local/lib -lpcl_io -lpcl_common -lpcl_io_ply -lpcl_visualization -lpcl_apps -lpcl_ml -lpcl_features -lpcl_filters -lpcl_keypoints
#/////////////

LIBS += -lopencv_core -lopencv_highgui -lopencv_imgproc -lopencv_video -lopencv_bgsegm -lopencv_imgcodecs -lopencv_videoio -lopencv_objdetect -lopencv_flann -lopencv_ml -lopencv_features2d -lopencv_calib3d
LIBS += -L/usr/local/lib
LIBS += -L/usr/lib/x86_64-linux-gnu/hdf5/serial

LIBS += -L/usr/local/cuda-8.0/lib64

LIBS += -lglog -lgflags #-lcaffe
LIBS += -lboost_system -lboost_filesystem -lboost_program_options -lm #-lhdf5_hl -lhdf5 -lleveldb -lsnappy -llmdb
LIBS += -lboost_thread -lstdc++ -lcblas -latlas
LIBS +=  -lcudart -lcublas -lcurand -lcudnn

LIBS += -lpng -ljpeg
LIBS += -lcblas -llapack

DISTFILES +=

RESOURCES += \
    qml.qrc
