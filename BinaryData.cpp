#include "BinaryData.h"
#include "Models.hpp"

#include <boost/filesystem.hpp>
#include <boost/foreach.hpp>

#include <dlib/clustering.h>
#include <dlib/dnn.h>
#include <dlib/gui_widgets.h>
#include <dlib/image_io.h>
#include <dlib/image_processing.h>
#include <dlib/image_processing/frontal_face_detector.h>
#include <dlib/image_processing/render_face_detections.h>

#include <fstream>
#include <iostream>

#include <cfloat>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
//#include "vptree.h"
#include "sptree.h"
#include "tsne.h"

#define FILE_NOT_FOUND_ERROR_CODE 20

/**
 * @brief Convert double <-> char array[8]
 */
union DoubleChar {
  double doubleValue;
  unsigned char charArray[8];
};

/**
 * @brief Convert float <-> char array[4]
 */
union FloatChar {
  float floatValue;
  unsigned char charArray[4];
};

/**
 * @brief convert double to unsigned char[8]
 * @param _number: double
 * @return unsigned char[8]
 */
std::vector<unsigned char> doubleToCharVector8(double _number) {
  DoubleChar n;
  n.doubleValue = _number;
  std::vector<unsigned char> _result = std::vector<unsigned char>();
  for (int i = 0; i < 8; ++i) {
    _result.push_back(n.charArray[i]);
  }
  return _result;
}

/**
 * @brief convert unsigned char[8] to double
 * @param _charVector: unsigned char[8]
 * @return double
 */
double charVector8ToDouble(std::vector<unsigned char> _charVector) {
  DoubleChar n;
  for (int i = 0; i < 8; ++i) {
    n.charArray[i] = _charVector.at(i);
  }
  return n.doubleValue;
}

/**
 * @brief convert float to unsigned char[4]
 * @param _number: float
 * @return unsigned char[4]
 */
std::vector<unsigned char> floatToCharVector4(float _number) {
  FloatChar n;
  n.floatValue = _number;
  std::vector<unsigned char> _result = std::vector<unsigned char>();
  for (int i = 0; i < 4; ++i) {
    _result.push_back(n.charArray[i]);
  }
  return _result;
}

/**
 * @brief convert unsigned char[4] to float
 * @param _charVector: unsigned char[4]
 * @return float
 */
double charVector4ToFloat(std::vector<unsigned char> _charVector) {
  FloatChar n;
  for (int i = 0; i < 4; ++i) {
    n.charArray[i] = _charVector.at(i);
  }
  return n.floatValue;
}

std::vector<unsigned char> intToCharVector1(int _number) {
  std::vector<unsigned char> _result;
  _result.push_back((unsigned char)(_number));
  return _result;
}

std::vector<unsigned char> intToCharVector2(int _number) {
  std::vector<unsigned char> _result;
  _result.push_back((unsigned char)(_number / 256));
  _result.push_back((unsigned char)(_number % 256));
  return _result;
}

std::vector<unsigned char> intToCharVector3(int _number) {
  int _tmp = _number;
  std::vector<unsigned char> _result;
  _result.push_back((unsigned char)(_tmp / 65536));
  _tmp %= 65536;
  _result.push_back((unsigned char)(_tmp / 256));
  _result.push_back((unsigned char)(_tmp % 256));
  return _result;
}

std::vector<unsigned char> intToCharVector4(int _number) {
  int _tmp = _number;
  std::vector<unsigned char> _result;
  _result.push_back((unsigned char)(_tmp / 16777216));
  _tmp %= 16777216;
  _result.push_back((unsigned char)(_tmp / 65536));
  _tmp %= 65536;
  _result.push_back((unsigned char)(_tmp / 256));
  _result.push_back((unsigned char)(_tmp % 256));
  return _result;
}

std::vector<unsigned char> stringToCharVector(std::string str) {
  std::vector<unsigned char> _result;
  for (size_t z = 0; z < str.length(); ++z) {
    _result.push_back((unsigned char)str.at(z));
  }
  return _result;
}

Image::Image(int _K) {
  K = _K;
  features = std::make_shared<std::vector<float>>(K);
}

Image::~Image(void) {}

Pose::Pose(int _K) {
  K = _K;
  images = std::make_shared<std::vector<std::shared_ptr<Image>>>();
  pose_center = std::make_shared<std::vector<float>>(K);
  pose_id = 0;
}

Pose::~Pose(void) {}

void Pose::calculatePoseCenter() {
  for (int j = 0; j < pose_center->size(); ++j) {
    pose_center->at(j) = .0;
  }
  if (images->empty()) {
    return;
  }
  for (int i = 0; i < images->size(); ++i) {
    for (int j = 0; j < K; ++j) {
      pose_center->at(j) += images->at(i)->getFeature(j);
    }
  }
  for (int j = 0; j < K; ++j) {
    pose_center->at(j) /= images->size();
  }
}

Face::Face(int _K) {
  K = _K;
  poses = std::make_shared<std::vector<std::shared_ptr<Pose>>>();
  face_center = std::make_shared<std::vector<float>>(K);
  face_id = 0;
}

Face::~Face(void) {}

void Face::calculateFaceCenter() {
  for (int j = 0; j < face_center->size(); ++j) {
    face_center->at(j) = .0;
  }
  if (poses->empty()) {
    return;
  }
  for (int i = 0; i < poses->size(); ++i) {
    for (int j = 0; j < K; ++j) {
      face_center->at(j) += poses->at(i)->getCenterFeature(j);
    }
  }
  for (int j = 0; j < face_center->size(); ++j) {
    face_center->at(j) /= poses->size();
  }
}

/**
 * @brief BinaryData::BinaryData
 */
Gallery::Gallery(void) {
  N = 0;
  K = 0;

  pose_list.clear();
  pose_list.push_back("straight");
  pose_list.push_back("left");
  pose_list.push_back("right");
  pose_list.push_back("up");
  pose_list.push_back("down");
  pose_list.push_back("smiling");

  std::vector<point_t> points;
  points.push_back(point_t(0, 0));
  points.push_back(point_t(1, 1));
  points.push_back(point_t(2, 2));
  points.push_back(point_t(3, 3));

  // build a rtree
  rtree_t rtree(points.begin(), points.end());
  //    // insert new value
  //    rtree.insert(b);
}

/**
 * @brief BinaryData::~BinaryData
 */
Gallery::~Gallery(void) {}

std::string Gallery::getPose(std::string file_name) {
  for (std::vector<std::string>::iterator it = pose_list.begin();
       it != pose_list.end(); ++it) {
    if (file_name.find(*it) != std::string::npos) {
      return *it;
    }
  }
  return "";
}

void Gallery::readFromFolder(std::string folder_path) {
  //    bool debug = false;
  PredictionModels models;
  C.clear();
  boost::filesystem::path _path(folder_path);
  BOOST_FOREACH (const boost::filesystem::path &person_folder,
                 std::make_pair(boost::filesystem::directory_iterator(_path),
                                boost::filesystem::directory_iterator())) {
    if (boost::filesystem::is_directory(person_folder)) {
      std::string _face = person_folder.filename().string();
      if (id_face.find(_face) == id_face.end()) {
        id_face.insert(std::pair<std::string, std::shared_ptr<Face>>(
            _face, std::make_shared<Face>(K)));
      }
      BOOST_FOREACH (
          const boost::filesystem::path &person_file,
          std::make_pair(boost::filesystem::directory_iterator(person_folder),
                         boost::filesystem::directory_iterator())) {
        if (boost::filesystem::is_directory(person_file)) {
          continue;
        }
        if (!boost::filesystem::is_regular_file(person_file)) {
          continue;
        } else if (person_file.extension().string() == ".jpg") {
          std::string _pose = this->getPose(person_file.filename().string());
          dlib::matrix<rgb_pixel> img;
          dlib::load_image(img, person_file.string());

          DLib128 d;
          d.face_id = _face;
          d.pose_id = _pose;
          d.features = models.net(img);
          d.full_path = person_file.string();
          C.push_back(d);
        }
      }
    }
  }
  N = C.size();
  K = N > 0 ? C.at(0).features.size() : 0;
}

void Gallery::writeToTextFile(std::string file_path) {
  std::ofstream fo;
  fo.open(file_path.c_str());
  fo << N << std::endl;
  fo << K << std::endl;
  for (int i = 0; i < N; ++i) {
    fo << C.at(i).face_id << std::endl;
    fo << C.at(i).pose_id << std::endl;
    fo << C.at(i).full_path << std::endl;
    for (int j = 0; j < K; ++j) {
      fo << C.at(i).features(j) << " ";
    }
    fo << std::endl;
  }
  fo.close();
}

void Gallery::readFromBinaryFile(std::string file_path) {
  bool debug = false;

  std::ifstream f(file_path.c_str(),
                  std::ios::in | std::ios::binary | std::ios::ate);
  char *buffer;
  if (!f.is_open()) {
    throw FILE_NOT_FOUND_ERROR_CODE;
    if (debug) {
      std::cout << "@Error: BinaryData::readFromFile: Unable to open file"
                << std::endl;
    }
  }
  // get size of file
  int size = (int)f.tellg();
  // jump to begin of file
  f.seekg(0, std::ios::beg);
  // allocate buffer
  buffer = new char[size];
  // read file
  f.read(buffer, size);
  // close file
  f.close();
  // set offset begin of buffer
  int offset = 0;

  if (debug) {
    std::cout << "BinaryData::readFromBinaryFile: 1" << std::endl;
  }

  // convert 4 bytes to N
  N = (unsigned char)buffer[offset] * 16777216 +
      (unsigned char)buffer[offset + 1] * 65536 +
      (unsigned char)buffer[offset + 2] * 256 +
      (unsigned char)buffer[offset + 3];
  offset += 4;

  if (debug) {
    std::cout << "N = " << N << std::endl;
  }

  // convert 4 bytes to K
  K = (unsigned char)buffer[offset] * 16777216 +
      (unsigned char)buffer[offset + 1] * 65536 +
      (unsigned char)buffer[offset + 2] * 256 +
      (unsigned char)buffer[offset + 3];
  offset += 4;

  if (debug) {
    std::cout << "K = " << K << std::endl;
  }

  if (debug) {
    std::cout << "BinaryData::readFromBinaryFile: 2" << std::endl;
  }

  // read feature vectors
  C.clear();

  for (int i = 0; i < N; ++i) {
    int l = (unsigned char)buffer[offset] * 16777216 +
            (unsigned char)buffer[offset + 1] * 65536 +
            (unsigned char)buffer[offset + 2] * 256 +
            (unsigned char)buffer[offset + 3];
    offset += 4;
    std::string _face_id = "";
    for (int z = 0; z < l; ++z) {
      _face_id += buffer[offset];
      offset++;
    }

    l = (unsigned char)buffer[offset] * 16777216 +
        (unsigned char)buffer[offset + 1] * 65536 +
        (unsigned char)buffer[offset + 2] * 256 +
        (unsigned char)buffer[offset + 3];
    offset += 4;
    std::string _pose_id = "";
    for (int z = 0; z < l; ++z) {
      _pose_id += buffer[offset];
      offset++;
    }

    l = (unsigned char)buffer[offset] * 16777216 +
        (unsigned char)buffer[offset + 1] * 65536 +
        (unsigned char)buffer[offset + 2] * 256 +
        (unsigned char)buffer[offset + 3];
    offset += 4;
    std::string _full_path = "";
    for (int z = 0; z < l; ++z) {
      _full_path += buffer[offset];
      offset++;
    }

    dlib::matrix<float, 0, 1> feature_vector(K);
    for (int j = 0; j < K; ++j) {
      FloatChar x;
      for (int z = 0; z < 4; ++z) {
        x.charArray[z] = (unsigned char)buffer[offset];
        offset++;
      }
      feature_vector(j) = x.floatValue;
    }

    if (debug) {
      std::cout << _face_id << " - " << _pose_id << " - " << _full_path
                << std::endl;
    }

    DLib128 item;
    item.face_id = _face_id;
    item.pose_id = _pose_id;
    item.full_path = _full_path;
    item.features = feature_vector;
    C.push_back(item);
  }

  // free buffer
  delete[] buffer;
  buffer = NULL;
  if (debug) {
    std::cout << "@BinaryData::readFromBinaryFile: OK!" << std::endl;
  }
}

void Gallery::writeToBinaryFile(std::string file_path) {
  bool debug = false;

  unsigned char *buffer;
  buffer =
      new unsigned char[8 + N * K * 4 + N * 1000]; // @TODO: feature_vector(K)
  int bufferSize = 0;

  std::vector<unsigned char> binN = intToCharVector4(N);
  for (int i = 0; i < (int)binN.size(); ++i) {
    buffer[bufferSize] = binN.at(i);
    bufferSize++;
  }

  std::vector<unsigned char> binK = intToCharVector4(K);
  for (int i = 0; i < (int)binK.size(); ++i) {
    buffer[bufferSize] = binK.at(i);
    bufferSize++;
  }

  FloatChar x;
  for (int i = 0; i < N; ++i) {
    std::vector<unsigned char> bin_face_id_length =
        intToCharVector4(C.at(i).face_id.length());
    for (int z = 0; z < (int)bin_face_id_length.size(); ++z) {
      buffer[bufferSize] = bin_face_id_length.at(z);
      bufferSize++;
    }
    for (int z = 0; z < C.at(i).face_id.length(); ++z) {
      buffer[bufferSize] = C.at(i).face_id.at(z);
      bufferSize++;
    }
    std::vector<unsigned char> bin_pose_id_length =
        intToCharVector4(C.at(i).pose_id.length());
    for (int z = 0; z < (int)bin_pose_id_length.size(); ++z) {
      buffer[bufferSize] = bin_pose_id_length.at(z);
      bufferSize++;
    }
    for (int z = 0; z < C.at(i).pose_id.length(); ++z) {
      buffer[bufferSize] = C.at(i).pose_id.at(z);
      bufferSize++;
    }
    std::vector<unsigned char> bin_path_length =
        intToCharVector4(C.at(i).full_path.length());
    for (int z = 0; z < (int)bin_path_length.size(); ++z) {
      buffer[bufferSize] = bin_path_length.at(z);
      bufferSize++;
    }
    for (int z = 0; z < C.at(i).full_path.length(); ++z) {
      buffer[bufferSize] = C.at(i).full_path.at(z);
      bufferSize++;
    }
    for (int j = 0; j < K; ++j) {
      x.floatValue = C.at(i).features(j);
      for (int z = 0; z < 4; ++z) {
        buffer[bufferSize] = x.charArray[z];
        bufferSize++;
      }
    }
  }

  std::ofstream f(file_path.c_str(), std::ios::out | std::ios::binary);
  f.write((char *)&buffer[0], bufferSize);
  f.close();
  if (debug) {
    std::cout << "BinaryData::writeToFile done!" << std::endl;
  }
}

void Gallery::calculateCenterOfCluster() {
  bool debug = false;
  if (debug) {
    std::cout << "Gallery::calculateCenterOfCluster start!" << std::endl;
  }
  this->numPerson = 0;
  std::string currentFaceId = this->C[0].face_id;
  int num_feature_vector = 0;
  dlib::matrix<float, 0, 1> sum_feature_vector;
  for (int i = 0; i < this->N; ++i) {
    if (debug) {
      cout << i << "---" << currentFaceId << endl;
    }
    if (currentFaceId.compare(this->C[i].face_id) != 0) {

      this->numPerson++;
      if (debug) {
        cout << "Step 1-----" << numPerson << "---" << this->C[i].face_id
             << endl;
      }
      //      for (int j = 0; j < this->K; ++j)
      //        sum_feature_vector[j] /= sum_feature_vector[j] /
      //        num_feature_vector;
      //      dlib::matrix_subtract_exp();
      dlib::matrix<float, 0, 1> tmp;
      if (debug) {
        cout << currentFaceId << "----" << num_feature_vector << endl;
      }
      double a = 1 / num_feature_vector;
      tmp = a * sum_feature_vector;
      if (debug) {
        for (int j = 0; j < 128; ++j) {
          cout << sum_feature_vector(j) << " ";
        }
        cout << std::endl;
      }

      this->clusterCenters.insert(
          std::pair<string, matrix<float, 0, 1>>(currentFaceId, tmp));
      tmp = dlib::zeros_matrix(sum_feature_vector);
      sum_feature_vector = dlib::zeros_matrix(sum_feature_vector);

      num_feature_vector = 1;

      currentFaceId = this->C[i].face_id;
    } else {
      if (debug) {
        cout << "Step 2-----" << numPerson << "---" << this->C[i].face_id
             << endl;
      }
      //      sum_feature_vector = dlib::add(sum_feature_vector,
      //      this->C[i].features);
      //      cout << sum_feature_vector << endl;
      dlib::matrix<float, 0, 1> tmp;

      tmp = sum_feature_vector + this->C[i].features;
      sum_feature_vector = tmp;
      if (debug) {
        cout << "---------------------------" << i << endl;
        for (int j = 0; j < 128; ++j) {
          cout << this->C[i].features(j) << " ";
        }
        cout << std::endl;
      }
      num_feature_vector++;
      currentFaceId = this->C[i].face_id;
    }
  }

  if (debug) {
    std::cout << "Gallery::calculateCenterOfCluster done ! "
              << this->clusterCenters.size() << std::endl;
  }
}

void Gallery::findCloserCluster(std::string faceId,
                                std::vector<std::string> &faceIdVec) {
  double min_1 = 9999;
  double min_2 = 9999;
  double min_3 = 9999;
  std::string faceId_1 = "";
  std::string faceId_2 = "";
  std::string faceId_3 = "";

  for (std::map<string, matrix<float, 0, 1>>::iterator it =
           this->clusterCenters.begin();
       it != this->clusterCenters.end(); ++it) {
    std::string currentFaceId = it->first;
    for (int j = 0; j < 128; ++j) {
      cout << it->second(j) << " ";
    }
    cout << std::endl;
    if (currentFaceId.compare(faceId) != 0) {
      double d = length(it->second - clusterCenters[faceId]);
      cout << "distance : " << d << endl;
      if (min_1 > d) {
        min_3 = min_2;
        faceId_3 = faceId_2;
        min_2 = min_1;
        faceId_2 = faceId_1;
        min_1 = d;
        faceId_1 = it->second;
      } else if (min_2 > d) {
        min_3 = min_2;
        faceId_3 = faceId_2;
        min_2 = min_1;
        faceId_2 = it->second;
      } else if (min_3 > d) {
        min_3 = d;
        faceId_3 = it->second;
      }
    }
  }
  faceIdVec.push_back(faceId_1);
  faceIdVec.push_back(faceId_2);
  faceIdVec.push_back(faceId_3);
}

void Gallery::tSNE(double theta, double perplexity, int no_dims, int max_iter) {
  bool debug = false;
  if (debug) {
    std::cout << "tSNE: 0" << std::endl;
  }
  // Define some variables
  int origN, _N, D, *landmarks;
  //	double perc_landmarks;
  double *data;
  int rand_seed = -1;
  TSNE *tsne = new TSNE();

  if (debug) {
    std::cout << "tSNE: 1; N = " << N << "; K = " << K << std::endl;
  }
  // Read the parameters and the dataset
  //	if(tsne->load_data(&data, &origN, &D, &no_dims, &theta, &perplexity,
  //&rand_seed, &max_iter)) {
  origN = N;
  D = K;
  data = (double *)malloc(D * origN * sizeof(double));
  if (debug) {
    std::cout << "tSNE: 1.1; N = " << N << std::endl;
  }
  for (int i = 0; i < N; ++i) {
    //		if (debug) {
    //			std::cout << "i = " << i << std::endl;
    //		}
    for (int j = 0; j < K; ++j) {
      data[i * K + j] = (double)C.at(i).features(j);
    }
  }

  if (debug) {
    std::cout << "tSNE: 2" << std::endl;
  }
  // Make dummy landmarks
  _N = origN;
  landmarks = (int *)malloc(_N * sizeof(int));
  if (landmarks == NULL) {
    printf("Memory allocation failed!\n");
    exit(1);
  }
  for (int z = 0; z < _N; z++)
    landmarks[z] = z;

  if (debug) {
    std::cout << "tSNE: 3" << std::endl;
  }
  // Now fire up the SNE implementation
  double *Y = (double *)malloc(_N * no_dims * sizeof(double));
  double *costs = (double *)calloc(_N, sizeof(double));
  if (Y == NULL || costs == NULL) {
    printf("Memory allocation failed!\n");
    exit(1);
  }
  if (debug) {
    std::cout << "tSNE: 4" << std::endl;
  }
  tsne->run(data, _N, D, Y, no_dims, perplexity, theta, rand_seed, false,
            max_iter);

  if (debug) {
    std::cout << "tSNE: 5; _N = " << _N << std::endl;
  }
  // Save the results
  //	tsne->save_data(Y, landmarks, costs, _N, no_dims);
  //  for (int i = 0; i < _N * no_dims; ++i) {
  //    cout << Y[i] << "     ";
  //  }
  for (int i = 0; i < _N; ++i) {
    C.at(i).x = Y[i * no_dims];
    C.at(i).y = Y[i * no_dims + 1];
    if (no_dims > 2) {
      C.at(i).z = Y[i * no_dims + 2];
    }
  }
  if (debug) {
    std::cout << "tSNE: 6" << std::endl;
  }

  // Clean up the memory
  free(data);
  data = NULL;
  free(Y);
  Y = NULL;
  free(costs);
  costs = NULL;
  free(landmarks);
  landmarks = NULL;
  if (debug) {
    std::cout << "tSNE: 7" << std::endl;
  }
  //	}
  delete (tsne);
}

void Gallery::cloneDataByFaceId(std::string faceId,
                                std::shared_ptr<Gallery> data) {
  this->K = 128;
  int count = 0;
  std::vector<std::string> idVec;
  //  data->findCloserCluster(faceId, idVec);
  idVec.push_back(faceId);
  C.clear();
  for (int i = 0; i < data->getN(); ++i) {
    if (std::find(idVec.begin(), idVec.end(), data->getFaceId(i)) !=
        idVec.end()) {
      DLib128 item;
      item.face_id = data->getFaceId(i);
      item.pose_id = data->getPoseId(i);
      item.full_path = data->getPath(i);
      item.features = data->get128Vec(i);
      C.push_back(item);
      count++;
    }
  }
  this->N = count;
}

int Gallery::closestVec(dlib::matrix<float, 0, 1> vec) {
  double min_1 = 9999;
  int index_1 = -1;

  for (int i = 0; i < N; ++i) {
    double d = dlib::length(C[i].features - vec);
    if (min_1 > d) {
      min_1 = d;
      index_1 = i;
    }
  }
  return index_1;
}

void Gallery::getPosOfVec(dlib::matrix<float, 0, 1> vec,
                          std::vector<int> &posVec) {
  int zzz = 10;
  // 128 dimension
  dlib::matrix<float, 0, 1> center = dlib::zeros_matrix(vec);
  // 3 dimension
  int center_x = 0;
  int center_y = 0;
  int center_z = 0;
  //  cout << "getPosOfVec : " << N << endl;
  for (int i = 0; i < N; ++i) {

    dlib::matrix<float, 0, 1> tmp;
    tmp = center + C.at(i).features;
    center = tmp;

    center_x += (int)zzz * C.at(i).x;
    center_y += (int)zzz * C.at(i).y;
    center_z += (int)zzz * C.at(i).z;
  }
  //  cout << std::endl;
  //  cout << "getPosOfVec1   " << endl;
  dlib::matrix<float, 0, 1> tmp1;
  double a = 1 / N;
  tmp1 = center * a;
  center = tmp1;

  //  cout << std::endl;
  //  cout << "getPosOfVec2  " << endl;
  center_x = (int)center_x / N;
  center_y = (int)center_y / N;
  center_z = (int)center_z / N;

  int closestVecIndex = this->closestVec(vec);
  //  cout << "Index : " << closestVecIndex << endl;
  double d1 = dlib::length(center - vec);
  double d2 = dlib::length(center - C.at(closestVecIndex).features);

  //  cout << d1 << "---" << d2 << endl;

  int u_x = (int)zzz * C.at(closestVecIndex).x - center_x;
  int u_y = (int)zzz * C.at(closestVecIndex).y - center_y;
  int u_z = (int)zzz * C.at(closestVecIndex).z - center_z;

  int pos_x = (int)(d2 * u_x / d1 + center_x);
  posVec.push_back(pos_x);
  int pos_y = (int)(d2 * u_y / d1 + center_y);
  posVec.push_back(pos_y);
  int pos_z = (int)(d2 * u_z / d1 + center_z);
  posVec.push_back(pos_z);
}
