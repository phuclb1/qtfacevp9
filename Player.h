#ifndef PLAYER_H
#define PLAYER_H
#include "MultiFaceArea.hpp"

//#include "sio_client.h"
#include <QImage>
#include <QMutex>
#include <QThread>
#include <QWaitCondition>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <queue>
#include <stdio.h>
//#include <>

using namespace cv;

enum class MODE { TRAINING = 0, RECOGNITION };
class Player : public QThread {
  Q_OBJECT
private:
  bool stop;
  QMutex mutex;
  QWaitCondition condition;
  Mat frame;
  int frameRate;
  int frame_width;
  int frame_height;
  VideoCapture *capture;
  Mat RGBframe;
  QImage img;
  MODE mode;
  std::string lastest_face_id;
  //  client _io;
  // std::queue<std::vector<matrix<float, 0, 1>>> vec_queue;
  // MatchAgainstGallery *matchgallery;
Q_SIGNALS:
  // Signal to output frame to be displayed
  void processedImage(const QImage &image);
  void new_persons_appear(QVector<matrix<float, 0, 1>> vec,
                          QVector<matrix<rgb_pixel>> img_vec);

  // void new_persons_appear(int a);

protected:
  void run();
  void msleep(int ms);

public:
  // Constructor
  Player(QObject *parent = 0);
  // Destructor
  ~Player();
  // Load a video from memory
  bool loadVideo(std::string filename);
  // Play the video
  void Play();
  // Stop the video
  void Stop();
  // check if the player has been stopped
  bool isStopped() const;

  // set video properties
  void setCurrentFrame(int frameNumber);

  // set MODE
  void setMode(MODE _mode) { this->mode = _mode; }
  // Get video properties
  double getFrameRate();
  double getCurrentFrame();
  double getNumberOfFrames();
  std::string getLatestFaceId() { return this->lastest_face_id; }
  //  void onReceiveNewProfile(std::string const &name, message::ptr const
  //  &data,
  //                           bool hasAck, message::list &ack_resp);

  inline int getFrameWidth() { return frame_width; }
  inline int getFrameHeight() { return frame_height; }
};

#endif // VIDEOPLAYER_H
