#ifndef RECENTFACELIST_HPP
#define RECENTFACELIST_HPP

#include <dlib/opencv.h>

#include <opencv2/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <dlib/image_processing.h>
#include <dlib/image_processing/frontal_face_detector.h>
#include <string.h>

// ----------------------------------------------------------------------------------------
class RecentFacesPresent {
public:
  cv::Mat face_in_gallery;
  cv::Mat face_on_stage;
  std::string time_present;
  std::string last_time_present;
  std::string score;
  std::string max_distance_other_face_id;
  std::string pose;
  std::vector<int> histogram;
  std::vector<int> graph;

public:
  RecentFacesPresent();
  ~RecentFacesPresent();
  void Get_Face_inGallery(std::string folderpath, std::string faceid,
                          std::string face_image_id,
                          std::string priority_face_pose);
  void Get_Time_Present();
  int Get_How_Long_Last_Present();
  inline void setRandom(int _random) { random = _random; }
  void setTimePresent(time_t _time);
  void setLastTimePresent(time_t _last_time);

private:
  int random;
  int resized_face_width;
  int resized_face_height;
};

#endif // RECENTFACELIST_HPP
