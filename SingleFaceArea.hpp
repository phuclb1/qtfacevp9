#ifndef SINGLEPERSONCASE_HPP
#define SINGLEPERSONCASE_HPP

#include <dlib/opencv.h>

#include <opencv2/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <dlib/image_processing.h>
#include <dlib/image_processing/frontal_face_detector.h>

#include <queue>
#include <string.h>

#include "MatchGallery.hpp"
#include "Models.hpp"
#include "RecentFaceList.hpp"
#include "UserFacialInfor.hpp"
#include <boost/filesystem.hpp>
// ----------------------------------------------------------------------------------------

class Single_Face_Area {
public:
  matrix<rgb_pixel> currentface;
  full_object_detection current_landmark_shape;
  matrix<float, 0, 1> current_face_descriptor;
  string current_person_name;
  cv::Rect current_det;
  string current_faceid;
  User_Infor current_faceid_infor;
  User_Facial_Infor current_peron_facial_infor;

  dlib::rectangle training_roi;

  MatchAgainstGallery *match_gallery;

public:
  // All of these functions are for Single image training roi
  Single_Face_Area();
  void init(int trl, int trt, int trr, int trb);
  void Update_Current_Infor(Detected_Faces_List detected_face_list);
  void Add_NewFaceID_Training_Roi(CTrack *curtract);
  void Modify_FaceID_Training_Roi(CTrack *curtract, int key);
  void Save_Multiple_FaceImages_Training_ROI(CTrack *curtrack);
  void Update_Track_with_FaceID(CTracker &tracker,
                                Detected_Faces_List detected_face_train_roi,
                                int key);
  void Drawing_Stuff(cv::Mat &frame, Detected_Faces_List detected_face_list);
  inline void setShowPose(int _show_pose) { show_pose = _show_pose; }
  void drawPoseCountGraph(cv::Mat &frame, std::string _current_faceid);

private:
  int show_pose;
  std::vector<cv::Scalar> pose_count_color;
};
#endif // SINGLEPERSONCASE_HPP
