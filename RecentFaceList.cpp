#include "config.h"
#include <string.h>
#include <stdlib.h>

#include <dlib/opencv.h>

#include <opencv2/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <dlib/image_processing/frontal_face_detector.h>
#include <dlib/image_processing.h>

#include <boost/filesystem.hpp>
#include <boost/foreach.hpp>
#include "Ctracker.hpp"
#include "RecentFaceList.hpp"

using namespace dlib;
using namespace boost::filesystem;
using namespace cv;
using namespace std;

RecentFacesPresent::RecentFacesPresent(){
    random = 0;
    resized_face_width = FRAME_WIDTH * 70 / 1280;
    resized_face_height = FRAME_HEIGHT * 70 / 720;
}

RecentFacesPresent::~RecentFacesPresent(){
}

void RecentFacesPresent::Get_Face_inGallery(std::string folderpath, std::string faceid, std::string face_image_id, std::string priority_face_pose) {
    bool debug = false;
    if (debug) {
        std::cout << folderpath << " >> " << faceid << " >> " << face_image_id << std::endl;
    }

    if (!random) { // take exactly nearest face
        path nearest_image_path(folderpath + faceid + "/" + face_image_id + ".jpg");
        if (boost::filesystem::exists(nearest_image_path)) {
            face_in_gallery = cv::imread(nearest_image_path.string());
            cv::resize(face_in_gallery, face_in_gallery,Size(resized_face_width, resized_face_height));
            if (debug) {
                std::cout << nearest_image_path.string() << std::endl;
            }
            return;
        }
    }

    // random image
    std::srand (time(NULL));

    path dpath(folderpath + faceid);

    std::vector<boost::filesystem::path> found_images;
    found_images.clear();
    BOOST_FOREACH(const path& p, std::make_pair(directory_iterator(dpath), directory_iterator())) {
        if (is_directory(p)) {
            continue;
        } else if(!is_regular_file(p)) {
            continue;
        } else if(p.extension().string() == ".jpg" && p.filename().string().find(priority_face_pose) != std::string::npos) {
            found_images.push_back(p);
        }
    }
    if (found_images.size() > 0) {
        int id = std::rand() % found_images.size();
        face_in_gallery = cv::imread(found_images.at(id).string());
        cv::resize(face_in_gallery, face_in_gallery,Size(resized_face_width, resized_face_height));
        return;
    }

    // if straight face not found >> take first jpg
    found_images.clear();
    BOOST_FOREACH(const path& p, std::make_pair(directory_iterator(dpath), directory_iterator())) {
        if (is_directory(p)) continue;
        if(!is_regular_file(p)) continue;

        else if(p.extension().string() == ".jpg") {
            found_images.push_back(p);
        }
    }
    if (found_images.size() > 0) {
        int id = std::rand() % found_images.size();
        face_in_gallery = cv::imread(found_images.at(id).string());
        cv::resize(face_in_gallery, face_in_gallery,Size(resized_face_width, resized_face_height));
    }
}

void RecentFacesPresent::Get_Time_Present(){

    time_t now = time(0);
    static char timestr[10];
    strftime(timestr, sizeof(timestr), "%H:%M", localtime(&now));
    time_present = timestr;

}
int RecentFacesPresent::Get_How_Long_Last_Present(){
    int hour, minute;
    int curhour, curminute;
    time_t now = time(0);
    static char timestr[10];
    strftime(timestr, sizeof(timestr), "%H:%M", localtime(&now));

    char *cstr = &time_present[0u];
    std::sscanf(cstr, "%d:%d", &hour, &minute);
    std::sscanf(timestr, "%d:%d", &curhour, &curminute);
    int duration_mins = std::abs(curhour*60 + curminute - hour*60 - minute);

    return duration_mins;
}

void RecentFacesPresent::setTimePresent(time_t _time) {
    if (_time > 0) {
        static char timestr[10];
        strftime(timestr, sizeof(timestr), "%H:%M", localtime(&_time));
        time_present = timestr;
    }
}

void RecentFacesPresent::setLastTimePresent(time_t _last_time) {
    if (_last_time > 0) {
        static char timestr[10];
        strftime(timestr, sizeof(timestr), "%H:%M", localtime(&_last_time));
        last_time_present = timestr;
    }
}
