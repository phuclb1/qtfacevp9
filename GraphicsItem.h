#ifndef GRAPHICSITEM_H
#define GRAPHICSITEM_H

#include <QColor>
#include <QGraphicsItem>

class GraphicsItem : public QGraphicsItem {
public:
  GraphicsItem(const QColor &_color, int _x, int _y, QString _pose_id,
               QString _full_path);
  GraphicsItem(const QColor &_color, int _x, int _y, QImage &_img);
  QRectF boundingRect() const;
  QPainterPath shape() const;
  void paint(QPainter *painter, const QStyleOptionGraphicsItem *item,
             QWidget *widget);

protected:
  void mousePressEvent(QGraphicsSceneMouseEvent *event);
  void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
  void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);

private:
  int x;
  int y;
  QColor color;
  QVector<QPointF> stuff;

  QString pose_id;
  QString path;
  QImage image;
};

#endif // GRAPHICSITEM_H
