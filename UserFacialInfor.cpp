#include "config.h"
#include <dlib/image_saver/save_jpeg.h>
#include <iostream>
#include <string>
#include <fstream>
#include <streambuf>

#include <stdio.h>
#include <time.h>

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <boost/foreach.hpp>

#include "UserFacialInfor.hpp"

using namespace boost::filesystem;
using namespace dlib;
using namespace std;
using namespace cv;

User_Facial_Infor::User_Facial_Infor(){
    direction_h = "";
    direction_v = "";
    angle_h = 0;
    angle_v = 0;
    smiling = false;
    eyeopening = false;
    saved_straight_face = false;
    saved_right30_face = false;
    saved_right45_face = false;
    saved_left30_face = false;
    saved_left45_face = false;
    saved_up30_face = false;
    saved_down30_face = false;
    saved_smiling_face = false;

    max_number_of_faces = 30;

    //eye_open_rate=0.0;
    mouth_open_rate=0.0;

}

//The first face to save to new person has to be a straight face
//After detect the straight face, other angle faces will be saved arcordingly
User_Facial_Infor::User_Facial_Infor(matrix<rgb_pixel> first_image_saving, string folderpath, string filename){
    direction_h = "";
    direction_v = "";
    angle_h = 0;
    angle_v = 0;
    smiling = false;
    saved_straight_face = true;
    saved_right30_face = false;
    saved_right45_face = false;
    saved_left30_face = false;
    saved_left45_face = false;
    saved_up30_face = false;
    saved_down30_face = false;
    saved_smiling_face = false;

    //eye_open_rate=0.0;
    mouth_open_rate=0.0;
    max_number_of_faces = 30;

    //string folderpath = folderpath+auto_faceid;
    //folderpath includes the auto_faceid
    string str = folderpath + "/" + filename + ".jpg";
    //cout<<str1;

    dlib::save_jpeg(first_image_saving, str);
}

bool User_Facial_Infor::Is_Looking_Straight(){
    if (direction_h.empty() && direction_v.empty()) return true;
    else return false;
}

bool User_Facial_Infor::Existing_FaceImage_InGallery(std::vector<bool>& index_saved_face){
    if (saved_straight_face) index_saved_face.push_back(true); //index for straight face is 0
    else index_saved_face.push_back(false);

    if (saved_right30_face) index_saved_face.push_back(true); //and so on 1
    else index_saved_face.push_back(false);

    if (saved_right45_face) index_saved_face.push_back(true); //and so on 2
    else index_saved_face.push_back(false);

    if (saved_left30_face) index_saved_face.push_back(true); //and so on 3
    else index_saved_face.push_back(false);

    if (saved_left45_face) index_saved_face.push_back(true); //and so on 4
    else index_saved_face.push_back(false);

    if (saved_up30_face) index_saved_face.push_back(true); //and so on 5
    else index_saved_face.push_back(false);

    if (saved_down30_face) index_saved_face.push_back(true); //and so on 6
    else index_saved_face.push_back(false);

    if (saved_smiling_face) index_saved_face.push_back(true); //and so on 7
    else index_saved_face.push_back(false);

    //In total there are 8 face images that need to be saved (optional)
    bool existing_face = false;
    for (size_t i=0;i<index_saved_face.size();i++){
        if (index_saved_face[i]) existing_face = true;
    }

    return existing_face;
}

bool User_Facial_Infor::Is_Enough_Face_Samples(std::vector<bool>& index_saved_face){

     Existing_FaceImage_InGallery(index_saved_face);
     bool enough_face_sample = true;
     for (size_t i=0;i<index_saved_face.size();i++){
         if (!index_saved_face[i]) {
             enough_face_sample = false;
             break;
         }
     }

     return enough_face_sample;
}

//Assuming that we have the straight face saved, then we now want to save other pose image
void User_Facial_Infor::Second_Image_Saving(matrix<rgb_pixel> second_image_saving, string folderpath){
    //save only one image at a time
    if (Is_Looking_Straight() && smiling && !saved_smiling_face){
        string str = folderpath + "/" + "smiling.jpg";
        dlib::save_jpeg(second_image_saving, str);
        saved_smiling_face = true;
    }else if (direction_h == "right" && angle_h == 30 && !saved_right30_face){
        string str = folderpath + "/" + "right30.jpg";
        dlib::save_jpeg(second_image_saving, str);
        saved_right30_face = true;
    }else if (direction_h == "right" && angle_h == 45 && !saved_right45_face){
        string str = folderpath + "/" + "right45.jpg";
        dlib::save_jpeg(second_image_saving, str);
        saved_right45_face = true;
    }else if (direction_h == "left" && angle_h == 30 && !saved_left30_face){
        string str = folderpath + "/" + "left30.jpg";
        dlib::save_jpeg(second_image_saving, str);
        saved_left30_face = true;
    }else if (direction_h == "left" && angle_h == 45 && !saved_left45_face){
        string str = folderpath + "/" + "left45.jpg";
        dlib::save_jpeg(second_image_saving, str);
        saved_left45_face = true;
    }else if (direction_h == "up" && angle_h == 30 && !saved_up30_face){
        string str = folderpath + "/" + "up30.jpg";
        dlib::save_jpeg(second_image_saving, str);
        saved_up30_face = true;
    }else if (direction_h == "down" && angle_h == 30 && !saved_down30_face){
        string str = folderpath + "/" + "down30.jpg";
        dlib::save_jpeg(second_image_saving, str);
        saved_down30_face = true;
    }
}

void User_Facial_Infor::Update_Saved_xxx_Face(path p){
    //The face image will be saved based on the face-direction and angle
    BOOST_FOREACH(const path& sp, std::make_pair(directory_iterator(p), directory_iterator())) {
        if (is_directory(sp)) continue;
        if(!is_regular_file(sp)) continue;

        else if(sp.extension().string() == ".jpg"){
            if (sp.filename().stem().string().substr(0,8) == "straight") saved_straight_face = true; //this is using for only one image for one pose
            if (sp.filename().stem() == "right30") saved_right30_face = true;
            if (sp.filename().stem() == "right45") saved_right45_face = true;
            if (sp.filename().stem() == "left30") saved_left30_face = true;
            if (sp.filename().stem() == "left45") saved_left45_face = true;
            if (sp.filename().stem() == "up30") saved_up30_face = true;
            if (sp.filename().stem() == "down30") saved_down30_face = true;
            if (sp.filename().stem() == "smiling") saved_smiling_face = true;
        }
    }
}

void User_Facial_Infor::Update_Smile_Ratio(const full_object_detection shape){
    if (shape.num_parts()!=68) return;
    //using eye_rate is not reliable because eyes are blink some times
    Point pt_eyeleft(shape.part(39)(0),shape.part(39)(1));
    Point pt_eyeright(shape.part(42)(0),shape.part(42)(1));

//       Point pt_eyeleft_upper(shape.part(38)(0),shape.part(38)(1));
//       Point pt_eyeleft_lower(shape.part(40)(0),shape.part(40)(1));
//       Point pt_eyeright_upper(shape.part(43)(0),shape.part(43)(1));
//       Point pt_eyeright_lower(shape.part(47)(0),shape.part(47)(1));

    Point pt_mouthleft(shape.part(48)(0),shape.part(48)(1));
    Point pt_mouthright(shape.part(54)(0),shape.part(54)(1));

    Point diff = pt_eyeright - pt_eyeleft;
    int eyeleft_to_eyeright = diff.x*diff.x + diff.y*diff.y;
    diff = pt_mouthright - pt_mouthleft;
    int mouthleft_to_mouthright = diff.x*diff.x + diff.y*diff.y;
//       diff = pt_eyeleft_lower - pt_eyeleft_upper;
//       int eyeleft_open_range = diff.x*diff.x + diff.y*diff.y;
//       diff = pt_eyeright_lower - pt_eyeright_upper;
//       int eyeright_open_range = diff.x*diff.x + diff.y*diff.y;

//       float eye_open_rate1 = (float) eyeleft_to_eyeright / eyeleft_open_range;
//       float eye_open_rate2 = (float) eyeleft_to_eyeright / eyeright_open_range;

    //Calculate the average over some frames
    if (mouth_open_rate ==0.0){
//           eye_open_rate = std::min(eye_open_rate1, eye_open_rate2);
        mouth_open_rate = (float) mouthleft_to_mouthright / eyeleft_to_eyeright;
    }
    else{
//           eye_open_rate = (eye_open_rate + std::min(eye_open_rate1, eye_open_rate2)) / 2.0;
        mouth_open_rate = (mouth_open_rate + (float) mouthleft_to_mouthright / eyeleft_to_eyeright) / 2.0;
    }
}

bool User_Facial_Infor::Is_Smiling_Face(const full_object_detection shape){
    if (shape.num_parts()!=68) return false;
    //using eye_rate is not reliable because eyes are blink some times
    Point pt_eyeleft(shape.part(39)(0),shape.part(39)(1));
    Point pt_eyeright(shape.part(42)(0),shape.part(42)(1));

//       Point pt_eyeleft_upper(shape.part(38)(0),shape.part(38)(1));
//       Point pt_eyeleft_lower(shape.part(40)(0),shape.part(40)(1));
//       Point pt_eyeright_upper(shape.part(43)(0),shape.part(43)(1));
//       Point pt_eyeright_lower(shape.part(47)(0),shape.part(47)(1));

    Point pt_mouthleft(shape.part(48)(0),shape.part(48)(1));
    Point pt_mouthright(shape.part(54)(0),shape.part(54)(1));

    Point diff = pt_eyeright - pt_eyeleft;
    int eyeleft_to_eyeright = diff.x*diff.x + diff.y*diff.y;
    diff = pt_mouthright - pt_mouthleft;
    int mouthleft_to_mouthright = diff.x*diff.x + diff.y*diff.y;
//       diff = pt_eyeleft_lower - pt_eyeleft_upper;
//       int eyeleft_open_range = diff.x*diff.x + diff.y*diff.y;
//       diff = pt_eyeright_lower - pt_eyeright_upper;
//       int eyeright_open_range = diff.x*diff.x + diff.y*diff.y;

//       float eye_open_rate1 = (float) eyeleft_to_eyeright / eyeleft_open_range;
//       float eye_open_rate2 = (float) eyeleft_to_eyeright / eyeright_open_range;
//       float current_eye_open_rate = std::min(eye_open_rate1, eye_open_rate2);
    float current_mouth_open_rate = (float) mouthleft_to_mouthright / eyeleft_to_eyeright;

//       cout<<"eye_open_rate "<<current_eye_open_rate / eye_open_rate<<endl;
//       cout<<"mouth_open_rate "<<current_mouth_open_rate / mouth_open_rate<<endl;

    if (current_mouth_open_rate / mouth_open_rate > 1.2)
        return true;
    else
        return false;
}

bool User_Facial_Infor::Is_Eyes_Openning(const full_object_detection shape){
    Point pt_eyeleft_left(shape.part(36)(0),shape.part(36)(1));
    Point pt_eyeleft_right(shape.part(39)(0),shape.part(39)(1));

    Point pt_eyeleft_upper(shape.part(38)(0),shape.part(38)(1));
    Point pt_eyeleft_lower(shape.part(40)(0),shape.part(40)(1));
    Point pt_eyeright_upper(shape.part(43)(0),shape.part(43)(1));
    Point pt_eyeright_lower(shape.part(47)(0),shape.part(47)(1));

    Point diff = pt_eyeleft_right - pt_eyeleft_left;
    int eye_width = diff.x*diff.x + diff.y*diff.y;
    diff = pt_eyeleft_lower - pt_eyeleft_upper;
    int eye_left_height = diff.x*diff.x + diff.y*diff.y;
    diff = pt_eyeright_lower - pt_eyeright_upper;
    int eye_right_height = diff.x*diff.x + diff.y*diff.y;

    float eye_left_open_rate = (float) eye_left_height/eye_width;
    float eye_right_open_rate = (float) eye_right_height/eye_width;

    //cout <<"eye_left_open_rate "<<eye_left_open_rate<<endl;
    if (eye_left_open_rate < 0.04 && eye_right_open_rate<0.04) return false;
    else return true;
}

bool User_Facial_Infor::Calculate_Angle_Direction_Landmarkbased(const full_object_detection shape) //, int& angle_h, int& angle_v, string& direction_h, string& direction_v)
{
    if (shape.num_parts()!=68) return false;
    //bool smiling;

    Point pt_nosetop(shape.part(30)(0),shape.part(30)(1));
    Point pt_noseup(shape.part(27)(0),shape.part(27)(1));
    Point pt_earleft(shape.part(1)(0),shape.part(1)(1));
    Point pt_earright(shape.part(15)(0),shape.part(15)(1));
    Point pt_eyeleft(shape.part(39)(0),shape.part(39)(1));
    Point pt_eyeright(shape.part(42)(0),shape.part(42)(1));

    Point pt_noseleft(shape.part(31)(0),shape.part(31)(1));
    Point pt_noseright(shape.part(35)(0),shape.part(35)(1));
    Point pt_nosemiddle(shape.part(33)(0),shape.part(33)(1));

    Point pt_liptop(shape.part(62)(0),shape.part(62)(1));
    Point pt_lipbottom(shape.part(66)(0),shape.part(66)(1));

    Point diff = pt_nosetop - pt_earleft;
    int earleft_to_nose = diff.x*diff.x + diff.y*diff.y;
    diff = pt_nosetop - pt_earright;
    int earright_to_nose = diff.x*diff.x + diff.y*diff.y;
    diff = pt_nosetop - pt_noseup;
    int noseup_to_nosetop = diff.x*diff.x + diff.y*diff.y;
    diff = pt_eyeleft - pt_eyeright;
    int eyeright_to_eyeleft = diff.x*diff.x + diff.y*diff.y;

    diff = pt_noseleft - pt_noseright;
    int noseright_to_noseleft = diff.x*diff.x + diff.y*diff.y;
    diff = pt_nosemiddle - pt_nosetop;
    int nosemiddle_to_nosetop = diff.x*diff.x + diff.y*diff.y;

    diff = pt_liptop - pt_lipbottom;
    //int liptop_to_lipbottom = diff.x*diff.x + diff.y*diff.y;

    //cout<<earright_to_nose<<" "<<earleft_to_nose<< " "<< (float) earleft_to_nose / earright_to_nose<< endl;

    float threshold_0 = 0.6;
    float threshold_30 = 0.3;

    //Horizontal
    if ((float) earleft_to_nose / earright_to_nose > threshold_0 || (float) earright_to_nose / earleft_to_nose > threshold_0) {
        direction_h = "";
        angle_h = 0;
    }
    if ((float) earleft_to_nose / earright_to_nose <= threshold_0 && (float) earleft_to_nose / earright_to_nose >= threshold_30){
        direction_h = "left";
        angle_h = 30;
    }
    if ((float) earleft_to_nose / earright_to_nose < threshold_30){
        direction_h = "left";
        angle_h = 45;
    }
    if ((float) earright_to_nose / earleft_to_nose <= threshold_0 && (float) earright_to_nose / earleft_to_nose >= threshold_30){
        direction_h = "right";
        angle_h = 30;
    }
    if ((float) earright_to_nose / earleft_to_nose < threshold_30){
        direction_h = "right";
        angle_h = 45;
    }

    //Vertial
    if ((float) noseup_to_nosetop / eyeright_to_eyeleft > threshold_0) {
        direction_v = "";
        angle_v = 0;
    }
    if ((float) noseup_to_nosetop / eyeright_to_eyeleft <= threshold_0){
        direction_v = "up";
        angle_v = 30;
    }
    if ((float) nosemiddle_to_nosetop / noseright_to_noseleft <= 0.2){
        direction_v = "down";
        angle_v = 30;
    }
    return true;
}

///
/// \brief Save_More_FaceImage_For1FaceID
/// \param gallery_face_descriptor : gallery_face_descriptor contains the mapping between image name (straight1, straight2,...) and image_rgb
/// \param image_saving: the image that we want to save of possible
/// \param folderpath:  have the path upto the faceid folder
/// \return true if success save, false otherwise
///
bool User_Facial_Infor::Save_More_FaceImage_For1FaceID(std::map<string,matrix<float,0,1>>& gallery_face_descriptor,
                                                       matrix<float,0,1> vector_face_descriptor, matrix<rgb_pixel> image_saving,
                                                       string folderpath, string file_prefix){
    //currently max_number_of_faces = 30
    if (gallery_face_descriptor.size() >= max_number_of_faces) return false;
    cout<<"You have "<<gallery_face_descriptor.size()<< " in gallery"<<endl;
    time_t now = time(0);
    static char auto_postfix[10];
    strftime(auto_postfix, sizeof(auto_postfix), "%M%S", localtime(&now));

    string key = file_prefix + auto_postfix;
    if (gallery_face_descriptor.count(key)==0){
        //add image
        string str = folderpath + "/" + key + ".jpg";
        dlib::save_jpeg(image_saving, str);
        gallery_face_descriptor.insert(std::pair<string,matrix<float,0,1>>(key, vector_face_descriptor));

        cout<<"Just added one more "<<file_prefix<< " face"<<endl;
        return true;
    }

    return false;
}
