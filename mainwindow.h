#ifndef MAINWINDOW_H
#define MAINWINDOW_H

// QT_BEGIN_NAMESPACE
class QGraphicsScene;
class QSplitter;
class QMainWindow;
class QFileDialog;
class QMessageBox;
class QTime;
class QSlider;
class QLabel;
class QPushButton;
class QMenuBar;
class QToolBar;
class QHBoxLayout;
class QVBoxLayout;
class QStatusBar;

class QListWidget;
class QListWidgetItem;
class QIcon;
class QGroupBox;

// QT_END_NAMESPACE
#include "BinaryData.h"
#include "ScatterDataModifier.hpp"
//#include "SocketIoUtils.hpp"
#include "Switch.h"
#include "View.h"
#include <Player.h>
#include <Q3DScatter>
#include <QMainWindow>
#include <QQuickView>
#include <QQuickWidget>
#include <QtDataVisualization>
#include <algorithm>
//#include <socket.h>
//#include <QFileDialog>
//#include <QMessageBox>
//#include <QTime>
//#include <QSlider>
//#include <QLabel>
//#include <QPushButton>
//#include <QMenuBar>
//#include <QToolBar>
//#include <QHBoxLayout>
//#include <QVBoxLayout>
//#include <QStatusBar>

#include <Qt3DCore>
#include <Qt3DExtras>
#include <Qt3DRender>

#include <QSet>

#include <cstdlib>
#include <functional>
#include <mutex>
#include <sio_client.h>
using namespace sio;
struct Point2D {
  float x, y;
  Point2D(float px = 0, float py = 0) : x(px), y(py) {}
};
class MainWindow : public QMainWindow {
  Q_OBJECT

public:
  explicit MainWindow(QWidget *parent = 0);
  ~MainWindow();
  void populateScene();
  void updateScene();
  void findCloserFace(std::vector<int> &index, double &distance_sum,
                      matrix<float, 0, 1> vec128);
  Point2D findPosOfCluster(std::string faceId);
  void populateScene(std::string faceId, dlib::matrix<float, 0, 1> vec);
  void prepare3dChart();

private Q_SLOTS:
  // Display video frame in player UI
  void updatePlayerUI(QImage img);
  void on_pushButton_clicked();
  void on_pushButton_2_clicked();
  QString getFormattedTime(int timeInSeconds);
  void on_horizontalSlider_sliderPressed();
  void on_horizontalSlider_sliderReleased();
  void on_horizontalSlider_sliderMoved(int position);
  void on_switch_clicked();
  void on_switch_graph_clicked();
  void on_catch_new_person_appear(QVector<matrix<float, 0, 1>> vec,
                                  QVector<matrix<rgb_pixel>> img_vec);
  void on_catch_new_person_appear_1(int a) { cout << "catch : " << a << endl; }

  void onFaceAdded(const QString &face_id, const QImage &face_image);
  void on3DFaceChange(const QString &face_id);

  void onGraphButtonClicked();

private:
  void OnNewProfile(std::string const &name, message::ptr const &data,
                    bool hasAck, message::list &ack_resp);
  void OnConnected(std::string const &nsp);
  void OnClosed(client::close_reason const &reason);
  void OnFailed();
  QListWidget *list_widget;
  QListWidgetItem *item_1;
  QListWidgetItem *item_2;
  QListWidgetItem *item_3;

  QWidget *centralWidget;

  QVBoxLayout *vboxlayout;
  QVBoxLayout *vboxlayout2;
  QSplitter *vsplitter;
  QSplitter *vsplitter2;

  QHBoxLayout *hboxlayout;
  QWidget *widget_0;

  QLabel *video_label;
  //	QLabel *graph_label;

  QHBoxLayout *hboxlayout_1;
  QWidget *widget_1;

  QSlider *horizontalSlider;
  QLabel *label_2;
  QLabel *label_3;

  QHBoxLayout *hboxlayout_2;
  QWidget *widget_2;

  QHBoxLayout *hboxlayout_3;
  QWidget *widget_3;
  Switch *switch_3;

  QPushButton *pushButton;
  QPushButton *pushButton_2;
  QPushButton *push_button_show_graph; // button to show / hide graph

  QMenuBar *menuBar;
  QToolBar *mainToolBar;
  QStatusBar *statusBar;

  Player *myPlayer;
  //  Socket *mySocket;

  QGraphicsScene *scene;
  QGraphicsScene *newScene;
  QSplitter *splitter;

  View *view;
  View *newView;

  QHBoxLayout *hboxlayout_graph;
  QWidget *widget_graph;
  Switch *switch_graph;

  QtDataVisualization::Q3DScatter *dGraph;
  ScatterDataModifier *modifier;
  QWidget *container3DGraph;

  std::shared_ptr<Gallery> data;
  std::map<std::string, std::shared_ptr<Gallery>> list3dData;
  std::string latest_face_id;
  QScatter3DSeries *current_series;
  std::string current_face_id;

  //  QQuickView * view_3d;
  //  QWidget * widget_3d;
  QQuickWidget *widget_3d;
  QVBoxLayout *layout_3d;
  QGroupBox *group_box_3d;
  QString current_3d_face_id;
  QSet<QString> face_id_3d_set;

  std::unique_ptr<client> _io;
  client h;

public:
  //  SocketIoUtils *socket;
  std::vector<QGraphicsItem *> current_item_vec;
};

double calculateDistaneBtwTwoPoint(Point2D point0, Point2D point1);

int calculateIntersectionPoint(Point2D point0, Point2D point1, double r0,
                               double r1,
                               std::vector<Point2D> &intersectionPoint);

double calculateRadius(std::vector<Point2D> point_vec, double ratio_12,
                       double ratio_13);

int isPointInsideCircle(Point2D point, Point2D circlePoint,
                        double circleRadius);
bool checkPoint(std::vector<Point2D> point_vec, std::vector<double> radius_vec,
                std::vector<Point2D> intersectionPoint);

Point2D findPosOnGraph(std::vector<Point2D> point_vec,
                       std::vector<double> distance_vec);
int findMax(std::vector<double> vec);
void updatePosition(Point2D &point, Point2D target_point);
bool checkDistane(std::vector<double> distance128_vec,
                  std::vector<double> distance2_vec,
                  std::vector<double> &divide_vec);

#endif // MAINWINDOW_H
