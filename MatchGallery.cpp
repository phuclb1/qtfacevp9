#include "config.h"
#include <dlib/opencv.h>
#include <sstream>
// #include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <opencv2/objdetect/objdetect.hpp>
// #include <opencv2/calib3d/calib3d.hpp>

#include <dlib/clustering.h>
#include <dlib/dnn.h>
#include <dlib/gui_widgets.h>
#include <dlib/image_io.h>
#include <dlib/image_processing.h>
#include <dlib/image_processing/frontal_face_detector.h>
#include <dlib/image_processing/render_face_detections.h>

#include "MatchGallery.hpp"
#include "curl/curl.h"
#include "utils.hpp"
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <boost/foreach.hpp>
#include <dirent.h>
#include <fstream>
#include <iostream>
#include <jsoncpp/json/json.h>
#include <map>
#include <math.h>
#include <queue>
#include <string.h>
#include <thread>

// #include "dbinfo.h"

#define FACE_DOWNSAMPLE_RATIO 1.0
#define SKIP_FRAMES 1
#define FILE_NOT_FOUND_ERROR_CODE 20
CURL *curl;
int sendDataToImgServer(cv::Mat mat, std::string fileName, std::string faceId,
                        dlib::matrix<float, 0, 1> vec) {
  bool debug = true;
  if (debug) {
    std::cout << "main::test_send_image >> start" << std::endl;
  }
  std::vector<unsigned char> imgBuffer;
  cv::imencode(".jpg", mat, imgBuffer);

  // Buffer to String with pointer/length
  unsigned char *imgBuffPtr = &imgBuffer[0];
  long imgBuffLength = imgBuffer.size();

  // CURL SETUP
  // Set headers as null
  struct curl_slist *headers = NULL;
  struct curl_httppost *formpost = NULL;
  struct curl_httppost *lastptr = NULL;

  // Data type
  headers = curl_slist_append(headers, "Content-Type: multipart/form-data");

  curl_formadd(&formpost, &lastptr, CURLFORM_COPYNAME, "imgUploader",
               CURLFORM_BUFFER, fileName.c_str(), CURLFORM_BUFFERPTR,
               imgBuffPtr, CURLFORM_BUFFERLENGTH, imgBuffLength, CURLFORM_END);
  // face id
  if (debug) {
    std::cout << "main >> test_send_image_and_vector >> faceid = " << faceId
              << std::endl;
  }

  curl_formadd(&formpost, &lastptr, CURLFORM_COPYNAME, "faceId",
               CURLFORM_COPYCONTENTS, faceId.c_str(), CURLFORM_END);

  // vector 128 to string
  std::stringstream stream;

  int K = vec.size();
  //  stream << K << " ";
  for (int i = 0; i < K; ++i) {
    stream << std::fixed << std::setprecision(6) << vec(i) << " ";
  }

  if (debug) {
    std::cout << "main >> test_send_image_and_vector >> stream = "
              << stream.str() << std::endl;
  }

  std::string vec2string = stream.str();
  curl_formadd(&formpost, &lastptr, CURLFORM_COPYNAME, "vector",
               CURLFORM_COPYCONTENTS, vec2string.c_str(), CURLFORM_END);

  // init easy_curl
  curl = curl_easy_init();

  if (!curl) {
    return 1;
  }

  std::string url = "http://api.hrl.vp9.vn/api/vupload";
  //  std::string url = "http://27cf7ae0.ngrok.io/api/vupload";
  curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
  curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 1);
  curl_easy_setopt(curl, CURLOPT_FAILONERROR, 1);
  curl_easy_setopt(curl, CURLOPT_NOBODY, 1);
  curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
  curl_easy_setopt(curl, CURLOPT_HTTPPOST, formpost);

  CURLcode res = curl_easy_perform(curl);
  if (debug) {
    std::cout << "sendData >> sent >> " << curl_easy_strerror(res) << std::endl;
  }

  curl_easy_cleanup(curl);
  curl_formfree(formpost);
  curl_slist_free_all(headers);
  if (debug) {
    std::cout << "sendData >> finish" << std::endl;
  }
  return 0;
}
namespace {
std::size_t callback(const char *in, std::size_t size, std::size_t num,
                     std::string *out) {
  const std::size_t totalBytes(size * num);
  out->append(in, totalBytes);
  return totalBytes;
}
}
int MatchAgainstGallery::getNewProfile(std::string URL) {
  cout << "getNewProfile : " << URL << endl;
  CURL *curl2 = curl_easy_init();
  curl_easy_setopt(curl2, CURLOPT_URL, URL.c_str());

  // Don't bother trying IPv6, which would increase DNS resolution time.
  curl_easy_setopt(curl2, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);

  // Don't wait forever, time out after 10 seconds.
  curl_easy_setopt(curl2, CURLOPT_TIMEOUT, 10);

  // Follow HTTP redirects if necessary.
  curl_easy_setopt(curl2, CURLOPT_FOLLOWLOCATION, 1L);

  // Response information.
  int httpCode(0);
  std::unique_ptr<std::string> httpData(new std::string());

  // Hook up data handling function.
  curl_easy_setopt(curl2, CURLOPT_WRITEFUNCTION, callback);

  // Hook up data container (will be passed as the last parameter to the
  // callback handling function).  Can be any pointer type, since it will
  // internally be passed as a void pointer.
  curl_easy_setopt(curl2, CURLOPT_WRITEDATA, httpData.get());

  // Run our HTTP GET command, capture the HTTP response code, and clean up.
  curl_easy_perform(curl2);
  curl_easy_getinfo(curl2, CURLINFO_RESPONSE_CODE, &httpCode);
  curl_easy_cleanup(curl2);
  if (httpCode == 200) {
    std::cout << "\nGot successful response from " << URL << std::endl;
    Json::Value jsonData;
    Json::Reader jsonReader;

    if (jsonReader.parse(*httpData.get(), jsonData)) {
      std::cout << "Successfully parsed JSON data" << std::endl;
      std::cout << "\nJSON data received:" << std::endl;
      //      std::cout << jsonData.toStyledString() << std::endl;
      std::map<string, matrix<float, 0, 1>> gallery_face_descriptor;
      for (auto row : jsonData["rows"]) {

        std::string faceId = row["name"].asString();
        cout << "faceid: " << faceId << endl;
        if (gallery_face_descriptors.find(faceId) ==
            gallery_face_descriptors.end()) {
          for (auto itr : row["photos"]) {
            string src = itr["src"].asString();
            string vecto = itr["vector"].asString();
            std::stringstream ss(vecto);
            std::istream_iterator<std::string> begin(ss);
            std::istream_iterator<std::string> end;
            std::vector<std::string> vstrings(begin, end);
            int i = 0;
            dlib::matrix<float, 128, 1> newVec;

            cout << newVec.nc() << "--" << newVec.nr() << endl;
            for (std::string a : vstrings) {
              float tmp;
              stringstream ss;
              ss << a;
              ss >> tmp;
              ss.clear();
              newVec(i) = tmp;
              i++;
            }

            this->updateProfile(src, faceId, newVec);
          }
        }
      }
      //      const std::string dateString(jsonData["date"].asString());
      //      const std::size_t unixTimeMs(
      //          jsonData["milliseconds_since_epoch"].asUInt64());
      //      const std::string timeString(jsonData["time"].asString());

      //      std::cout << "Natively parsed:" << std::endl;
      //      std::cout << "\tDate string: " << dateString << std::endl;
      //      std::cout << "\tUnix timeMs: " << unixTimeMs << std::endl;
      //      std::cout << "\tTime string: " << timeString << std::endl;
      //      std::cout << std::endl;
      httpData->clear();
    } else {
      std::cout << "Could not parse HTTP data as JSON" << std::endl;
      std::cout << "HTTP data was:\n" << *httpData.get() << std::endl;
      return 1;
    }
  }
  cout << "getNewProfile 4 " << endl;
}
// curl
size_t write_data(char *ptr, size_t size, size_t nmemb, void *userdata) {
  std::vector<uchar> *stream = (std::vector<uchar> *)userdata;
  size_t count = size * nmemb;
  stream->insert(stream->end(), ptr, ptr + count);
  return count;
}

// function to retrieve the image as cv::Mat data type
cv::Mat curlImg(const char *img_url, int timeout = 10) {
  std::vector<uchar> stream;
  CURL *curl = curl_easy_init();
  curl_easy_setopt(curl, CURLOPT_URL, img_url); // the img url
  curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION,
                   write_data); // pass the writefunction
  curl_easy_setopt(curl, CURLOPT_WRITEDATA,
                   &stream); // pass the stream ptr to the writefunction
  curl_easy_setopt(curl, CURLOPT_TIMEOUT,
                   timeout);                 // timeout if curl_easy hangs,
  CURLcode res = curl_easy_perform(curl);    // start curl
  curl_easy_cleanup(curl);                   // cleanup
  return imdecode(stream, cv::IMREAD_COLOR); // 'keep-as-is'
}
union Float2Char {
  float floatValue;
  unsigned char charArray[4];
};
/**
 * @brief convert float to unsigned char[4]
 * @param _number: float
 * @return unsigned char[4]
 */
std::vector<unsigned char> float2CharVector4(float _number) {
  Float2Char n;
  n.floatValue = _number;
  std::vector<unsigned char> _result = std::vector<unsigned char>();
  for (int i = 0; i < 4; ++i) {
    _result.push_back(n.charArray[i]);
  }
  return _result;
}

/**
 * @brief convert unsigned char[4] to float
 * @param _charVector: unsigned char[4]
 * @return float
 */
double charVector4_2Float(std::vector<unsigned char> _charVector) {
  Float2Char n;
  for (int i = 0; i < 4; ++i) {
    n.charArray[i] = _charVector.at(i);
  }
  return n.floatValue;
}

MatchAgainstGallery::MatchAgainstGallery() {
  folderpath = "Gallery_demo/";
  trainingimagepath = "Training_images/";

  threshold = 0.30; // 0.37; // to decide the faceid if distance smaller than
  // this value

  // Set_Single_Level_Detector();
  // max_numframes_confirm_identity = 2;

  use_multiface_samples_autosave = true;
  // roi_region_shift_value = 0;//0 no moving the ROI, -1 move it to bottom; +1
  // move it up
  // max_length_recent_faces_list = 8;
  max_numberof_faceimages_for1faceid = 20;

  HISTOGRAM_X0 = 0.2;
  HISTOGRAM_X1 = 0.8;
  HISTOGRAM_N = 10;
  HISTOGRAM_DX = (HISTOGRAM_X1 - HISTOGRAM_X0) / HISTOGRAM_N;

  pose_list.clear();
  pose_list.push_back("straight");
  pose_list.push_back("left");
  pose_list.push_back("right");
  pose_list.push_back("up");
  pose_list.push_back("down");
  pose_list.push_back("smiling");

  GRAPH_N = 8;
  max_distance_ever = -100;
  maxOfVec = 500;
}

// Using only frontal face detector, remove 4 others (left, right, up, down)
void MatchAgainstGallery::Set_Single_Level_Detector(
    frontal_face_detector &detector_) {
  detector_ = get_frontal_face_detector();

  // dlib::serialize("current.svm")<< detector;

  std::vector<frontal_face_detector> parts;
  // Split into parts and serialize to disk
  for (unsigned long i = 0; i < detector_.num_detectors(); ++i) {
    dlib::frontal_face_detector part(detector_.get_scanner(),
                                     detector_.get_overlap_tester(),
                                     detector_.get_w(i));
    // dlib::serialize("part" + std::to_string(i) + ".svm") << part;

    // if (i==0 ||i==1||i==2){//Seem that this is frontal one
    if (i == 0) { // Seem that this is frontal one
      parts.push_back(part);
      break;
    }
  }

  // Reconstruct original detector
  frontal_face_detector reconstructed(parts);
  // dlib::serialize("reconstructed.svm") << reconstructed;

  // Create detector that will work only on one level of pyramid
  typedef dlib::scan_fhog_pyramid<dlib::pyramid_down<6>> image_scanner_type;
  image_scanner_type scanner;
  scanner.copy_configuration(reconstructed.get_scanner());
  // scanner.set_max_pyramid_levels(5); //try setting to 2, 3...
  frontal_face_detector one_level_detector =
      dlib::object_detector<image_scanner_type>(
          scanner, reconstructed.get_overlap_tester(), reconstructed.get_w());

  detector_ = one_level_detector;
}

// Compute the relative ACC, based on the distance of (most similar image pair -
// 100%)
// and the distance of (most different image pair - 0%)
string MatchAgainstGallery::Calculate_ACC_Rate(float x) {
  // len < 0.1 -- 100
  // len > 0.6 -- 50 (range = 0.6-0.1 = 0.5)
  // if len=x, acc = ((0.5 -(x-0.1))/0.5)*50 +50

  float acc = 0.;
  float tmp = 0.5 - (x - 0.1);
  if (tmp <= 0.0)
    acc = 50.0;
  else if (tmp >= 0.5)
    acc = 100.0;
  else
    acc = ((0.5 - (x - 0.1)) / 0.5) * 50. + 50.;

  string str;
  if (acc == 100)
    str = " [" + std::to_string(x).substr(0, 4) + ": " + "100%]";
  else
    str = " [" + std::to_string(x).substr(0, 4) + ": " +
          std::to_string(acc).substr(0, 2) + "%]";
  return str;
}

std::vector<matrix<rgb_pixel>>
MatchAgainstGallery::Jitter_Image(const matrix<rgb_pixel> &img, int number) {
  // All this function does is make 100 copies of img, all slightly jittered by
  // being
  // zoomed, rotated, and translated a little bit differently.
  thread_local random_cropper cropper;
  cropper.set_chip_dims(150, 150);
  cropper.set_randomly_flip(true);
  cropper.set_max_object_size(0.99999);
  cropper.set_background_crops_fraction(0);
  cropper.set_min_object_size(1, 1); // dlib 19.6
  //  cropper.set_min_object_size(1, 1); // dlib 19.9
  cropper.set_translate_amount(0.02);
  cropper.set_max_rotation_degrees(3);

  std::vector<mmod_rect> raw_boxes(1), ignored_crop_boxes;
  raw_boxes[0] = shrink_rect(get_rect(img), 3);
  std::vector<matrix<rgb_pixel>> crops;

  matrix<rgb_pixel> temp;
  for (int i = 0; i < number; ++i) {
    cropper(img, raw_boxes, temp, ignored_crop_boxes);
    crops.push_back(move(temp));
  }
  return crops;
}

// Create some images using jiter from testing image, then ensemble the results
std::vector<matrix<float, 0, 1>>
MatchAgainstGallery::Jitter_Based_Testing(matrix<rgb_pixel> image) {
  int number = 2;
  std::vector<matrix<rgb_pixel>> jitterimages = Jitter_Image(image, number);
  // Wheather we need to do face landmark and align or not?

  std::vector<matrix<rgb_pixel>> faces;
  // dlib::rectangle face(0,0,150,150);
  for (size_t i = 0; i < jitterimages.size(); i++) {
    // If using facial landmark and align
    // auto shape = sp(jitterimages[i], face);
    // matrix<rgb_pixel> face_chip;
    // extract_image_chip(jitterimages[i],
    // get_face_chip_details(shape,150,0.25), face_chip);
    // faces.push_back(move(face_chip));

    // If not using that. It seems that the following is better, not 100% sure
    faces.push_back(jitterimages[i]);
  }
  // add the original image which was aligned already
  faces.push_back(image);

  std::vector<matrix<float, 0, 1>> face_descriptors =
      models->net(faces); // vector of column_vectors

  matrix<float, 0, 1> face_des_0 = face_descriptors[0];

  cout << face_des_0.nr() << ":" << face_des_0.nc() << endl;

  for (size_t i = 1; i < face_descriptors.size(); i++) {
    matrix<float, 0, 1> face_des_i = face_descriptors[i];
    for (long j = 0; j < face_des_0.nr(); j++)
      face_des_0(j, 0) += face_des_i(j, 0);
  }
  // Calculate the average
  for (long j = 0; j < face_des_0.nr(); j++)
    face_des_0(j, 0) =
        face_des_0(j, 0) / (number + 1); // include the original one

  face_descriptors[0] = face_des_0;

  return face_descriptors;
}

// ----------------------------------------------------------------------------------------

bool has_suffix(const string &s, const string &suffix) {
  return (s.size() >= suffix.size()) &&
         equal(suffix.rbegin(), suffix.rend(), s.rbegin());
}

std::string MatchAgainstGallery::getPose(std::string file_name) {
  for (std::vector<std::string>::iterator it = pose_list.begin();
       it != pose_list.end(); ++it) {
    if (file_name.find(*it) != std::string::npos) {
      return *it;
    }
  }
  return "";
}

// This will based on structure
// person name1 containing all images of that person no matter what names
void MatchAgainstGallery::Init_Gallery_Folder_Based() {
  bool debug = false;
  path dpath(folderpath);
  // int count =0;

  BOOST_FOREACH (const path &p, std::make_pair(directory_iterator(dpath),
                                               directory_iterator())) {
    if (is_directory(p)) {
      if (debug) {
        // std::cout << count++<<" : " << p.filename().string() << std::endl;
        std::cout << p.filename().string() << std::endl;
      }
      // string s="";
      User_Infor userinfor;
      User_Facial_Infor person_facial_infor;
      std::map<string, matrix<float, 0, 1>> gallery_face_descriptor;

      // Look up only txt file and load into s
      // BOOST_FOREACH(const path& sp, std::make_pair(directory_iterator(p),
      // directory_iterator())) {
      // if (is_directory(sp)) continue;
      // if(!is_regular_file(sp)) continue;

      // if(sp.extension().string() == ".txt"){
      // userinfor = Read_Info_GivenName(sp.string(),s);
      // userinfor.faceid = p.filename().string();
      // }

      userinfor.faceid = p.filename().string();
      person_facial_infor.Update_Saved_xxx_Face(p);

      // add the name of folder (person's name)
      unique_faceid_list.insert(
          std::pair<string, int>(p.filename().string(), 1));
      // }

      // In some case, the folder is created, but no image saved (due to the
      // long lag saving time)
      // We need to remove those folders, or skip them
      bool found_image = false;
      std::map<std::string, int> pose_count;
      BOOST_FOREACH (const path &sp, std::make_pair(directory_iterator(p),
                                                    directory_iterator())) {
        if (is_directory(sp))
          continue;
        if (!is_regular_file(sp)) {
          continue;
        } else if (sp.extension().string() == ".jpg") {
          matrix<float, 0, 1> face_descriptor;
          if (debug)
            std::cout << sp.string() << std::endl;
          matrix<rgb_pixel> img;
          load_image(img, sp.string());

          face_descriptor = models->net(img);
          gallery_face_descriptor.insert(std::pair<string, matrix<float, 0, 1>>(
              sp.filename().stem().string(), face_descriptor));
          std::string pose = getPose(sp.filename().stem().string());
          if (pose.length() > 0) {
            std::map<std::string, int>::iterator it = pose_count.find(pose);
            if (it != pose_count.end()) {
              it->second += 1;
            } else {
              pose_count.insert(std::pair<std::string, int>(pose, 1));
            }
          }

          // gallery_faces.push_back(img);

          found_image = true;
        }
      }

      if (found_image && gallery_face_descriptor.size() > 0) {
        output_inforlist.insert(
            std::pair<string, User_Infor>(p.filename().string(), userinfor));
        output_person_facial_inforlist.insert(
            std::pair<string, User_Facial_Infor>(p.filename().string(),
                                                 person_facial_infor));

        // care needs to be taken in consideration here, if say we have
        // straight.jpg and up30.jpg, but have no left30.jpg
        // We need to condider the order of those images
        // So we can use pair (imagename - facefeaturevector)

        gallery_face_descriptors.insert(
            std::pair<string, std::map<string, matrix<float, 0, 1>>>(
                p.filename().string(), gallery_face_descriptor));
      }

      if (pose_count.size() > 0) {
        faceid_pose_count.insert(
            std::pair<std::string, std::map<std::string, int>>(
                p.filename().string(), pose_count));
      }
    }
  }
}

// Load user_infor from ODB or from txt file
User_Infor MatchAgainstGallery::Load_User_Infor(
    string folder_faceid) // given folder/faceID
{
  User_Infor userinfor;
  string path_tofaceid =
      folderpath + folder_faceid + "/" + folder_faceid + ".txt";
  // path dpath(path_tofaceid);
  if (!boost::filesystem::exists(path_tofaceid))
    return userinfor;

  string s;
  if (file_size(path_tofaceid) > 1) {
    userinfor = Read_Info_GivenName(path_tofaceid, s);
    userinfor.faceid = folder_faceid;
  }

  return userinfor;
}

void MatchAgainstGallery::Load_User_Infor_From_DB(
    string folder_faceid, User_Infor &userinfor) // given folder/faceID
{
  int millisec;
  // struct tm* tm_info;
  struct timeval tv;

  gettimeofday(&tv, NULL);

  millisec = lrint(tv.tv_usec / 1000.0); // Round to nearest millisec
  if (millisec >= 1000) { // Allow for rounding up to nearest second
    millisec -= 1000;
    tv.tv_sec++;
  }

  time_t now = time(0);

  static char sec[5];
  strftime(sec, sizeof(sec), "%S", localtime(&now));

  // int sec_num = atoi(sec);
  ////    if (millisec%100 ==0){
  // if (sec_num % 2 ==0){
  ////to do
  ////Access DB here

  // cout<<"Loading: "<<folder_faceid<<endl;
  // HumanInfo out = DBInstance.getHumanInfo(folder_faceid);

  // userinfor.name = out.name;
  // userinfor.age = out.age;
  // userinfor.gender = out.gender;
  // }
}

// void MatchAgainstGallery::Write_User_Infor_To_DB(string
// folder_faceid){//given folder/faceID
// HumanInfo out = DBInstance.getHumanInfo(folder_faceid);
// }

std::string
MatchAgainstGallery::Matching_OneFace_Against_Gallery_FacePoseDriven(
    matrix<float, 0, 1> face_descriptor, string file_prefix) {
  // Given that the face was cropped and aligned properly
  bool success_match = false;
  float minlength = 100;
  // int minID1 = -1;
  float match_point1 = 0.0;
  // int minID2 = -1;
  float match_point2 = 0.0;
  string minfaceID1 = "";
  string minfaceID2 = "";
  string return_faceid = "";

  for (std::map<string, std::map<string, matrix<float, 0, 1>>>::iterator it =
           gallery_face_descriptors.begin();
       it != gallery_face_descriptors.end(); ++it) {
    std::map<string, matrix<float, 0, 1>> gallery_one_person = it->second;
    // Here we check if
    // the distance between two face descriptors is less than 0.6, which is the
    // decision threshold the network was trained to use.  Although you can
    // certainly use any other threshold you find useful.
    for (std::map<string, matrix<float, 0, 1>>::iterator it_one =
             gallery_one_person.begin();
         it_one != gallery_one_person.end();
         ++it_one) { // This loop through all face poses
      if (it_one->first.substr(0, 8) != file_prefix)
        continue; // We only want  to compare with straight
      float lng = length(it_one->second - face_descriptor);
      // this is for all image (fixed as [0] straight.jpg, [1] left30.jpg ...)
      // in gallery
      // in some case, the [j] image can be empty, because the auto-nature of
      // multi-face image sampling for gallery
      if (lng < threshold) { // 0.40 for vp9_facenet1
        if (lng <= minlength) {
          minlength = lng;
          match_point2 = match_point1;

          // minID2 = minID1;
          minfaceID2 = minfaceID1;

          match_point1 = 1 - minlength;
          // minID1 = i;
          minfaceID1 = it->first;
        }
        success_match = true;
      }
    }
  }
  // if (minlength<0.45)
  // cout<<minlength<< " : "<<minfaceID1<<endl;

  if (success_match) {
    // Or vector of string if we want return a list
    // string ratestr = Calculate_ACC_Rate(minlength);
    // string str = unique_faceid_list[minID1] + ratestr;// + ":" +
    // std::to_string(match_point1*100).substr(0,4)+"%";
    // current_person_name = unique_faceid_list[minID1];

    // THIS current_person_name IS NOW USE FOR BOTH AREA: TRAINING AREA AND RECO
    // AREA
    return_faceid = minfaceID1;

    // This will load from the on-RAM memory infolist
    // current_faceid_infor = output_inforlist[return_faceid];
    // This will load from the on-RAM memory person_facial_infolist
    // current_peron_facial_infor =
    // output_person_facial_inforlist[return_faceid];

    // This will load from ODB or text file on hard drive

    // We need to make an interal (some sec to run once) to access DB, to
    // improve speed
    // Hence, after access, if there is exist some data, do not access again to
    // save time
    // to do .....
    // if (current_faceid_infor.name.empty() ||
    // current_faceid_infor.gender.empty() ){
    // Load_User_Infor_From_DB(return_faceid, current_faceid_infor);

    // output_inforlist[return_faceid].name = current_faceid_infor.name;
    // output_inforlist[return_faceid].gender = current_faceid_infor.gender;
    // output_inforlist[return_faceid].age = current_faceid_infor.age;
    // }

    // cout<<current_faceid_infor.name<<endl;
  }
  return return_faceid;
}

int MatchAgainstGallery::convertDistanceToPercent(float _d) {
  return (_d >= HISTOGRAM_X0 && _d <= HISTOGRAM_X1)
             ? 100 * (HISTOGRAM_X1 - _d) / (HISTOGRAM_X1 - HISTOGRAM_X0)
             : 0;
}

// This function will check the gallery, find the best match, if not over the
// threshold then
// Ask to add new one in Gallery
RecentFaceInfo MatchAgainstGallery::Matching_OneFace_Against_Gallery(
    matrix<float, 0, 1> face_descriptor) {
  bool debug = false;
  if (debug) {
    std::cout << "MatchAgainstGallery::Matching_OneFace_Against_Gallery >> 0 "
                 ">> face_descriptor.size() = "
              << face_descriptor.size() << std::endl;
  }
  // Given that the face was cropped and aligned properly
  bool success_match = false;
  float minlength = 100;
  string min_face_id = "";
  string min_face_image_id = "";
  string min_face_pose = "";
  // string return_faceid = "";
  RecentFaceInfo result;
  result.face_id = "";
  result.score = 0;
  result.max_distance_other_face_id = 0;

  //  std::vector<int> histogram = std::vector<int>(HISTOGRAM_N, 0);

  if (debug) {
    std::cout << "MatchAgainstGallery::Matching_OneFace_Against_Gallery >> 1"
              << std::endl;
  }

  for (std::map<string, std::map<string, matrix<float, 0, 1>>>::iterator it =
           gallery_face_descriptors.begin();
       it != gallery_face_descriptors.end(); ++it) {
    std::string face_id = it->first;
    //    if (debug) {
    //      std::cout << "MatchAgainstGallery::Matching_OneFace_Against_Gallery
    //      >> 1.1 >> face_id = " << face_id << std::endl;
    //    }
    std::map<string, matrix<float, 0, 1>> gallery_one_person = it->second;
    // Here we check if
    // the distance between two face descriptors is less than 0.6, which is the
    // decision threshold the network was trained to use.  Although you can
    // certainly use any other threshold you find useful.
    for (std::map<string, matrix<float, 0, 1>>::iterator it_one =
             gallery_one_person.begin();
         it_one != gallery_one_person.end();
         ++it_one) { // This loop through all face poses
                     //        if (debug) {
                     //          std::cout <<
      //          "MatchAgainstGallery::Matching_OneFace_Against_Gallery >>
      //          1.1.1 >> " << it_one->first << std::endl;
      //        }
      // if (it_one->first.substr(0,8) != "straight" &&
      // it_one->first.substr(0,7) != "smiling") continue; //We only want  to
      // compare with straight
      //      if (it_one->first.substr(0, 8) != "straight")
      //        continue;  // We only want  to compare with straight

      //        if (debug) {
      //          std::cout <<
      //          "MatchAgainstGallery::Matching_OneFace_Against_Gallery >>
      //          1.1.2 >> face_descriptor.size = " << face_descriptor.size() <<
      //          std::endl;
      //        }
      float lng = length(it_one->second - face_descriptor); // @@@ crashes here
      if (debug) {
        std::cout
            << "MatchAgainstGallery::Matching_OneFace_Against_Gallery >> 1.1.3"
            << std::endl;
      }

      // this is for all image (fixed as [0] straight.jpg, [1] left30.jpg ...)
      // in gallery
      // in some case, the [j] image can be empty, because the auto-nature of
      // multi-face image sampling for gallery
      //      if (debug) {
      //          std::cout << lng << " : ";
      //      }

      if (lng < threshold) { // 0.40 for vp9_facenet1
        if (lng <= minlength) {
          minlength = lng;
          min_face_id = face_id;
          min_face_image_id = it_one->first;
        }
        success_match = true;
      }

      if (debug) {
        std::cout
            << "MatchAgainstGallery::Matching_OneFace_Against_Gallery >> 1.1.4"
            << std::endl;
      }
      //      if (lng > HISTOGRAM_X0 && lng <= HISTOGRAM_X1) {
      //        int HISTORAM_INDEX = floor((HISTOGRAM_X1 - lng) / HISTOGRAM_DX);
      //        histogram[HISTORAM_INDEX]++;
      //        if (debug) std::cout << HISTORAM_INDEX << " : ";
      //      }
    }
  }
  //  if (debug) {
  //      std::cout << std::endl;
  //  }
  // if (minlength<0.45)
  // cout<<minlength<< " : "<<minfaceID1<<endl;

  if (debug) {
    std::cout << "MatchAgainstGallery::Matching_OneFace_Against_Gallery >> 2"
              << std::endl;
  }

  if (success_match) {
    // Or vector of string if we want return a list
    // string ratestr = Calculate_ACC_Rate(minlength);
    // string str = unique_faceid_list[minID1] + ratestr;// + ":" +
    // std::to_string(match_point1*100).substr(0,4)+"%";
    // current_person_name = unique_faceid_list[minID1];

    // THIS current_person_name IS NOW USE FOR BOTH AREA: TRAINING AREA AND RECO
    // AREA
    // return_faceid = minfaceID1;
    result.face_id = min_face_id;
    result.face_image_id = min_face_image_id;
    result.score = convertDistanceToPercent(minlength);
    // calculate graph
    float min_distance_same_pose = 100;
    float average_distance_same_pose = 0;
    int same_pose_count = 0;
    float max_distance_same_pose = -100;
    float min_distance_other_pose = 100;
    float average_distance_other_pose = 0;
    int other_pose_count = 0;
    float max_distance_other_pose = -100;
    float min_distance_other_face_id = 100;
    float max_distance_other_face_id = -100;
    min_face_pose = getPose(min_face_image_id);
    //    std::cout << "min_face_id = " << min_face_id << std::endl;
    //    std::cout << "min_face_pose = " << min_face_pose << std::endl;
    for (std::map<string, std::map<string, matrix<float, 0, 1>>>::iterator it =
             gallery_face_descriptors.begin();
         it != gallery_face_descriptors.end(); ++it) {
      std::string face_id = it->first;
      //      std::cout << "face_id = " << face_id << std::endl;
      std::map<string, matrix<float, 0, 1>> gallery_one_person = it->second;
      for (std::map<string, matrix<float, 0, 1>>::iterator it_one =
               gallery_one_person.begin();
           it_one != gallery_one_person.end(); ++it_one) {
        std::string pose = getPose(it_one->first);
        //        std::cout << "pose = " << pose << std::endl;
        float distance = length(it_one->second - face_descriptor);
        //        std::cout << "distance = " << distance << std::endl;
        if (face_id == min_face_id) {
          if (pose == min_face_pose) {
            same_pose_count++;
            if (min_distance_same_pose > distance) {
              min_distance_same_pose = distance;
            }
            average_distance_same_pose += distance;
            //                std::cout << "average_distance_same_pose = " <<
            //                average_distance_same_pose << std::endl;
            if (max_distance_same_pose < distance) {
              max_distance_same_pose = distance;
            }
          } else {
            other_pose_count++;
            if (min_distance_other_pose > distance) {
              min_distance_other_pose = distance;
              //                    std::cout << "min_distance_other_pose = " <<
              //                    min_distance_other_pose << std::endl;
            }
            average_distance_other_pose += distance;
            //                std::cout << "average_distance_other_pose = " <<
            //                average_distance_other_pose << std::endl;
            //                std::cout << "other_post_count = " <<
            //                other_pose_count << std::endl;
            if (max_distance_other_pose < distance) {
              max_distance_other_pose = distance;
            }
          }
        } else {
          //            std::cout << "min_distance_other_face_id = " <<
          //            min_distance_other_face_id << std::endl;
          if (min_distance_other_face_id > distance) {
            min_distance_other_face_id = distance;
            //                std::cout << "min_distance_other_face_id (2) = "
            //                << min_distance_other_face_id << std::endl;
          }
          if (max_distance_other_face_id < distance) {
            max_distance_other_face_id = distance;
          }
        }
      }
    }
    if (max_distance_ever < max_distance_other_face_id) {
      max_distance_ever = max_distance_other_face_id;
    }

    if (debug) {
      std::cout << "MatchAgainstGallery::Matching_OneFace_Against_Gallery >> "
                   "Graph >> 1"
                << std::endl;
    }
    std::vector<int> graph;
    graph.clear();
    graph.push_back(convertDistanceToPercent(min_distance_same_pose));
    graph.push_back(convertDistanceToPercent(min_distance_other_pose));
    graph.push_back(convertDistanceToPercent(min_distance_other_face_id));
    if (same_pose_count > 0) {
      graph.push_back(convertDistanceToPercent(average_distance_same_pose /
                                               same_pose_count));
    } else {
      graph.push_back(0);
    }
    if (other_pose_count > 0) {
      graph.push_back(convertDistanceToPercent(average_distance_other_pose /
                                               other_pose_count));
    } else {
      graph.push_back(0);
    }
    graph.push_back(convertDistanceToPercent(max_distance_same_pose));
    graph.push_back(convertDistanceToPercent(max_distance_other_pose));
    graph.push_back(convertDistanceToPercent(max_distance_other_face_id));
    result.graph = graph;
    if (debug) {
      std::cout << "MatchAgainstGallery::Matching_OneFace_Against_Gallery >> "
                << min_face_id << " " << graph[0] << " " << graph[1] << " "
                << graph[2] << " " << graph[3] << " " << graph[4] << " "
                << graph[5] << " " << graph[6] << " " << graph[7] << endl;
    }

    //    result.histogram = histogram;

    if (debug) {
      std::cout << "MatchAgainstGallery::Matching_OneFace_Against_Gallery >> "
                   "Graph >> 2"
                << std::endl;
    }
    result.max_distance_other_face_id =
        convertDistanceToPercent(max_distance_other_face_id);

    // This will load from the on-RAM memory infolist
    // current_faceid_infor = output_inforlist[current_person_name];
    // This will load from the on-RAM memory person_facial_infolist
    // current_peron_facial_infor =
    // output_person_facial_inforlist[current_person_name];

    // This will load from ODB or text file on hard drive

    // We need to make an interal (some sec to run once) to access DB, to
    // improve speed
    // Hence, after access, if there is exist some data, do not access again to
    // save time
    // to do .....
    // if (current_faceid_infor.name.empty() ||
    // current_faceid_infor.gender.empty() ){
    // Load_User_Infor_From_DB(current_person_name, current_faceid_infor);

    // output_inforlist[current_person_name].name = current_faceid_infor.name;
    // output_inforlist[current_person_name].gender =
    // current_faceid_infor.gender;
    // output_inforlist[current_person_name].age = current_faceid_infor.age;
    // }

    // cout<<current_faceid_infor.name<<endl;
  }
  if (debug) {
    std::cout << "MatchAgainstGallery::Matching_OneFace_Against_Gallery >> done"
              << std::endl;
  }
  return result;
}

///
/// \brief MatchAgainstGallery::Add_More_FaceImages_That_Have_Certain_Distance
/// Only add images that are not close (very similar) to ones that have been in
/// gallery already
/// \param face_descriptor
/// \param face_id
/// \return
///
bool MatchAgainstGallery::Add_More_FaceImages_That_Have_Certain_Distance(
    matrix<float, 0, 1> face_descriptor, string face_id) {
  float upper_threshold = 0.40;
  float lower_threshold = 0.22;
  bool satisfied = true;

  std::map<string, matrix<float, 0, 1>> gallery_one_person =
      gallery_face_descriptors[face_id];
  // Here we check if
  // the distance between two face descriptors is less than 0.40 and larger than
  // 0.12, which is the
  // decision threshold to add more images to a face_id.
  for (std::map<string, matrix<float, 0, 1>>::iterator it_one =
           gallery_one_person.begin();
       it_one != gallery_one_person.end();
       ++it_one) { // This loop through all face poses
    // if (it_one->first.substr(0,8) != "straight" && it_one->first.substr(0,7)
    // != "smiling") continue; //We only want  to compare with straight
    float lng = length(it_one->second - face_descriptor);
    // this is for all image (fixed as [0] straight.jpg, [1] left30.jpg ...) in
    // gallery
    // in some case, the [j] image can be empty, because the auto-nature of
    // multi-face image sampling for gallery
    if (!(lng < upper_threshold && lng > lower_threshold)) {
      satisfied = false;
      break;
    }
  }
  return satisfied;
}

///
/// \brief MatchAgainstGallery::SingleFace_Recognition --> given the
/// detected_face_list, return the all cur_sth
/// \param frame
/// \param img
/// \param d_roi
/// \param detected_face_list
///
void MatchAgainstGallery::SingleFace_LookUp_Gallery(
    Mat &frame, dlib::rectangle d_roi,
    Detected_Faces_List &detected_face_list) {
  bool debug = false;
  // cv::Rect rec(d_roi.left(),d_roi.top(),d_roi.right()-d_roi.left(),
  // d_roi.bottom()-d_roi.top());

  // cv_image<bgr_pixel> img(frame);
  // models->Face_and_Shape_Detection(img, d_roi, true, detected_face_list);
  ////Remove_BackgroundNoise(frame, detected_face_list.face_list,
  /// detected_face_list.shape_list, detected_face_list.det_list);

  // if (detected_face_list.face_list.size()==0) return;

  // int max_length = 0;
  // int biggest_faceindex = -1;
  // int ind = 0;

  // for (auto det : detected_face_list.det_list)
  // {
  // if (det.width > max_length){
  // max_length = det.width;
  // biggest_faceindex = ind;
  // }
  // ind++;
  // detected_face_list.face_id_list.push_back("");
  // }

  // matrix<float,0,1> face_descriptor;
  // face_descriptor =
  // models->net(detected_face_list.face_list[biggest_faceindex]);

  models->Face_Recognition(frame, d_roi, detected_face_list, false);
  if (detected_face_list.face_list.size() == 0)
    return;

  // string faceid =
  // Matching_OneFace_Against_Gallery(detected_face_list.face_discriptor_list[0]);//because
  // there is only one
  if (debug) {
    std::cout << "MatchAgainstGallery::SingleFace_LookUp_Gallery" << std::endl;
  }
  RecentFaceInfo face_info = Matching_OneFace_Against_Gallery(
      detected_face_list.face_discriptor_list[0]); // because there is only one

  // The current_sth can be non-empty even though detected_face_list is empty
  // So we still need the current_sth to store the lastest one detected

  // current_face_descriptor = detected_face_list.face_discriptor_list[0];
  // currentface =
  // detected_face_list.face_list[detected_face_list.biggest_face_index];
  // current_landmark_shape =
  // detected_face_list.shape_list[detected_face_list.biggest_face_index];
  // current_faceid = faceid;
  // current_det =
  // detected_face_list.det_list[detected_face_list.biggest_face_index];

  // All face_ids are empty except the one with index biggest_faceindex
  // detected_face_list.face_id_list[detected_face_list.biggest_face_index] =
  // faceid;
  detected_face_list.face_info_list[detected_face_list.biggest_face_index] =
      face_info;

  // cv::Mat cv_image =
  // toMat(detected_face_list.face_list[detected_face_list.biggest_face_index]);
  // double brightness = 0.0;
  // Get_Average_Brightness(cv_image,brightness);
  // cout<<"brightness: "<<brightness<<endl;
  // cout<<current_faceid<<endl;
}

///
/// \brief MatchAgainstGallery::MultiFace_Recognition --> given the
/// detected_face_list, return the all corresponding face_ids, stored in
/// detected_face_list
/// \param frame --> the frame to show to user
/// \param img
/// \param d_roi
/// \param detected_face_list
///
void MatchAgainstGallery::MultiFace_LookUp_Gallery(
    Mat &frame, dlib::rectangle d_roi, Detected_Faces_List &detected_face_list,
    DtrackerManager &dtrackers, int count_frame) {
  bool debug = false;

  //  cout << "Images Size : " << this->savedImage.size() << endl;
  // cv_image<bgr_pixel> img(frame);

  // models->Face_and_Shape_Detection(img, d_roi, false,
  // detected_face_list);
  ////    cv::rectangle(frame, rec,cv::Scalar(0,255,255),2);
  ////Remove_BackgroundNoise(frame, detected_face_list.face_list,
  /// detected_face_list.shape_list, detected_face_list.det_list);

  // if (detected_face_list.face_list.size()==0) return;

  // cv::Mat cv_image = toMat(detected_face_list.face_list[0]);
  // double brightness = 0.0;
  // Get_Average_Brightness(cv_image,brightness);
  // cout<<"brightness: "<<brightness<<endl;

  // std::vector<matrix<float,0,1>> face_descriptors;
  // face_descriptors = models->net(detected_face_list.face_list);

  if (debug) {
    std::cout
        << "MatchAgainstGallery::MultiFace_LookUp_Gallery >> count_frame = "
        << count_frame << std::endl;
  }
  models->Face_Recognition_With_Track(frame, d_roi, detected_face_list, true,
                                      dtrackers, count_frame);

  if (debug) {
    std::cout << "MatchAgainstGallery::MultiFace_LookUp_Gallery >> 1 >> "
              << detected_face_list.face_list.size() << " - "
              << detected_face_list.face_info_list.size() << std::endl;
  }
  // no match number then do nothing
  //    if (dtrackers.getTrackerList().size() !=
  //    detected_face_list.det_list.size()) return;
  std::vector<std::string> faceid_list;

  // if doing network recogntion -> face_discriptor_list may not empty
  for (size_t i = 0; i < detected_face_list.face_discriptor_list.size(); ++i) {
    if (debug) {
      std::cout
          << "MatchAgainstGallery::MultiFace_LookUp_Gallery >> 1.1 >> i = " << i
          << std::endl;
    }
    if (detected_face_list.face_discriptor_list[i].size() != 128) {
      continue;
    }
    // res is the returned face_id
    // string faceid =
    // Matching_OneFace_Against_Gallery(detected_face_list.face_discriptor_list[i]);
    RecentFaceInfo face_info = Matching_OneFace_Against_Gallery(
        detected_face_list.face_discriptor_list[i]);
    detected_face_list.face_info_list.push_back(face_info);

    faceid_list.push_back(face_info.face_id);
  }

  if (debug) {
    std::cout << "MatchAgainstGallery::MultiFace_LookUp_Gallery >> 2"
              << std::endl;
  }

  if (!models->use_track_on_detection_and_recognition)
    return;

  // update faceid with corresponding track
  if (detected_face_list.face_discriptor_list.size() > 0) {
    dtrackers.addfaceidTracker(faceid_list);

    // Get the available on-track faceid to show out on screen
    for (size_t i = 0; i < dtrackers.getTrackerList().size(); ++i) {
      if (dtrackers.getTrackerList()[i].allFaceIDs.size() == 1)
        detected_face_list.face_info_list[i].face_id =
            dtrackers.getTrackerList()[i].allFaceIDs[0];
      //      cout << "no face found frame:"
      //           << dtrackers.getTrackerList()[i].no_face_id_found << endl;
      if (dtrackers.getTrackerList()[i].no_face_id_found >= 15 &&
          dtrackers.getTrackerList()[i].no_face_id_found <= 60) {
        // Create new Id
        time_t now = time(0);
        if (dtrackers.getTrackerList()[i].tmp_name.empty()) {
          static char autoFaceID[30];
          strftime(autoFaceID, sizeof(autoFaceID), "Customer-%d%H%M%S",
                   localtime(&now));
          dtrackers.getTrackerList()[i].tmp_name = autoFaceID;
        }

        string filename =
            "straight" +
            std::to_string(dtrackers.getTrackerList()[i].no_face_id_found);

        string str =
            dtrackers.getTrackerList()[i].tmp_name + "-" + filename + ".jpg";

        cv::Mat img = this->frame(dtrackers.getTrackerList()[i].cvRect);
        cv::resize(img, img, cv::Size(150, 150));
        /* Save image */
        cout << "Save image : " << str << endl;
        if (detected_face_list.face_discriptor_list[i].nr() == 128) {
          //          cout << detected_face_list.face_discriptor_list[i].nc() <<
          //          endl;
          sendDataToImgServer(img, str, dtrackers.getTrackerList()[i].tmp_name,
                              detected_face_list.face_discriptor_list[i]);
          ImageInfo info;
          info.url = "http://api.hrl.vp9.vn/batch/" + str;
          info.image = img.clone();
          info.vec128 = detected_face_list.face_discriptor_list[i];
          savedImage.push_back(info);
        }
      }
      if (detected_face_list.face_info_list[i].face_id.empty())
        dtrackers.getTrackerList()[i].no_face_id_found++;
      else
        dtrackers.getTrackerList()[i].no_face_id_found = 0;
      //            else detected_face_list.face_info_list[i].face_id = "";

      //            if (dtrackers.getTrackerList()[i].allFaceIDs.size()==1)
      //                std::cout<<"FaceID:
      //                "<<dtrackers.getTrackerList()[i].allFaceIDs[0]<<std::endl;
    }
  }
  if (debug) {
    std::cout << "MatchAgainstGallery::MultiFace_LookUp_Gallery >> 3"
              << std::endl;
  }
}

#if 0
void MatchAgainstGallery::ProcessVideo_MultiFace_ThangMay_VP9()
{
    int key, count_frame = 0;

    Init_Gallery_Folder_Based();

    // string filename= "rtsp://10.12.11.152:554/stream1";
    string filename = "rtsp://10.12.11.222:554/av0_0";
    // string filename= "camgocrong41.mp4";
    // string filename= "http://10.11.11.234/live/nal/0/005a20528d7bxyz14345";
    // string filename= "facevideo.mp4"; //Le tan

    cv::VideoCapture cap;
    if (filename.empty())
        cap.open(0);
    else
        cap.open(filename);

    cv::Mat frame;
    CTracker tracker(0.15, 0.5, 100.0, 17, 17);  // for Kalman filter analysis
    namedWindow("My Window", 1);
    // set the callback function for any mouse event
    // Point3_<int> mouse_pos;
    // setMouseCallback("My Window", CallBackFunc, &mouse_pos);
    // cout<<"Please enter 1 to save face!"<<endl;

    while (key != 27) {
        cap >> frame;
        count_frame++;

        if (filename[0] == 'r') {
            if (count_frame < 2)
                continue;
            else count_frame = 0;
        }

        // cv::rotate(frame, frame, ROTATE_90_CLOCKWISE);
        // if (frame.empty()) continue;
        // cv::resize(frame, frame, cv::Size(1280,720));

        // This is used for Reco area
        Detected_Faces_List detected_face_list;

        // Change to dlib's image format. No memory is copied.
        // cv_image<bgr_pixel> img_small(frame);

        // dlib::rectangle d_roi(frame.cols/6,frame.rows/10,frame.cols/6+500,frame.rows/10+400);
        dlib::rectangle d_roi(frame.cols/8, frame.rows/6, frame.cols-frame.cols/8, frame.rows/2.2);

        MultiFace_LookUp_Gallery(frame, d_roi, detected_face_list);
        for (int i = 0; i < detected_face_list.face_list.size(); i++) {
            time_t now = time(0);
            static char auto_faceid[10];
            strftime(auto_faceid, sizeof(auto_faceid), "%M%S", localtime(&now));
            string str0 = auto_faceid;
            // string str = "/home/tucnv/Qt/FaceVP9/ThangMayVP9/"+str0+".jpg";
            // dlib::save_jpeg(detected_face_list.face_list[i],str);

            cout<<detected_face_list.det_list[0].width <<":"
                                                      <<detected_face_list.det_list[0].height<<endl;
        }
        // Add_Face_to_Recent_Face_List(frame,detected_face_list);

        cv::imshow("My Window", frame);
        if (filename.substr(0, 4) != "rtsp" && filename.substr(0, 4) != "http")
            key = cv::waitKey(30);
        else key = cv::waitKey(1);
    }
}

#endif

int MatchAgainstGallery::getPoseCount(std::string face_id, std::string pose) {
  std::map<std::string, std::map<std::string, int>>::iterator it =
      faceid_pose_count.find(face_id);
  if (it != faceid_pose_count.end()) {
    std::map<std::string, int>::iterator it_1 = it->second.find(pose);
    if (it_1 != it->second.end()) {
      return it_1->second;
    }
  }
  return 0;
}

/*Phuclb*/
void MatchAgainstGallery::readFromBinary(string file_path) {
  bool debug = false;

  std::ifstream f(file_path.c_str(),
                  std::ios::in | std::ios::binary | std::ios::ate);
  char *buffer;
  if (!f.is_open()) {
    throw FILE_NOT_FOUND_ERROR_CODE;
    if (debug) {
      std::cout << "@Error: MatchGallery::readFromFile: Unable to open file"
                << std::endl;
    }
  }
  // get size of file
  int size = (int)f.tellg();
  // jump to begin of file
  f.seekg(0, std::ios::beg);
  // allocate buffer
  buffer = new char[size];
  // read file
  f.read(buffer, size);
  // close file
  f.close();
  // set offset begin of buffer
  int offset = 0;

  if (debug) {
    std::cout << "MatchGallery::readFromBinaryFile: 1" << std::endl;
  }

  // convert 4 bytes to N
  int N = (unsigned char)buffer[offset] * 16777216 +
          (unsigned char)buffer[offset + 1] * 65536 +
          (unsigned char)buffer[offset + 2] * 256 +
          (unsigned char)buffer[offset + 3];
  offset += 4;

  if (debug) {
    std::cout << "N = " << N << std::endl;
  }

  // convert 4 bytes to K
  int K = (unsigned char)buffer[offset] * 16777216 +
          (unsigned char)buffer[offset + 1] * 65536 +
          (unsigned char)buffer[offset + 2] * 256 +
          (unsigned char)buffer[offset + 3];
  offset += 4;

  if (debug) {
    std::cout << "K = " << K << std::endl;
  }

  if (debug) {
    std::cout << "MatchGallery::readFromBinaryFile: 2" << std::endl;
  }

  // read feature vectors
  // C.clear();
  std::string current_face_id = "";
  for (int i = 0; i < N; ++i) {
    int l = (unsigned char)buffer[offset] * 16777216 +
            (unsigned char)buffer[offset + 1] * 65536 +
            (unsigned char)buffer[offset + 2] * 256 +
            (unsigned char)buffer[offset + 3];
    offset += 4;
    std::string _face_id = "";
    for (int z = 0; z < l; ++z) {
      _face_id += buffer[offset];
      offset++;
    }

    l = (unsigned char)buffer[offset] * 16777216 +
        (unsigned char)buffer[offset + 1] * 65536 +
        (unsigned char)buffer[offset + 2] * 256 +
        (unsigned char)buffer[offset + 3];
    offset += 4;
    std::string _pose_id = "";
    for (int z = 0; z < l; ++z) {
      _pose_id += buffer[offset];
      offset++;
    }

    l = (unsigned char)buffer[offset] * 16777216 +
        (unsigned char)buffer[offset + 1] * 65536 +
        (unsigned char)buffer[offset + 2] * 256 +
        (unsigned char)buffer[offset + 3];
    offset += 4;
    std::string _full_path = "";
    for (int z = 0; z < l; ++z) {
      _full_path += buffer[offset];
      offset++;
    }

    dlib::matrix<float, 0, 1> feature_vector(K);
    for (int j = 0; j < K; ++j) {
      Float2Char x;
      for (int z = 0; z < 4; ++z) {
        x.charArray[z] = (unsigned char)buffer[offset];
        offset++;
      }
      feature_vector(j) = x.floatValue;
    }

    if (debug) {
      std::cout << _face_id << " - " << _pose_id << " - " << _full_path
                << std::endl;
    }

    /////////////

    if (_face_id.compare(current_face_id)) {
      User_Infor userinfor;
      User_Facial_Infor person_facial_infor;

      userinfor.faceid = _face_id;
      path facepath("Gallery_demo/" + _face_id);
      person_facial_infor.Update_Saved_xxx_Face(facepath);

      unique_faceid_list.insert(std::pair<string, int>(_face_id, 1));
      output_inforlist.insert(
          std::pair<string, User_Infor>(_face_id, userinfor));
      output_person_facial_inforlist.insert(
          std::pair<string, User_Facial_Infor>(_face_id, person_facial_infor));
    }

    current_face_id = _face_id;

    //    DLib128 item;
    //    item.face_id = _face_id;
    //    item.pose_id = _pose_id;
    //    item.full_path = _full_path;
    //    item.features = feature_vector;
    //    C.push_back(item);
    if (faceid_pose_count.find(_face_id) == faceid_pose_count.end()) {
      std::map<std::string, int> pose_count;
      pose_count.insert(std::pair<std::string, int>(_pose_id, 1));
      faceid_pose_count.insert(
          std::pair<std::string, std::map<std::string, int>>(_face_id,
                                                             pose_count));
    } else {
      faceid_pose_count[_face_id].insert(
          std::pair<std::string, int>(_pose_id, 1));
    }
    std::size_t found = _full_path.find_last_of("/\\");
    string tmpFileName =
        _full_path.substr(found + 1, _full_path.length() - found);
    if (gallery_face_descriptors.find(_face_id) ==
        gallery_face_descriptors.end()) {
      std::map<string, matrix<float, 0, 1>> gallery_face_descriptor;

      gallery_face_descriptor.insert(
          std::pair<string, matrix<float, 0, 1>>(tmpFileName, feature_vector));
      gallery_face_descriptors.insert(
          std::pair<string, std::map<string, matrix<float, 0, 1>>>(
              _face_id, gallery_face_descriptor));
    } else {
      gallery_face_descriptors[_face_id].insert(
          std::pair<string, matrix<float, 0, 1>>(tmpFileName, feature_vector));
    }
  }

  // free buffer
  delete[] buffer;
  buffer = NULL;
  if (debug) {
    std::cout << "@MatchGallery::readFromBinaryFile: OK!" << std::endl;
  }
}

void MatchAgainstGallery::updateProfile(std::string img_url,
                                        std::string face_id,
                                        dlib::matrix<float, 0, 1> vector) {
  bool debug = true;
  if (debug)
    cout << "update to database" << endl;
  std::map<string, matrix<float, 0, 1>> gallery_face_descriptor;
  std::string fileName = img_url;
  if (gallery_face_descriptors.find(face_id) ==
      gallery_face_descriptors.end()) {
    User_Infor userinfor;
    userinfor.faceid = face_id;
    this->output_inforlist.insert(
        std::pair<string, User_Infor>(face_id, userinfor));
    gallery_face_descriptor.insert(
        std::pair<string, matrix<float, 0, 1>>(fileName, vector));
    gallery_face_descriptors.insert(
        std::pair<string, std::map<string, matrix<float, 0, 1>>>(
            face_id, gallery_face_descriptor));
  } else {
    gallery_face_descriptors[face_id].insert(
        std::pair<string, matrix<float, 0, 1>>(fileName, vector));
  }
  std::string newFolder = "Gallery_demo/" + face_id;
  std::string newFile = newFolder + "/" + fileName;
  boost::filesystem::create_directory(newFolder);
  string imgUrl = "http://api.hrl.vp9.vn/batch/" + fileName;
  cv::Mat img = curlImg(imgUrl.c_str());
  cv::imwrite(newFile, img);
}
