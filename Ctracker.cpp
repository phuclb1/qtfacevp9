#include "Ctracker.hpp"
#include "config.h"
using namespace cv;
using namespace std;

size_t CTrack::NextTrackID = 0;
// ---------------------------------------------------------------------------
// Track constructor.
// The track begins from initial point (pt)
// ---------------------------------------------------------------------------
CTrack::CTrack(Point2f pt, float dt, float Accel_noise_mag) {
  track_id = NextTrackID;

  NextTrackID++;
  box_drawn = Rect(0, 0, 0, 0);
  // Every track have its own Kalman filter,
  // it user for next point position prediction.
  KF = new TKalmanFilter(pt, dt, Accel_noise_mag);
  // Here stored points coordinates, used for next position prediction.
  prediction = pt;
  skipped_frames = 0;
  del_marked = false;
  trackname = "";
  trackname.clear();
  behumantrack_confidence = 1.0;
  number_conseccutiveframes_nofacefound = 0;
}
// ---------------------------------------------------------------------------
//
// ---------------------------------------------------------------------------
CTrack::~CTrack() {
  // Free resources.
  delete KF;
}

bool CTrack::Same_faceIDs_onsametrack() {
  if (number_conseccutiveframes_nofacefound == 0) { // found face with ID
    // if (trace.size()>5){ //Let the track to be established first
    if (all_faceIDs_onsametrack.size() == 2)
      return false; // if track contains to faceids
    else if (all_faceIDs_onsametrack.size() == 0) {
      all_faceIDs_onsametrack.push_back(trackname);
      return true;
    } else if (all_faceIDs_onsametrack.size() == 1) {
      if (trackname == all_faceIDs_onsametrack[0])
        return true;
      else {
        all_faceIDs_onsametrack.push_back(trackname); // its size is now = 2
        return false;
      }
    }
    // }
  }
}

void CTrack::Update_all_faceIDs_onsametrack(string &prompt) {
  if (number_conseccutiveframes_nofacefound == 0) { // found face with ID
    // if (trace.size()>5){ //Let the track to be established first
    if (all_faceIDs_onsametrack.size() >= 2)
      prompt = "";
    else if (all_faceIDs_onsametrack.size() == 0)
      all_faceIDs_onsametrack.push_back(trackname);
    else if (all_faceIDs_onsametrack.size() == 1) {
      if (trackname == all_faceIDs_onsametrack[0])
        prompt = "Ready to take more samples, turn your face on left, on "
                 "right, up and down slowly! please";
      else {
        all_faceIDs_onsametrack.push_back(trackname);
        prompt = "";
      }
    }
    // }
  }
}

void CTrack::Reset() {
  number_conseccutiveframes_nofacefound = 0;
  trace.clear();
  trackname = "";
  all_faceIDs_onsametrack.clear();
  all_faceimages_onsametrack.clear();
}

// ---------------------------------------------------------------------------
// Tracker. Manage tracks. Create, remove, update.
// ---------------------------------------------------------------------------
CTracker::CTracker(float _dt, float _Accel_noise_mag, double _dist_thres,
                   int _maximum_allowed_skipped_frames, int _max_trace_length) {
  dt = _dt;
  Accel_noise_mag = _Accel_noise_mag;
  dist_thres = _dist_thres;
  maximum_allowed_skipped_frames = _maximum_allowed_skipped_frames;
  max_trace_length = _max_trace_length;

  frameID = 0;
  max_rect_trace_length = 5;
}
// ---------------------------------------------------------------------------
//
// ---------------------------------------------------------------------------
void CTracker::Update(vector<Point2d> &detections,
                      bool allow_add_erase_tracks) {
  // -----------------------------------
  // If there is no tracks yet, then every point begins its own track.
  // -----------------------------------
  if (tracks.size() == 0) {
    // If no tracks yet
    for (int i = 0; i < (int)detections.size(); i++) {
      CTrack *tr = new CTrack(detections[i], dt, Accel_noise_mag);
      tracks.push_back(tr);
    }
  }

  int N = tracks.size();
  int M = detections.size();

  vector<vector<double>> Cost(N, vector<double>(M));
  //    vector<int> assignment;
  assignment.clear();

  double dist;
  for (int i = 0; i < (int)tracks.size(); i++) {
    // Point2d prediction=tracks[i]->prediction;
    // cout << prediction << endl;
    for (int j = 0; j < (int)detections.size(); j++) {
      Point2d diff = (tracks[i]->prediction - detections[j]);
      dist = sqrtf(diff.x * diff.x + diff.y * diff.y);
      Cost[i][j] = dist;
    }
  }
  // -----------------------------------
  // Solving assignment problem (tracks and predictions of Kalman filter)
  // -----------------------------------
  AssignmentProblemSolver APS;
  APS.Solve(Cost, assignment, AssignmentProblemSolver::optimal);

  // -----------------------------------
  // clean assignment from pairs with large distance
  // -----------------------------------
  // Not assigned tracks
  vector<int> not_assigned_tracks;

  for (int i = 0; i < (int)assignment.size(); i++) {
    if (assignment[i] != -1) {
      if (Cost[i][assignment[i]] > dist_thres) {
        assignment[i] = -1;
        // Mark unassigned tracks, and increment skipped frames counter,
        // when skipped frames counter will be larger than threshold, track will
        // be deleted.
        not_assigned_tracks.push_back(i);
      }
    } else {
      // If track have no assigned detect, then increment skipped frames
      // counter.
      tracks[i]->skipped_frames++;
    }
  }

  if (allow_add_erase_tracks) { // If during the person_tracking process, there
                                // is no need to do add, remove track
    // -----------------------------------
    // If track didn't get detects long time, remove it.
    // -----------------------------------
    for (int i = 0; i < (int)tracks.size(); i++) {
      if ((int)tracks[i]->skipped_frames > maximum_allowed_skipped_frames) {
        delete tracks[i];
        tracks.erase(tracks.begin() + i);
        assignment.erase(assignment.begin() + i);
        i--;
      }
    }
    // -----------------------------------
    // Search for unassigned detects
    // -----------------------------------
    vector<int> not_assigned_detections;
    vector<int>::iterator it;
    for (int i = 0; i < (int)detections.size(); i++) {
      it = find(assignment.begin(), assignment.end(), i);
      if (it == assignment.end()) {
        not_assigned_detections.push_back(i);
      }
    }

    // -----------------------------------
    // and start new tracks for them
    // Start new track too early, need wait a second to know if that is person
    // or not
    // -----------------------------------
    if (not_assigned_detections.size() != 0) {
      for (int i = 0; i < (int)not_assigned_detections.size(); i++) {
        CTrack *tr = new CTrack(detections[not_assigned_detections[i]], dt,
                                Accel_noise_mag);
        tracks.push_back(tr);
      }
    }

    // Do change assignement, hence the track ID is changed overtime
    // assignment_global.reserve(assignment.size());
    // for(int i=0;i<(int) assignment.size();i++)
    // assignment_global.push_back(assignment[i]);

  } // else{
  // Do not change assignement, because the track is kept unchanged
  //	assignment_global = assignment;

  // }

  // Update Kalman Filters state
  // assignment_global.clear();
  // assignment_global = assignment;

  // assignment_global.reserve(assignment.size());
  // for(int i=0;i<assignment.size();i++)
  // assignment_global.push_back(assignment[i]);
  // std::copy(assignment.begin(),assignment.end(),assignment_global.begin());

  // std::copy(assignment.begin(),assignment.end(),std::back_inserter(assignment_global));
  // //may not keep the same order of elements
  // assignment_global.assign(assignment.begin(),assignment.end());
  // assignment_global.resize(assignment.size());
  // assignment_global.assign(assignment.begin(),assignment.end());
  // memcpy(&assignment_global.at(0), &assignment.at(0), assignment.size());

  for (int i = 0; i < (int)assignment.size(); i++) {
    // If track updated less than one time, than filter state is not correct.

    tracks[i]->KF->GetPrediction();

    if (assignment[i] !=
        -1) // If we have assigned detect, then update using its coordinates,
    {
      tracks[i]->skipped_frames = 0;
      tracks[i]->prediction =
          tracks[i]->KF->Update(detections[assignment[i]], 1);
    } else // if not continue using predictions
    {
      tracks[i]->prediction = tracks[i]->KF->Update(Point2f(0, 0), 0);
    }

    //---ME here try
    // Point2d diff=(tracks[i]->prediction-detections[i]);
    // double dist=sqrtf(diff.x*diff.x+diff.y*diff.y);
    // if (dist>15.0) tracks[i]->del_marked = true;

    // std::cout<< (tracks[i]->trace.size()) << std::endl;

    //        if((int) tracks[i]->trace.size()>max_trace_length)
    //        {
    //            tracks[i]->trace.erase(tracks[i]->trace.begin(),tracks[i]->trace.end()-max_trace_length);
    //        }

    // We comment out above, because we donot want to cut short the trace,
    // let it show how many frame a face is found with ID

    tracks[i]->trace.push_back(tracks[i]->prediction);
    tracks[i]->KF->LastResult = tracks[i]->prediction;
  }
}
// ---------------------------------------------------------------------------
//
// ---------------------------------------------------------------------------
CTracker::~CTracker(void) {
  for (int i = 0; i < (int)tracks.size(); i++) {
    delete tracks[i];
  }
  tracks.clear();
}
