#include "Player.h"
#include "config.h"
#include "jsoncpp/json/json.h"
#include "utils.hpp"
int FRAME_WIDTH;
int FRAME_HEIGHT;
int AREA_TEXT_DY;
float area_name_font_scale;
Player::Player(QObject *parent) : QThread(parent) {
  cout << "player" << endl;
  stop = true;
  mode = MODE::RECOGNITION;
}

bool Player::loadVideo(std::string filename) {
  capture = new cv::VideoCapture(filename);
  //    capture  =  new cv::VideoCapture("rtsp://10.12.11.152:554/stream1");

  if (capture->isOpened()) {
    frameRate = (int)capture->get(CV_CAP_PROP_FPS);
    frame_width = (int)capture->get(CV_CAP_PROP_FRAME_WIDTH);
    frame_height = (int)capture->get(CV_CAP_PROP_FRAME_HEIGHT);
    return true;
  } else
    return false;
}

void Player::Play() {
  if (!isRunning()) {
    if (isStopped()) {
      stop = false;
    }
    start(LowPriority);
  }
}

void Player::run() {
  bool debug = false;
  std::string a = "";

  if (debug) {
    std::cout << "Player::run >> 0" << std::endl;
  }
  FRAME_WIDTH = this->frame_width;
  FRAME_HEIGHT = this->frame_height;
  AREA_TEXT_DY = 20 * FRAME_HEIGHT / 720;
  area_name_font_scale = 0.9 * FRAME_WIDTH / 1280;
  initParameters();
  int delay = 1; //(1000 / frameRate);
  MatchAgainstGallery matchgallery;
  matchgallery.readFromBinary("FaceVP9.bin");

  PredictionModels models;
  matchgallery.models = &models;

  // matchgallery.Init_Gallery_Folder_Based();
  CTracker tracker(0.15, 0.5, 100.0, 17, 17); // for Kalman filter analysis
  DtrackerManager dtrackers;

  int margin_left_right = (int)this->frame_width * 30 / 1280;
  int margin_top = (int)this->frame_height * 50 / 720;
  int margin_bottom = (int)this->frame_height * 200 / 720;

  // x-coordinate of the center point between gallery & recognition
  int x_gallery = (int)this->frame_width * 200 / 1280;
  int x_recognition = (int)this->frame_width * 1000 / 1280;

  Multi_Face_Area multi_face_area;
  multi_face_area.init(x_gallery + margin_left_right, margin_top,
                       x_recognition - margin_left_right,
                       this->frame_height - margin_bottom);
  multi_face_area.setRandom(1);
  multi_face_area.setShowPose(1);

  Single_Face_Area single_face_area;
  single_face_area.init(x_gallery + 6 * margin_left_right, margin_top,
                        x_recognition - 6 * margin_left_right,
                        this->frame_height - margin_bottom);
  single_face_area.setShowPose(1);

  multi_face_area.match_gallery = &matchgallery;
  single_face_area.match_gallery = &matchgallery;

  int count_frame = 0, cnt = 0, cnt2 = 0, cnt3 = 0, real_count = 0;
  int key;

  if (debug) {
    std::cout << "Player::run >> 1" << std::endl;
  }

  while (!stop) {
    if (debug) {
      std::cout << "Player::run >> 1.0" << std::endl;
    }
    if (!capture->read(frame)) {
      cout << "cant read frame !!!" << endl;
      stop = true;
      continue;
    }
    real_count++;
    if (debug) {
      std::cout << "Player::run >> 1.0 >> real_count = " << real_count
                << std::endl;
    }
    count_frame++;
    cnt++;
    cnt2++;
    cnt3++;
    if (cnt >= 30)
      cnt = 0;
    if (cnt2 >= 50)
      cnt2 = 0;
    if (cnt3 > 150)
      cnt3 = 0;
    if (count_frame < 2)
      continue;
    else
      count_frame = 0;

    // cv::resize(frame, frame, cv::Size(this->frame_width,
    // this->frame_height));
    matchgallery.frame = frame;

    // cout << cnt << endl;
    /*Process  Video*/
    if (this->mode == MODE::RECOGNITION) {
      if (debug) {
        std::cout << "Player::run >> 1.1" << std::endl;
      }
      //      cout << cnt3 << endl;
      if (cnt3 == 150) {
        matchgallery.getNewProfile("http://api.hrl.vp9.vn/api/humans?limit=2");
      }
      // cout << "RECOGNITION MODE" << endl;
      Detected_Faces_List detected_face_list;
      multi_face_area.match_gallery->MultiFace_LookUp_Gallery(
          frame, multi_face_area.recognition_roi, detected_face_list, dtrackers,
          cnt);
      if (debug) {
        std::cout << "Player::run >> 1.2 >> "
                  << detected_face_list.face_list.size() << " - "
                  << detected_face_list.face_info_list.size() << std::endl;
      }
      multi_face_area.Add_Face_to_Recent_Face_List(
          frame, detected_face_list, multi_face_area.match_gallery->folderpath);
      if (debug) {
        std::cout << "Player::run >> 1.3" << std::endl;
      }
      multi_face_area.Drawing_Stuff(frame, detected_face_list);
      if (detected_face_list.face_info_list.size() != 0) {
        QVector<matrix<float, 0, 1>> qVec =
            QVector<matrix<float, 0, 1>>::fromStdVector(
                detected_face_list.face_discriptor_list);
        QVector<matrix<rgb_pixel>> img_vec =
            QVector<matrix<rgb_pixel>>::fromStdVector(
                detected_face_list.face_list);
        if (debug) {
          std::cout << "Player::run >> 1.4" << std::endl;
        }

        QVector<string> id_qvec;
        for (size_t i; i < detected_face_list.face_info_list.size(); ++i) {
          id_qvec.push_back(detected_face_list.face_info_list[i].face_id);
          //          if
          //          (!detected_face_list.face_info_list[0].face_id.empty())
          // cv::Mat cv_img = toMat(detected_face_list.face_list[i]);
          // this->getCurrentFaceImg().push_back(cv_img);
          //          QImage tmp_img =
          //              QImage((const unsigned char *)(cv_img.data),
          //              cv_img.cols,
          //                     cv_img.rows, QImage::Format_RGB888);
          // img_vec.push_back(cv_img);
        }

        // cout << "Before emit new_persons_appear: " << endl;
        // cout << qVec[0] << endl;
        //<< qVec[0] << " -- "             << img_vec[0] << " -- " <<
        // id_qvec[0]
        //<< endl;

        if (debug) {
          std::cout << "Player::run >> 1.5" << std::endl;
        }
        Q_EMIT new_persons_appear(qVec, img_vec);

        // emit new_persons_appear(a);
      }
    } else {
      // cout << "TRAINING MODE" << endl;
      Detected_Faces_List detected_face_train_roi;
      single_face_area.match_gallery->SingleFace_LookUp_Gallery(
          frame, single_face_area.training_roi, detected_face_train_roi);
      single_face_area.Update_Current_Infor(
          detected_face_train_roi); // Update all current_sth variables
      single_face_area.Update_Track_with_FaceID(tracker,
                                                detected_face_train_roi, key);
      single_face_area.Drawing_Stuff(frame, detected_face_train_roi);
    }

    if (frame.channels() == 3) {
      cv::cvtColor(frame, RGBframe, CV_BGR2RGB);
      img = QImage((const unsigned char *)(RGBframe.data), RGBframe.cols,
                   RGBframe.rows, QImage::Format_RGB888);
    } else {
      img = QImage((const unsigned char *)(frame.data), frame.cols, frame.rows,
                   QImage::Format_Indexed8);
    }
    if (debug) {
      std::cout << "Player::run >> 1.6" << std::endl;
    }
    Q_EMIT processedImage(img);
    if (debug) {
      std::cout << "Player::run >> 1.7" << std::endl;
    }
    this->msleep(delay);
    if (debug) {
      std::cout << "Player::run >> 1.8" << std::endl;
    }
  }
  if (debug) {
    std::cout << "Player::run >> done!" << std::endl;
  }
}

Player::~Player() {
  mutex.lock();
  stop = true;
  capture->release();
  delete capture;
  condition.wakeOne();
  mutex.unlock();
  wait();
}

void Player::Stop() { stop = true; }

void Player::msleep(int ms) {
  struct timespec ts = {ms / 1000, (ms % 1000) * 1000 * 1000};
  nanosleep(&ts, NULL);
}

bool Player::isStopped() const { return this->stop; }

double Player::getCurrentFrame() {
  return capture->get(CV_CAP_PROP_POS_FRAMES);
}
double Player::getNumberOfFrames() {
  return capture->get(CV_CAP_PROP_FRAME_COUNT);
}
double Player::getFrameRate() { return frameRate; }
void Player::setCurrentFrame(int frameNumber) {
  capture->set(CV_CAP_PROP_POS_FRAMES, frameNumber);
}
