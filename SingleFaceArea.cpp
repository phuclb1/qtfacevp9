#include "config.h"
#include <dlib/opencv.h>

//#include <opencv2/core/core.hpp>
#include <opencv2/bgsegm.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/objdetect/objdetect.hpp>
//#include <opencv2/calib3d/calib3d.hpp>

#include <dlib/clustering.h>
#include <dlib/dnn.h>
#include <dlib/gui_widgets.h>
#include <dlib/image_io.h>
#include <dlib/image_processing.h>
#include <dlib/image_processing/frontal_face_detector.h>
#include <dlib/image_processing/render_face_detections.h>

#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <boost/foreach.hpp>
#include <dirent.h>
#include <fstream>
#include <iostream>
#include <map>
#include <math.h>
#include <queue>
#include <string.h>
#include <thread>

#include "SingleFaceArea.hpp"
#include "utils.hpp"

using namespace boost::filesystem;
using namespace dlib;
using namespace std;
using namespace cv;

Single_Face_Area::Single_Face_Area() {
  show_pose = 0;
  pose_count_color.clear();
  pose_count_color.push_back(cv::Scalar(0, 51, 255));
  pose_count_color.push_back(cv::Scalar(255, 102, 255));
  pose_count_color.push_back(cv::Scalar(0, 51, 0));
  pose_count_color.push_back(cv::Scalar(51, 153, 51));
  pose_count_color.push_back(cv::Scalar(102, 0, 102));
  pose_count_color.push_back(cv::Scalar(255, 153, 153));
}

void Single_Face_Area::init(int trl, int trt, int trr, int trb) {
  training_roi.left() = trl;
  training_roi.top() = trt;
  training_roi.right() = trr;
  training_roi.bottom() = trb;
}

void Single_Face_Area::Update_Current_Infor(
    Detected_Faces_List detected_face_list) {
  if (detected_face_list.det_list.size() > 0) {
    int ind = detected_face_list.biggest_face_index;
    current_face_descriptor = detected_face_list.face_discriptor_list[0];
    currentface = detected_face_list.face_list[ind];
    current_landmark_shape = detected_face_list.shape_list[ind];
    //        current_faceid = detected_face_list.face_id_list[ind];
    current_faceid = detected_face_list.face_info_list[ind].face_id;
    current_det = detected_face_list.det_list[ind];

    if (!current_faceid.empty())
      current_faceid_infor = match_gallery->output_inforlist[current_faceid];
    // This will load from the on-RAM memory person_facial_infolist
    current_peron_facial_infor =
        match_gallery->output_person_facial_inforlist[current_faceid];
  }
}

void Single_Face_Area::Add_NewFaceID_Training_Roi(CTrack *curtrack) {
  if (curtrack->trackname.empty())
    curtrack->number_conseccutiveframes_nofacefound++;
  else // if found face/ known person, then reset this
    curtrack->number_conseccutiveframes_nofacefound = 0;

  int trace_size = (int)curtrack->trace.size();
  // If 13 consecutive frames scan face, but do not recognize this track/person
  // -> add as new faceID
  // We need to check if face is nicely aligned, should base on landmark
  if (curtrack->number_conseccutiveframes_nofacefound >= 17 &&
      trace_size < 40) { // && Checkif_FasceisNicelyAligned_Landmarkbased){
    // Press Enter to force to save face, even not straight face

    if (currentface.size() == 0)
      return;
    //                    if
    //                    (!Checkif_FasceisNicelyAligned_Landmarkbased(current_landmark_shape))
    //                    continue;
    // if (!current_peron_facial_infor.Is_Looking_Straight()) return;

    time_t now = time(0);

    static char auto_faceid[30], auto_filename[30];
    strftime(auto_faceid, sizeof(auto_faceid), "%d%H%M%S", localtime(&now));
    // check if faceid is unique
    if (match_gallery->unique_faceid_list.count(auto_faceid) == 0)
      match_gallery->unique_faceid_list.insert(
          std::pair<string, int>(auto_faceid, 1));
    else
      return;

    strftime(auto_filename, sizeof(auto_filename), "%M%S", localtime(&now));
    string str = match_gallery->folderpath + auto_faceid;
    boost::filesystem::create_directory(str);

    string filename = auto_filename;

    // This is to save image with original Resolusion and with no Face aligned,
    // with larger crop
    Save_Images_to_TrainingImagePath(
        match_gallery->frame, current_det,
        match_gallery->trainingimagepath + auto_faceid, "straight" + filename);

    // Write_Info_GivenName(str2, auto_faceid);
    // Access the DB here to add new faceID, currently, no name has been entered
    //                    DBInstance.getHumanInfo(auto_faceid);
    //                    cout<<auto_faceid<<endl;

    std::map<string, matrix<float, 0, 1>> gallery_face_descriptor;
    gallery_face_descriptor.insert(std::pair<string, matrix<float, 0, 1>>(
        "straight", current_face_descriptor));
    match_gallery->gallery_face_descriptors.insert(
        std::pair<string, std::map<string, matrix<float, 0, 1>>>(
            auto_faceid, gallery_face_descriptor));

    std::map<std::string, int> pose_count;
    pose_count.insert(std::pair<string, int>("straight", 1));
    match_gallery->faceid_pose_count.insert(
        std::pair<std::string, std::map<string, int>>(auto_faceid, pose_count));

    User_Infor user_auto;
    user_auto.faceid = auto_faceid;
    // We do not add to list because we want to access DB for infor intervally
    // If using this on-mem list, then for each image saved of one faceID, we
    // need to add one User_Infor
    match_gallery->output_inforlist.insert(
        std::pair<string, User_Infor>(auto_faceid, user_auto));

    // Store the first and straight face here at declaration
    User_Facial_Infor user_facial_new(currentface, str, "straight" + filename);
    match_gallery->output_person_facial_inforlist.insert(
        std::pair<string, User_Facial_Infor>(auto_faceid, user_facial_new));
  }
}

void Single_Face_Area::Modify_FaceID_Training_Roi(CTrack *curtrack, int key) {
  // Press Backspace key (8) to Delete current faceID, reset all the face track
  // cout«key«endl;
  if ((key == 8 || key == 112) && !curtrack->trackname.empty()) { // p - 112 ;
    if (current_faceid.empty())
      return;
    match_gallery->gallery_face_descriptors.erase(current_faceid);
    match_gallery->output_inforlist.erase(current_faceid);
    match_gallery->output_person_facial_inforlist.erase(current_faceid);
    curtrack->trackname = "";
    curtrack->number_conseccutiveframes_nofacefound = 0;
    curtrack->trace.clear();
    string str = match_gallery->folderpath + current_faceid;
    current_faceid = "";
    // string str1 = folderpath+current_person_name+"/straight.jpg";
    path path_to_remove(str);
    for (directory_iterator end_dir_it, it(path_to_remove); it != end_dir_it;
         ++it) {
      remove_all(it->path());
    }
    // boost::filesystem::remove(str1);
    boost::filesystem::remove(str);
  }
  if (key == 13 || key == 102) { // key f 102 or enter
    // Press Enter to force to save face to new FaceID, even not straight face
    // cout«key«endl;
    if (current_faceid.empty())
      return;
    time_t now = time(0);
    static char auto_faceid[30], auto_filename[30];
    strftime(auto_faceid, sizeof(auto_faceid), "%d%H%M%S", localtime(&now));
    // check if faceid is unique
    if (match_gallery->unique_faceid_list.count(auto_faceid) == 0)
      match_gallery->unique_faceid_list.insert(
          std::pair<string, int>(auto_faceid, 1));
    else
      return;

    strftime(auto_filename, sizeof(auto_filename), "%H%M%S", localtime(&now));
    string str = match_gallery->folderpath + auto_faceid;

    boost::filesystem::create_directory(str);

    string filename = auto_filename;
    // This is to save image with original Resolusion and with no Face aligned,
    // with larger crop
    Save_Images_to_TrainingImagePath(
        match_gallery->frame, current_det,
        match_gallery->trainingimagepath + auto_faceid, "straight" + filename);

    std::map<string, matrix<float, 0, 1>> gallery_face_descriptor;
    gallery_face_descriptor.insert(std::pair<string, matrix<float, 0, 1>>(
        "straight", current_face_descriptor));
    match_gallery->gallery_face_descriptors.insert(
        std::pair<string, std::map<string, matrix<float, 0, 1>>>(
            auto_faceid, gallery_face_descriptor));

    std::map<std::string, int> pose_count;
    pose_count.insert(std::pair<string, int>("straight", 1));
    match_gallery->faceid_pose_count.insert(
        std::pair<std::string, std::map<string, int>>(auto_faceid, pose_count));

    User_Infor user_auto;
    user_auto.faceid = auto_faceid;
    // We do not add to list because we want to access DB for infor intervally
    // If using this on-mem list, then for each image saved of one faceID, we
    // need to add one User_Infor
    match_gallery->output_inforlist.insert(
        std::pair<string, User_Infor>(auto_faceid, user_auto));
    // Store the first and straight face here at declaration

    User_Facial_Infor user_facial_new(currentface, str, "straight" + filename);
    match_gallery->output_person_facial_inforlist.insert(
        std::pair<string, User_Facial_Infor>(auto_faceid, user_facial_new));
  }

  if (key == 65 && curtrack->trackname.empty()) { // d -100; a 65 ;
    // Press d to force to save face to current faceid
    time_t now = time(0);
    static char auto_filename[30];
    strftime(auto_filename, sizeof(auto_filename), "%H%M%S", localtime(&now));

    if (!current_faceid.empty()) {
      string str = match_gallery->folderpath + current_faceid;

      std::map<string, matrix<float, 0, 1>> &gallery_face_descriptor =
          match_gallery->gallery_face_descriptors[current_faceid];
      gallery_face_descriptor.insert(std::pair<string, matrix<float, 0, 1>>(
          current_faceid, current_face_descriptor));

      // Store the first and straight face here at declaration
      string str1 = str + "/" + auto_filename + ".jpg";
      // cout << str1;
      dlib::save_jpeg(currentface, str1);
    }
  }
  // Merge two most recent face profiles together
  // This is not working now, because we separate the training ROI, and have no
  // recent face list related
  //    if (key == 109 && !curtrack->trackname.empty()){ // m 109
  //        if (recent_faces_list.size()>1){
  //            string face_id_last =
  //            recent_faces_list[recent_faces_list.size()-2].first;
  //            string destination = folderpath+face_id_last;

  //            gallery_face_descriptors.erase(current_faceid);
  //            output_inforlist.erase(current_faceid);
  //            output_person_facial_inforlist.erase(current_faceid);

  //            std::map<string,matrix<float,0,1»& gallery_face_descriptor =
  //            gallery_face_descriptors[face_id_last];
  //            gallery_face_descriptor.insert(std::pair<string,matrix<float,0,1»(face_id_last,
  //            current_face_descriptor));

  //            curtrack->trackname = face_id_last;
  //            recent_faces_list.erase(recent_faces_list.begin() +
  //            recent_faces_list.size()-1);//can not erase more than 1

  //            string cur_folder = folderpath+current_faceid;
  //            path path_to_copy(cur_folder);
  //            for (directory_iterator end_dir_it, it(path_to_copy);
  //            it!=end_dir_it; ++it) {
  //                path path_to_paste(destination+"/" +
  //                it->path().filename().string());
  //                copy_file(it->path(),path_to_paste);
  //            }

  //            //Now remove folder
  //            for (directory_iterator end_dir_it, it(path_to_copy);
  //            it!=end_dir_it; ++it) {
  //                 remove_all(it->path());
  //            }
  //            boost::filesystem::remove(path_to_copy);

  //        }
  //    }
}

void Single_Face_Area::Save_Multiple_FaceImages_Training_ROI(CTrack *curtrack) {
  // TAKE MORE FACE SAMPLES HERE
  // The current FACEID is actually current_faceid

  if (current_faceid.empty())
    return;
  if (curtrack->trace.size() < 15)
    return; // need established track
  if (match_gallery->use_multiface_samples_autosave) {
    std::map<string, matrix<float, 0, 1>> &gallery_face_descriptor =
        match_gallery->gallery_face_descriptors[current_faceid];

    // Check if images are not very similar
    if (!match_gallery->Add_More_FaceImages_That_Have_Certain_Distance(
            current_face_descriptor, current_faceid))
      return;

    // Store the first and straight face here at declaration
    User_Facial_Infor user_face_infor =
        match_gallery->output_person_facial_inforlist[current_faceid];

    // Update the state of face direction
    user_face_infor.Calculate_Angle_Direction_Landmarkbased(
        current_landmark_shape);
    // Using file prefix to name a saving file to gallery
    string file_prefix = "";
    if (user_face_infor.direction_h.empty() &&
        user_face_infor.direction_v.empty())
      file_prefix = "straight";
    else if (!user_face_infor.direction_h.empty())
      file_prefix = user_face_infor.direction_h;
    else if (!user_face_infor.direction_v.empty())
      file_prefix = user_face_infor.direction_v;
    // else if (user_face_infor.Is_Smiling_Face(current_landmark_shape))
    // file_prefix = "smiling";

    time_t now = time(0);
    static char auto_postfix[10];
    strftime(auto_postfix, sizeof(auto_postfix), "%M%S", localtime(&now));
    string fullfilename = file_prefix + auto_postfix;

    bool saved = user_face_infor.Save_More_FaceImage_For1FaceID(
        gallery_face_descriptor, current_face_descriptor, currentface,
        match_gallery->folderpath + current_faceid, file_prefix);
    if (saved) {
      cout << current_faceid << endl;
    }

    // This is to save image with original Resolusion and with no Face aligned,
    // with larger crop
    Save_Images_to_TrainingImagePath(
        match_gallery->frame, current_det,
        match_gallery->trainingimagepath + current_faceid, fullfilename);
  }
}

void Single_Face_Area::Update_Track_with_FaceID(
    CTracker &tracker, Detected_Faces_List detected_face_train_roi, int key) {
  std::vector<cv::Point2d> corners; // for Kalman filter
  for (size_t i = 0; i < (int)detected_face_train_roi.det_list.size(); i++) {
    Rect rect =
        detected_face_train_roi.det_list[i]; // this is face detection rect
    corners.push_back(Point2d(rect.x, rect.y));
  }

  // cout<<"Start Kalman"<<endl;
  // Using Kalman filter here
  if (corners.size() > 0) {
    // cout<<"Start Kalman update"<<endl;
    tracker.Update(corners, true);

    // cout<<"End Kalman update"<<endl;
    // Loop through all the tracks available
    // std::cout<<endl<<"Number of tracks: "<< tracker.tracks.size()<<endl;
    for (int i = 0; i < (int)tracker.tracks.size(); i++) {
      int ind = tracker.assignment[i];
      if (ind < 0)
        continue;
      // int trace_size = tracker.tracks[i]->trace.size();
      CTrack *curtrack = tracker.tracks[i];

      // Clean some track that contain more than one faceIDs, or tracks with
      // occlusion
      // Do somethings here

      //            if (ind<(int) detected_face_train_roi.face_id_list.size())
      //                curtrack->trackname =
      //                detected_face_train_roi.face_id_list[ind]; //this is
      //                res, res can be empty (no best match found)
      if (ind < (int)detected_face_train_roi.face_info_list.size())
        curtrack->trackname =
            detected_face_train_roi.face_info_list[ind]
                .face_id; // this is res, res can be empty (no best match found)

      //            if (curtrack->Same_faceIDs_onsametrack() == false){//Track
      //            is occluded
      //                curtrack->Reset();
      //            }

      // Call all the related functions here
      Add_NewFaceID_Training_Roi(curtrack);
      Modify_FaceID_Training_Roi(curtrack, key);
      Save_Multiple_FaceImages_Training_ROI(curtrack);
      //////////////////////////////////////
    }
  }
}

void Single_Face_Area::Drawing_Stuff(cv::Mat &frame,
                                     Detected_Faces_List detected_face_list) {

  cv::Rect t_roi(training_roi.left(), training_roi.top(), training_roi.width(),
                 training_roi.height());
  cv::rectangle(frame, t_roi, cv::Scalar(255, 255, 0), 2);

  std::string training_text = "Training Area";
  int baseline = 0;
  cv::Size training_text_size = cv::getTextSize(
      training_text, cv::FONT_HERSHEY_TRIPLEX, 0.9, 2, &baseline);
  //    long training_text_x = training_roi.left() + (training_roi.width() -
  //    training_text_size.width) / 2;
  //    long training_text_y = training_roi.top() - AREA_TEXT_DY;
  long training_text_y =
      training_roi.top() - AREA_TEXT_DY >= training_text_size.height
          ? training_roi.top() - AREA_TEXT_DY
          : training_roi.bottom() + AREA_TEXT_DY + training_text_size.height;

  //    cv::putText(frame, training_text, cv::Point(training_text_x,
  //    training_text_y),cv::FONT_HERSHEY_TRIPLEX, 0.9, cv::Scalar(255,255,255),
  //    2, 8);
  textCentered(frame, training_text, training_roi.left(), training_roi.width(),
               training_text_y, cv::FONT_HERSHEY_TRIPLEX, area_name_font_scale,
               cv::Scalar(255, 255, 255), 2, 8);

  // Drawing detection RECT or landmark and faceID and other information on
  // Training ROI
  //    if (detected_face_list.face_id_list.size()>0){
  if (detected_face_list.face_info_list.size() > 0) {
    if (!current_faceid.empty()) {
      int linespace = 30 * FRAME_HEIGHT / 720;
      float ratioleft = 1.0;
      Point pt2(current_det.x - (int)ratioleft * current_det.width,
                current_det.y - (int)current_det.height / 2 + linespace);
      //            Point pt3(current_det.x- (int) ratioleft*current_det.width ,
      //                      current_det.y - (int) current_det.height/2 + 2 *
      //                      linespace);

      Scalar color = Scalar(255, 255, 255);
      double fontscale = 0.6;
      int thickness = 1;

      int baseline = 0;
      cv::Size face_id_text_size = cv::getTextSize(
          "ID: " + match_gallery->output_inforlist[current_faceid].faceid,
          cv::FONT_HERSHEY_TRIPLEX, fontscale * 1.3, thickness, &baseline);
      int y_tmp = current_det.y - linespace;
      int y_face_id = y_tmp > face_id_text_size.height
                          ? y_tmp
                          : current_det.y + current_det.height +
                                face_id_text_size.height + linespace;

      //            cv::putText(frame, "ID: " +
      //            match_gallery->output_inforlist[current_faceid].faceid, pt2,
      //            cv::FONT_HERSHEY_TRIPLEX, fontscale, color, thickness);
      //            if (show_pose) {
      //                cv::putText(frame, "Pose: " + facepose, pt3,
      //                cv::FONT_HERSHEY_TRIPLEX, fontscale, color, thickness);
      //            }
      textCentered(
          frame,
          "ID: " + match_gallery->output_inforlist[current_faceid].faceid,
          current_det.x, current_det.width, y_face_id, cv::FONT_HERSHEY_TRIPLEX,
          fontscale * 1.3, color, thickness, cv::LINE_8, false, true);

      Drawing_Detection(frame, current_det, color);
    } else
      Drawing_Landmark(frame, current_landmark_shape);
  }

  // Drawing the progress bar for training area
  if (detected_face_list.det_list.size() > 0) {
    //        Drawing_ProgressBar(frame,
    //        Point(training_roi.left(),training_roi.bottom()),
    //        training_roi.width(),
    //                            match_gallery->gallery_face_descriptors[current_faceid].size(),
    //                            match_gallery->output_person_facial_inforlist[current_faceid].max_number_of_faces)
    //                            ;
    drawPoseCountGraph(frame, current_faceid);
  }
  //    drawPoseCountGraph(frame, "NamND");
}

void Single_Face_Area::drawPoseCountGraph(cv::Mat &frame,
                                          std::string _current_faceid) {
  int thick = 2 * FRAME_WIDTH / 1280;
  double font_scale = 0.3 * FRAME_WIDTH / 1280;
  int dy1 = (int)12 * FRAME_HEIGHT / 720;
  int dy2 = (int)2 * FRAME_HEIGHT / 720;

  const Scalar color = Scalar(255, 255, 0);
  int x0 = training_roi.left();
  int y0 = training_roi.bottom();
  int width = training_roi.width();
  int height = (int)width / 15;
  cv::Rect rec(x0, y0, width, height);
  cv::rectangle(frame, rec, color, thick);
  int x = x0 + thick;
  int max_count = match_gallery->output_person_facial_inforlist[_current_faceid]
                      .max_number_of_faces;
  int total_pose_count = 0;
  for (int i = 0; i < match_gallery->getNumberOfPose(); ++i) {
    std::string pose = match_gallery->getPose(i);
    int pose_count = match_gallery->getPoseCount(_current_faceid, pose);
    total_pose_count += pose_count;
  }
  if (max_count < total_pose_count) {
    max_count = total_pose_count;
  }
  for (int i = 0; i < match_gallery->getNumberOfPose(); ++i) {
    std::string pose = match_gallery->getPose(i);
    int pose_count = match_gallery->getPoseCount(_current_faceid, pose);
    if (pose_count > 0) {
      cv::Point vertices[4];
      int _w = (int)(width - 2 * thick) * pose_count / max_count;
      vertices[0].x = x;
      vertices[0].y = y0 + thick;
      vertices[1].x = x + _w;
      vertices[1].y = y0 + thick;
      vertices[2].x = vertices[1].x;
      vertices[2].y = y0 + height - thick;
      vertices[3].x = x;
      vertices[3].y = y0 + height - thick;
      // Now we can fill the rotated rectangle with our specified color
      cv::Scalar pose_color = pose_count_color.at(i);
      cv::fillConvexPoly(frame, vertices, 4, pose_color);
      textCentered(frame, pose.substr(0, 1), x, _w, vertices[2].y - dy1,
                   cv::FONT_HERSHEY_TRIPLEX, font_scale,
                   cv::Scalar(255, 255, 255), 1, 8);
      textCentered(frame, std::to_string(pose_count), x, _w,
                   vertices[2].y - dy2, cv::FONT_HERSHEY_TRIPLEX, font_scale,
                   cv::Scalar(255, 255, 255), 1, 8);
      x = vertices[1].x + 1;
    }
  }
}
